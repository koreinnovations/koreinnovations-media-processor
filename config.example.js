export default {
  max_run_time_seconds: 3600,
  file_downloader: {
    file_path_regex: 'watch/',
    temp_downloads_directory: ''
  },
  paths: {
    temp: '',
    originals: ''
  },
  oauth2: {
    youtube: {
      client_id: 'CLIENT_ID_HERE',
      client_secret: 'CLIENT_SECRET_HERE',
      token_url: 'https://accounts.google.com/o/oauth2/token'
    },
    facebook: {
      client_id: 'CLIENT_ID_HERE',
      client_secret: 'CLIENT_SECRET_HERE',
      token_url: 'https://graph.facebook.com/oauth/access_token'
    }
  },
  mysql: {
    host: '',
    database: '',
    user: '',
    password: ''
  },
  mongo: {
    uri: ''
  }
};
