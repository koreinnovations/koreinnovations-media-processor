import {EventEmitter} from 'events';
import stream from 'stream';
import moment from 'moment';

import MemoryStream from 'memorystream';

let test_string = 'The rain in Spain falls mainly on the plain.';

export default class ftp extends EventEmitter {

  constructor() {
    super();
    this._connected = false;
  }

  connect(options) {
    if (options.host == 'fake.server') {
      this.emit('error', makeError(0, 'Cannot connect to server'));
    } else {
      this._connected = true;
      this.emit('ready');
    }
  }

  end() {
    this._connected = false;
  }

  destroy() {

  }

  size(path, cb) {
    if (path == 'file.not.found') {
      cb(makeError(0, 'File not found'), null);
    } else {
      cb(null, Buffer.byteLength(test_string, 'utf8'));
    }
  }

  get(path, cb) {
    if (path.match(/file\.not\.found/)) {
      cb(makeError(0, 'File not found'), null);
    } else {
      try {
        let memStream = new MemoryStream(); // MemoryStream.createReadStream();

        cb(null, memStream);

        setTimeout(() => {
          console.log(test_string);
          memStream.write(test_string);
          memStream.end();
        }, 1);
      } catch (ex) {
        console.warn(ex);
        cb(makeError(0, ex.message), null);
      }
    }
  }

}


function makeError(code, text) {
  var err = new Error(text);
  err.code = code;
  return err;
}