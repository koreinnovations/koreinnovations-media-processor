import EventEmitter from 'events';

class ffmpeg extends EventEmitter {

  constructor(file_path) {
    super();
    this._file_path = file_path;
  }

  outputOptions(options) {
    return this;
  }

  audioCodec(codec) {
    return this;
  }

  save(new_path) {

    setTimeout(() => {

      if (this._file_path == 'error.mov') {
        this.emit('error', new Error('Error processing file'));
      } else {
        this.emit('end');
      }

    }, 200);

  }

}

export default function (file_path) {
  return new ffmpeg(file_path);
}