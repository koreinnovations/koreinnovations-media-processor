import Deferred from '../../build/lib/Deferred';
import FileMetaData from '../../build/lib/FileMetaData';
import FetcherConnectorBase from '../../build/lib/connectors/fetchers/FetcherConnectorBase';
import PreparerConnectorBase from '../../build/lib/connectors/preparers/PreparerConnectorBase';
import PublisherConnectorBase from '../../build/lib/connectors/publishers/PublisherConnectorBase';
import FinalizerConnectorBase from '../../build/lib/connectors/finalizers/FinalizerConnectorBase';
import MetaDataParserBase from '../../build/lib/metadata_parsers/MetaDataParserBase';

export class FakeMetaDataParser extends MetaDataParserBase {
  static parse(file_path) {
    let deferred = new Deferred();
    let file_metadata = new FileMetaData(file_path, {});
    deferred.resolve(file_metadata);
    return deferred.promise;
  }
}

export class FakeMetaDataParserWillFailOnSpecialFile extends MetaDataParserBase {
  static parse(file_path) {
    let deferred = new Deferred();

    if (file_path.match(/failure/)) {
      deferred.reject('Mock metadata parser failing on special failure file');
    } else {
      let file_metadata = new FileMetaData(file_path, {});
      deferred.resolve(file_metadata);
    }
    
    return deferred.promise;
  }
}

export class FakeMetaDataParserReturnsWrongType extends MetaDataParserBase {
  static parse(file_path) {
    let deferred = new Deferred();
    deferred.resolve('abcdefg');
    return deferred.promise;
  }
}

export class FakeMetaDataParserError extends MetaDataParserBase {
  static parse(file_path) {
    let deferred = new Deferred();
    deferred.reject();
    return deferred.promise;
  }
}

export class FakeFetcherConnectorNoActionDescription extends FetcherConnectorBase {
  parse(file_path) {
    let deferred = new Deferred();
    deferred.resolve();
    return deferred.promise;
  }
}

export class FakeFetcherConnectorEmptyActionDescription extends FetcherConnectorBase {

  static actionDescription() {
    return null;
  }

  process(file_path) {
    let deferred = new Deferred();
    deferred.resolve();
    return deferred.promise;
  }
}

export class FakeFetcherConnector1 extends FetcherConnectorBase {

  static actionDescription() {
    return 'fetched using fake connector 1';
  }

  process() {
    let deferred = new Deferred();
    deferred.resolve();
    return deferred.promise;
  }

}

export class FakeFetcherConnector2 extends FetcherConnectorBase {

  static actionDescription() {
    return 'fetched using fake connector 2';
  }

  process() {
    let deferred = new Deferred();
    deferred.resolve();
    return deferred.promise;
  }

}

export class FakeFetcherConnectorError extends FetcherConnectorBase {

  static actionDescription() {
    return 'fetched using fake connector error';
  }

  process() {
    let deferred = new Deferred();
    deferred.reject('Fake connector failed on purpose');
    return deferred.promise;
  }

}

export class FakePreparerConnector1 extends PreparerConnectorBase {

  static actionDescription() {
    return 'prepared using fake connector 1';
  }

  process() {
    let deferred = new Deferred();
    deferred.resolve();
    return deferred.promise;
  }

}

export class FakePreparerConnector2 extends PreparerConnectorBase {

  static actionDescription() {
    return 'prepared using fake connector 1';
  }

  process() {
    let deferred = new Deferred();
    deferred.resolve();
    return deferred.promise;
  }

}

export class FakePreparerThrowErrorConnector extends PreparerConnectorBase {

  static actionDescription() {
    return 'prepared using fake connector that throws error';
  }

  process() {
    throw new Error('Simulated preparer error');
  }

}

export class FakePreparerConnectorError extends PreparerConnectorBase {

  static actionDescription() {
    return 'prepared using fake connector error';
  }

  process() {
    let deferred = new Deferred();
    deferred.reject('Fake connector failed on purpose');
    return deferred.promise;
  }

}


export class FakePublisherConnector1 extends PublisherConnectorBase {

  static actionDescription() {
    return 'published using fake connector 1';
  }

  process() {
    let deferred = new Deferred();
    deferred.resolve();
    return deferred.promise;
  }

}

export class FakePublisherConnector2 extends PublisherConnectorBase {

  static actionDescription() {
    return 'published using fake connector 2';
  }

  process() {
    let deferred = new Deferred();
    deferred.resolve();
    return deferred.promise;
  }

}

export class FakePublisherConnectorError extends PublisherConnectorBase {

  static actionDescription() {
    return 'published using fake connector error';
  }

  process() {
    let deferred = new Deferred();
    deferred.reject('Fake connector failed on purpose');
    return deferred.promise;
  }

}

export class FakePublisherConnectorError2 extends PublisherConnectorBase {

  static actionDescription() {
    return 'published using fake connector error (2)';
  }

  process() {
    let deferred = new Deferred();
    deferred.reject('Fake connector failed on purpose (2)');
    return deferred.promise;
  }

}

export class FakePublisherThrowErrorConnector extends PublisherConnectorBase {

  static actionDescription() {
    return 'published using fake connector that deliberately throws an error';
  }

  process() {
    throw new Error('Uber mega error');
  }

}


export class FakeFinalizerConnector1 extends FinalizerConnectorBase {

  static actionDescription() {
    return 'finalized using fake connector 1';
  }

  process() {
    let deferred = new Deferred();
    deferred.resolve();
    return deferred.promise;
  }

}

export class FakeFinalizerConnector2 extends FinalizerConnectorBase {

  static actionDescription() {
    return 'finalized using fake connector 2';
  }

  process() {
    let deferred = new Deferred();
    deferred.resolve();
    return deferred.promise;
  }

}

export class FakeFinalizerConnectorError extends FinalizerConnectorBase {

  static actionDescription() {
    return 'finalized using fake connector error';
  }

  process() {
    let deferred = new Deferred();
    deferred.reject('Fake connector failed on purpose');
    return deferred.promise;
  }

}