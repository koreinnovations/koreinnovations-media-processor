import fetchMock from 'fetch-mock';
import qs from 'qs';

export default function () {
  fetchMock.mock('http://my.test.url/token', (url, opts) => {
    let post_data = qs.parse(opts.body);

    if (post_data.grant_type == 'refresh_token') {
      if (post_data.refresh_token == 'valid') {
        return {
          access_token: 'brand_new_token',
          refresh_token: 'new_refresh_token',
          expires_in: 3600
        };
      } else {
        return 400;
      }
    }
  });

  fetchMock.mock('https://accounts.google.com/o/oauth2/token', (url, opts) => {
    let post_data = qs.parse(opts.body);

    if (post_data.grant_type == 'authorization_code') {
      if (post_data.code == 'valid') {
        return {
          access_token: 'brand_new_token',
          refresh_token: 'new_refresh_token',
          expires_in: 3600
        };
      } else {
        return 400;
      }
    }
    else if (post_data.grant_type == 'refresh_token') {
      if (post_data.refresh_token == 'valid') {
        return {
          access_token: 'brand_new_token',
          refresh_token: 'new_refresh_token',
          expires_in: 3600
        };
      } else {
        return 400;
      }
    }
  });
}