import sinon from 'sinon';

var clock;
var faked = false;

export function fakeTimer(moment) {
  restoreTimer();

  clock = sinon.useFakeTimers(moment.valueOf());
  faked = true;
}

export function restoreTimer() {
  if (faked) {
    clock.restore();
  }
}

export function isFaked() {
  if (faked) {
    // console.log('timer is faked');
  }
  return faked;
}
