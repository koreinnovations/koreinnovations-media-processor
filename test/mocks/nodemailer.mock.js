export default {
  createTransport: (config) => ({

    sendMail: (options, cb) => {

      if (options.to.match(/throw_error/)) {
        throw new Error('Throwing error');
      }
      if (options.to.match(/trigger_error/)) {
        cb(new Error('Failed to send on purpose'), {});
      } else {
        cb(null, {
          response: 'Sent successfully'
        });
      }

    }

  })
}