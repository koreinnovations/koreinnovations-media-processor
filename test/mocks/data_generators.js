import CustomerConfig from '../../build/lib/CustomerConfig';

export function generateCustomer() {
  return new CustomerConfig('test', 'Test Customer');
}