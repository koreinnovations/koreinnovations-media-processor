require("babel-core/register");
require("babel-polyfill");

import mockery from 'mockery';
import fetchMock from 'fetch-mock';
import fs from 'fs';
import path from 'path';
import register from 'babel-core/register';
import chai from 'chai';
import chaiAsPromised from 'chai-as-promised';
import proxyquire from 'proxyquire';

import ftp_mock from './mocks/ftp.mock';
import setupFetchMocks from './mocks/fetch.mock';

setupFetchMocks();


import ConfigurationManager from '../build/lib/ConfigurationManager';

let config_defaults = {
  email_reports: {
    transport: 'smtps://test.transport',
    sender: 'noreply@koreinnovations.io',
  },
  file_downloader: {
    temp_downloads_directory: __dirname
  }
};

ConfigurationManager.setDefaults(config_defaults);

const FileDownloader = proxyquire.noCallThru().load('../build/lib/connectors/fetchers/FileDownloader', {
  'ftp': ftp_mock,
}).default;

// Ignore all node_modules except these
const modulesToCompile = [].map((moduleName) => new RegExp(`/node_modules/${moduleName}`));
const rcPath = path.join(__dirname, '..', '.babelrc');
const source = fs.readFileSync(rcPath).toString();
const config = JSON.parse(source);

config.ignore = function (filename) {
  if (!(/\/node_modules\//).test(filename)) {
    return false;
  } else {
    const matches = modulesToCompile.filter((regex) => regex.test(filename));
    const shouldIgnore = matches.length === 0;
    return shouldIgnore;
  }
};

register(config);

chai.use(chaiAsPromised);

// mockery.registerMock('node-fetch', fetchMock.fetchMock);

// Setup globals / chai
global.__DEV__ = true;
global.expect = chai.expect;
global.config_defaults = config_defaults;
global.fetch = fetchMock.fetchMock;