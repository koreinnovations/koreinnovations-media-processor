export default {
  invalid: {
    server: 'fake.server',
    username: 'test',
    password: 'test',
    path: '/fake/path'
  },
  valid: {
    server: 'successful.server',
    username: 'test',
    password: 'test',
    path: '/real/path'
  }
};