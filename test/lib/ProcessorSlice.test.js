import ConnectorManager from '../../build/lib/ConnectorManager';
import CustomerConfig from '../../build/lib/CustomerConfig';
import FileMetaData from '../../build/lib/FileMetaData';
import ProcessorSlice from '../../build/lib/ProcessorSlice';

import {
  CONNECTOR_FETCHER,
  CONNECTOR_PREPARER,
  CONNECTOR_PUBLISHER,
  CONNECTOR_FINALIZER
} from '../../build/lib/Constants';

import {
  FakeMetaDataParser, FakeMetaDataParserError, FakeMetaDataParserReturnsWrongType,
  FakeFetcherConnector1, FakeFetcherConnector2, FakeFetcherConnectorError,
  FakePreparerConnector1, FakePreparerConnector2, FakePreparerThrowErrorConnector, FakePreparerConnectorError,
  FakePublisherThrowErrorConnector, FakePublisherConnector1, FakePublisherConnector2,
  FakePublisherConnectorError, FakePublisherConnectorError2,
  FakeFinalizerConnector1, FakeFinalizerConnector2, FakeFinalizerConnectorError
} from '../mocks/fake_connectors.mock';

describe('ProcessorSlice', () => {
  let customer, file_metadata, file_raw;

  beforeEach(() => {
    customer = new CustomerConfig('abc', 'Test Customer');
    file_raw = {name: 'myfile.mp4', path: '/Users/noahlively/Desktop/myfile.mp4'};
  });

  describe('constructor()', () => {


    it('Should error if I pass something other than a CustomerConfig object as first parameter', () => {
      expect(() => new ProcessorSlice({}, file_raw)).to.throw(Error);
    });

    it('Should error if the second parameter is not an object with a path property', () => {
      expect(() => new ProcessorSlice(customer, {})).to.throw(Error);
    });

    it('Should error if I pass a FileMetaData instance as second parameter', () => {
      let file_metadata = new FileMetaData('/Users/noahlively/Desktop/myfile.mp4');
      expect(() => new ProcessorSlice(customer, file_metadata)).to.throw(Error);
    });

    it('Should not throw error if I pass a CustomerConfig object and an object with a path property', () => {
      expect(() => new ProcessorSlice(customer, file_raw)).to.not.throw(Error);
    });

  });

  describe('Processing chain', () => {
    let slice;

    beforeEach(() => {
      ConnectorManager.unregisterAll();
      ConnectorManager.registerMetaDataParser(FakeMetaDataParser);
      slice = new ProcessorSlice(customer, file_raw);
    });

    describe('fetch()', () => {
      let type = CONNECTOR_FETCHER;

      it('first fetcher run should be the first one registered', async() => {
        ConnectorManager.registerConnectors(type, [FakeFetcherConnector1, FakeFetcherConnector2]);

        await slice.fetch();

        expect(slice.getHistory(type)[0].connector_name).to.equal('FakeFetcherConnector1');

      });

      describe('one of the fetchers throws an error during execution', () => {

        beforeEach(() => {
          ConnectorManager.registerConnectors(type, [FakeFetcherConnector1, FakeFetcherConnectorError, FakeFetcherConnector2]);
        });

        it('should not run remaining fetchers', async() => {
          try {
            await slice.fetch();
          } catch (err) {

          }

          expect(slice.getHistory(type)[0].success).to.be.true;
          expect(slice.getHistory(type)[1].success).to.be.false;
          expect(slice.getHistory(type)).to.have.length(2);
        });
        it('should reject the fetch() promise', (done) => {
          slice.fetch().then(
            () => done('Should have rejected promise but resolved it'),
            () => done()
          ).catch(err => done(err));
        });
      });

    });

    describe('parseMetaData()', () => {

      it('should resolve the promise if the parser yields a FileMetaData instance', async() => {

        ConnectorManager.registerMetaDataParser(FakeMetaDataParser);

        let promise;
        try {
          promise = slice.parseMetaData();
          await promise;
        } catch (err) {

        }

        expect(promise).to.be.resolved;

      });

      it('should reject the promise if the parser does not yield a FileMetaData instance', async() => {

        ConnectorManager.registerMetaDataParser(FakeMetaDataParserReturnsWrongType);

        let promise;
        try {
          promise = slice.parseMetaData();
          await promise;
        } catch (err) {

        }

        expect(promise).to.be.rejected;

      });

      it('should reject the promise if the parser throws an error', async() => {

        ConnectorManager.registerMetaDataParser(FakeMetaDataParserError);

        let promise;
        try {
          promise = slice.parseMetaData();
          await promise;
        } catch (err) {

        }

        expect(promise).to.be.rejected;

      });

    });

    describe('prepare()', () => {
      let type = CONNECTOR_PREPARER;

      it('first preparer run should be the first one registered', async() => {
        ConnectorManager.registerConnectors(type, [FakePreparerConnector1, FakePreparerConnector2]);

        await slice.parseMetaData();
        await slice.prepare();

        expect(slice.getHistory(type)[0].connector_name).to.equal('FakePreparerConnector1');

      });

      describe('one of the preparers throws an error during execution', () => {

        beforeEach(() => {
          ConnectorManager.registerConnectors(type, [FakePreparerConnector1, FakePreparerConnectorError, FakePreparerConnector2]);
        });

        it('should not run remaining preparers', async() => {
          try {
            await slice.parseMetaData();
            await slice.prepare();
          } catch (err) {

          }

          expect(slice.getHistory(type)[0].success).to.be.true;
          expect(slice.getHistory(type)[1].success).to.be.false;
          expect(slice.getHistory(type)).to.have.length(2);
        });
        it('should reject the prepare() promise', (done) => {
          slice.parseMetaData().then(slice.prepare).then(
            () => done('Should have rejected promise but resolved it'),
            () => done()
          ).catch(err => done(err));
        });
      });

    });

    describe('publish()', () => {
      let type = CONNECTOR_PUBLISHER;

      it('first publisher run should be the first one registered', async() => {
        ConnectorManager.registerConnectors(type, [FakePublisherConnector1, FakePublisherConnector2]);

        await slice.parseMetaData();
        await slice.publish();

        expect(slice.getHistory(type)[0].connector_name).to.equal('FakePublisherConnector1');

      });

      describe('one of the publishers throws an error during execution', () => {

        beforeEach(() => {
          ConnectorManager.registerConnectors(type, [FakePublisherConnector1, FakePublisherConnectorError, FakePublisherConnector2]);
        });

        it('should continue to run remaining publishers', async() => {
          try {
            await slice.parseMetaData();
            await slice.publish();
          } catch (err) {

          }

          expect(slice.getHistory(type)[0].success).to.be.true;
          expect(slice.getHistory(type)[1].success).to.be.false;
          expect(slice.getHistory(type)[2].success).to.be.true;
          expect(slice.getHistory(type)).to.have.length(3);
        });
      });

      describe('any of the publishers succeeded', () => {

        beforeEach(() => {
          ConnectorManager.registerConnectors(type, [FakePublisherConnector1, FakePublisherConnectorError, FakePublisherConnector2]);
        });

        it('should resolve the publish() promise', (done) => {
          slice.parseMetaData().then(() => slice.publish()).then(
            () => done(),
            (ex) => done('Should have resolved promise but rejected it. ' + ex)
          ).catch(err => done(err));
        });
      });

      describe('none of the publishers succeeded', () => {

        beforeEach(() => {
          ConnectorManager.registerConnectors(type, [FakePublisherConnectorError, FakePublisherConnectorError2]);
        });

        it('should reject the publish() promise', (done) => {
          slice.parseMetaData().then(() => slice.publish()).then(
            () => done('Should have rejected promise but resolved it'),
            () => done()
          ).catch(err => done(err));
        });
      });

    });

    describe('finalize()', () => {
      let type = CONNECTOR_FINALIZER;

      it('first finalizer run should be the first one registered', async() => {
        ConnectorManager.registerConnectors(type, [FakeFinalizerConnector1, FakeFinalizerConnector2]);

        await slice.parseMetaData()
        await slice.finalize();

        expect(slice.getHistory(type)[0].connector_name).to.equal('FakeFinalizerConnector1');

      });

      describe('one of the finalizers throws an error during execution', () => {

        beforeEach(() => {
          ConnectorManager.registerConnectors(type, [FakeFinalizerConnector1, FakeFinalizerConnectorError, FakeFinalizerConnector2]);
        });

        it('should not run remaining finalizers', async() => {
          try {
            await slice.parseMetaData();
            await slice.finalize();
          } catch (err) {

          }

          expect(slice.getHistory(type)[0].success).to.be.true;
          expect(slice.getHistory(type)[1].success).to.be.false;
          expect(slice.getHistory(type)).to.have.length(2);
        });
        it('should reject the finalize() promise', (done) => {
          slice.parseMetaData().then(slice.finalize).then(
            () => done('Should have rejected promise but resolved it'),
            () => done()
          ).catch(err => done(err));
        });
      });

    });

    describe('process()', () => {

      beforeEach(() => {
        ConnectorManager.unregisterAll();
      });

      it('Should stop execution and reject the promise if fetchers fail', (done) => {

        ConnectorManager.registerConnectors(CONNECTOR_FETCHER, [FakeFetcherConnector1, FakeFetcherConnectorError, FakeFetcherConnector2]);
        ConnectorManager.registerMetaDataParser(FakeMetaDataParser);
        ConnectorManager.registerConnectors(CONNECTOR_PREPARER, [FakePreparerConnector1, FakePreparerConnector2]);
        ConnectorManager.registerConnectors(CONNECTOR_PUBLISHER, [FakePublisherConnector1, FakePublisherConnector2]);
        ConnectorManager.registerConnectors(CONNECTOR_FINALIZER, [FakeFinalizerConnector1, FakeFinalizerConnector2]);

        slice.process().then(() => {
            done('Should have rejected the promise but resolved it');
          },
          (err) => {
            let history = slice.getChronologicalHistory();
            let last_item = history.pop();
            if (last_item.connector_name == 'FakeFetcherConnectorError') {
              done();
            } else {
              done(`Expected last connector to have been FakeFetcherConnectorError but got ${last_item.connector_name}`);
            }
          }
        );
      });
      it('Should stop execution and reject the promise if metadata parsing fails', (done) => {
        ConnectorManager.registerConnectors(CONNECTOR_FETCHER, [FakeFetcherConnector1, FakeFetcherConnector2]);
        ConnectorManager.registerMetaDataParser(FakeMetaDataParserError);
        ConnectorManager.registerConnectors(CONNECTOR_PREPARER, [FakePreparerConnector1, FakePreparerConnector2]);
        ConnectorManager.registerConnectors(CONNECTOR_PUBLISHER, [FakePublisherConnector1, FakePublisherConnector2]);
        ConnectorManager.registerConnectors(CONNECTOR_FINALIZER, [FakeFinalizerConnector1, FakeFinalizerConnector2]);

        slice.process().then(() => {
            done('Should have rejected the promise but resolved it');
          },
          (err) => {
            let history = slice.getChronologicalHistory();
            let last_item = history.pop();
            if (last_item.connector_name == 'FakeFetcherConnector2') {
              done();
            } else {
              done(`Expected last connector to have been FakeFetcherConnector2 but got ${last_item.connector_name}`);
            }
          }
        );
      });
      it('Should stop execution and reject the promise if preparers fail', (done) => {
        ConnectorManager.registerConnectors(CONNECTOR_FETCHER, [FakeFetcherConnector1, FakeFetcherConnector2]);
        ConnectorManager.registerMetaDataParser(FakeMetaDataParser);
        ConnectorManager.registerConnectors(CONNECTOR_PREPARER, [FakePreparerConnector1, FakePreparerConnectorError, FakePreparerConnector2]);
        ConnectorManager.registerConnectors(CONNECTOR_PUBLISHER, [FakePublisherConnector1, FakePublisherConnector2]);
        ConnectorManager.registerConnectors(CONNECTOR_FINALIZER, [FakeFinalizerConnector1, FakeFinalizerConnector2]);

        slice.process().then(() => {
            done('Should have rejected the promise but resolved it');
          },
          (err) => {
            let history = slice.getChronologicalHistory();
            let last_item = history.pop();
            if (last_item.connector_name == 'FakePreparerConnectorError') {
              done();
            } else {
              done(`Expected last connector to have been FakePreparerConnectorError but got ${last_item.connector_name}`);
            }
          }
        );
      });
      it('Should stop execution and reject the promise if all publishers fail', (done) => {
        ConnectorManager.registerConnectors(CONNECTOR_FETCHER, [FakeFetcherConnector1, FakeFetcherConnector2]);
        ConnectorManager.registerMetaDataParser(FakeMetaDataParser);
        ConnectorManager.registerConnectors(CONNECTOR_PREPARER, [FakePreparerConnector1, FakePreparerConnector2]);
        ConnectorManager.registerConnectors(CONNECTOR_PUBLISHER, [FakePublisherConnectorError, FakePublisherConnectorError2]);
        ConnectorManager.registerConnectors(CONNECTOR_FINALIZER, [FakeFinalizerConnector1, FakeFinalizerConnector2]);

        slice.process().then(() => {
            done('Should have rejected the promise but resolved it');
          },
          (err) => {
            let history = slice.getChronologicalHistory();
            let last_item = history.pop();
            if (last_item.connector_name == 'FakePublisherConnectorError2') {
              done();
            } else {
              done(`Expected last connector to have been FakePublisherConnectorError but got ${last_item.connector_name}`);
            }
          }
        );
      });
      it('Should stop execution and reject the promise if finalizers fail', (done) => {
        ConnectorManager.registerConnectors(CONNECTOR_FETCHER, [FakeFetcherConnector1, FakeFetcherConnector2]);
        ConnectorManager.registerMetaDataParser(FakeMetaDataParser);
        ConnectorManager.registerConnectors(CONNECTOR_PREPARER, [FakePreparerConnector1, FakePreparerConnector2]);
        ConnectorManager.registerConnectors(CONNECTOR_PUBLISHER, [FakePublisherConnector1, FakePublisherConnector2]);
        ConnectorManager.registerConnectors(CONNECTOR_FINALIZER, [FakeFinalizerConnector1, FakeFinalizerConnectorError, FakeFinalizerConnector2]);

        slice.process().then(() => {
            done('Should have rejected the promise but resolved it');
          },
          (err) => {
            let history = slice.getChronologicalHistory();
            let last_item = history.pop();
            if (last_item.connector_name == 'FakeFinalizerConnectorError') {
              done();
            } else {
              done(`Expected last connector to have been FakeFinalizerConnectorError but got ${last_item.connector_name}`);
            }
          }
        );
      });

      describe('all connectors ran successfully', () => {

        beforeEach(() => {
          ConnectorManager.registerConnectors(CONNECTOR_FETCHER, [FakeFetcherConnector1, FakeFetcherConnector2]);
          ConnectorManager.registerMetaDataParser(FakeMetaDataParser);
          ConnectorManager.registerConnectors(CONNECTOR_PREPARER, [FakePreparerConnector1, FakePreparerConnector2]);
          ConnectorManager.registerConnectors(CONNECTOR_PUBLISHER, [FakePublisherConnector1, FakePublisherConnector2]);
          ConnectorManager.registerConnectors(CONNECTOR_FINALIZER, [FakeFinalizerConnector1, FakeFinalizerConnector2]);
        });

        it('Last connector processed should be last item in the history stack if process was successful', (done) => {
          slice.process().then(() => {
              let history = slice.getChronologicalHistory();
              let last_item = history.pop();
              if (last_item.connector_name == 'FakeFinalizerConnector2') {
                done();
              } else {
                done(`Expected last connector to have been FakeFinalizerConnector2 but got ${last_item.connector_name}`);
              }
            },
            (err) => {
              done(err);
            }
          );
        });

        it('Should resolve the promise if all connectors ran successfully', async() => {
          let promise = slice.process();
          await promise;

          expect(promise).to.be.resolved;
        });
      });

      it('Should stop execution and reject the promise if an error is thrown by a connector', (done) => {
        ConnectorManager.registerConnectors(CONNECTOR_FETCHER, [FakeFetcherConnector1, FakeFetcherConnector2]);
        ConnectorManager.registerMetaDataParser(FakeMetaDataParser);
        ConnectorManager.registerConnectors(CONNECTOR_PREPARER, [FakePreparerConnector1, FakePreparerConnector2, FakePreparerThrowErrorConnector]);
        ConnectorManager.registerConnectors(CONNECTOR_PUBLISHER, [FakePublisherConnector1, FakePublisherConnector2]);
        ConnectorManager.registerConnectors(CONNECTOR_FINALIZER, [FakeFinalizerConnector1, FakeFinalizerConnector2]);

        slice.process().then(
          () => done('Should have rejected promise but resolved it'),
          (err) => done()
        );

      });

    });

  });

});