import path from 'path';
import moment from 'moment';
import MP4MetaDataParser from '../../../build/lib/metadata_parsers/MP4MetaDataParser';

let sample1_path = path.resolve(__dirname + '/../../media/test1.mp4');
let sample2_path = path.resolve(__dirname + '/../../media/test2.mp4');
let lttvmetadatatest_path = path.resolve(__dirname + '/../../media/lttvmetadatatest.mp4');

describe('MP4 Metadata Parser', () => {

  describe('Sample file "lttvmetadatatest.mp4', () => {
    let filepath = lttvmetadatatest_path;
    let file_metadata;
    let expected_metadata = {
      title: 'LTTV Metadata Test',
      short_description: 'This is a test short description of a metadata test.',
      description: 'This is a test long description of a metadata test.',
      date: '7/16/2012',
      category: 'Sunday Morning Service',
      speaker: 'Apostle Leroy Thompson Sr.',
      tags: ['revelation', 'impartation', 'word of faith']
    };

    beforeEach(async() => {
      file_metadata = await MP4MetaDataParser.parse(filepath);
    });

    it(`Title should be "${expected_metadata.title}"`, () => {
      expect(file_metadata.getTitle()).to.equal(expected_metadata.title);
    });
    it(`Description should be "${expected_metadata.description}"`, () => {
      expect(file_metadata.getDescription()).to.equal(expected_metadata.description);
    });
    it(`Date should be "${expected_metadata.date}"`, () => {
      expect(file_metadata.getDate()).to.equal(expected_metadata.date);
    });
    it(`Date moment formatted should be "${expected_metadata.date}"`, () => {
      expect(file_metadata.getDateMoment().format('M/DD/YYYY')).to.equal(expected_metadata.date);
    });
    it(`Category should be "${expected_metadata.category}"`, () => {
      expect(file_metadata.getCategory()).to.equal(expected_metadata.category);
    });
    it(`Speaker should be "${expected_metadata.speaker}"`, () => {
      expect(file_metadata.getSpeaker()).to.equal(expected_metadata.speaker);
    });
    expected_metadata.tags.forEach(tag => {
      it(`Tags should include "${tag}"`, () => {
        expect(file_metadata.getTags()).to.contain(tag);
      })
    })

  });

  describe('Sample file "test1.mp4', () => {

    it('Should throw error because title cannot be determined', (done) => {
      let filepath = sample1_path;
      MP4MetaDataParser.parse(filepath).then(
        (file_metadata) => done('Promise was resolved when it should have been rejected'),
        (err) => done()
      );
    });

  });

  describe('Sample file "test2.mp4"', () => {

    let filepath = sample2_path;
    let expected_metadata = {
      title: 'lttv sample 2 - noah',
      description: 'Fun stuff',
      date: '12/25/2016',
      album: 'Christian Teaching',
      speaker: 'Leroy Jr.',
      duration: 78854,
      bytes: 20872597,
      dimensions: '1280x720',
      sample_aspect_ratio: '1:1',
      display_aspect_ratio: '16:9'
    };

    it(`Title should be "${expected_metadata.title}"`, () => {
      return MP4MetaDataParser.parse(filepath).then(
        (file_metadata) => {
          expect(file_metadata.getTitle()).to.equal(expected_metadata.title);
        }
      );
    });
    it(`Description should be "${expected_metadata.description}"`, () => {
      return MP4MetaDataParser.parse(filepath).then(
        (file_metadata) => {
          expect(file_metadata.getDescription()).to.equal(expected_metadata.description);
        }
      );
    });
    it(`Date should be "${expected_metadata.date}"`, () => {
      return MP4MetaDataParser.parse(filepath).then(
        (file_metadata) => {
          expect(file_metadata.getDate()).to.equal(expected_metadata.date);
        }
      );
    });
    it(`Category should be "${expected_metadata.category}"`, () => {
      return MP4MetaDataParser.parse(filepath).then(
        (file_metadata) => {
          expect(file_metadata.getCategory()).to.equal(expected_metadata.category);
        }
      );
    });
    it(`Speaker should be "${expected_metadata.speaker}"`, () => {
      return MP4MetaDataParser.parse(filepath).then(
        (file_metadata) => {
          expect(file_metadata.getSpeaker()).to.equal(expected_metadata.speaker);
        }
      );
    });
    it(`Duration should be ${expected_metadata.duration} milliseconds`, () => {
      return MP4MetaDataParser.parse(filepath).then(
        (file_metadata) => {
          expect(file_metadata.getDuration()).to.equal(expected_metadata.duration);
        }
      );
    });
    it(`Size should be ${expected_metadata.bytes} bytes`, () => {
      return MP4MetaDataParser.parse(filepath).then(
        (file_metadata) => {
          expect(file_metadata.getFilesize()).to.equal(expected_metadata.bytes);
        }
      );
    });
    it(`Dimensions should be ${expected_metadata.dimensions} bytes`, () => {
      return MP4MetaDataParser.parse(filepath).then(
        (file_metadata) => {
          expect(file_metadata.getDimensionsString()).to.equal(expected_metadata.dimensions);
        }
      );
    });
    it(`Sample aspect ratio should be ${expected_metadata.sample_aspect_ratio} bytes`, () => {
      return MP4MetaDataParser.parse(filepath).then(
        (file_metadata) => {
          expect(file_metadata.getSampleAspectRatio()).to.equal(expected_metadata.sample_aspect_ratio);
        }
      );
    });
    it(`Display aspect ratio should be ${expected_metadata.display_aspect_ratio} bytes`, () => {
      return MP4MetaDataParser.parse(filepath).then(
        (file_metadata) => {
          expect(file_metadata.getDisplayAspectRatio()).to.equal(expected_metadata.display_aspect_ratio);
        }
      );
    });

  });
});