import sinon from 'sinon';
import moment from 'moment';
import proxyquire from 'proxyquire';
import ConfigurationManager from '../../build/lib/ConfigurationManager';
import BatchProcessor from '../../build/lib/BatchProcessor';
import CustomerConfig from '../../build/lib/CustomerConfig';
import ProcessorSlice from '../../build/lib/ProcessorSlice';
import ConnectorManager from '../../build/lib/ConnectorManager';
// import EmailSummary from '../../build/lib/EmailSummary';
// import {generateCustomer} from '../mocks/data_generators';
import nodemailer from '../mocks/nodemailer.mock';
import {fakeTimer, restoreTimer} from '../mocks/timers';

const EmailSummary = proxyquire.noCallThru().load('../../build/lib/EmailSummary', {
  nodemailer: nodemailer
}).default;

import {
  FakeMetaDataParser,
  FakeMetaDataParserError,
  FakeMetaDataParserReturnsWrongType,
  FakeMetaDataParserWillFailOnSpecialFile,
  FakeFetcherConnector1,
  FakeFetcherConnector2,
  FakeFetcherConnectorError,
  FakePreparerConnector1,
  FakePreparerConnector2,
  FakePreparerConnectorError,
  FakePublisherThrowErrorConnector,
  FakePublisherConnector1,
  FakePublisherConnector2,
  FakePublisherConnectorError,
  FakeFinalizerConnector1,
  FakeFinalizerConnector2,
  FakeFinalizerConnectorError
} from '../mocks/fake_connectors.mock';

import {
  CONNECTOR_FETCHER,
  CONNECTOR_PREPARER,
  CONNECTOR_PUBLISHER,
  CONNECTOR_FINALIZER
} from '../../build/lib/Constants';

describe('EmailSummary', () => {

  let batch, customer, summary;
  let message_content;
  let clock, clock2;

  beforeEach(async() => {

    ConfigurationManager.setConfig({
      email_reports: {
        transport: 'smtp://test.transport',
        sender: 'noreply@koreinnovations.io'
      }
    });

    customer = new CustomerConfig('test', 'Test Customer');
    batch = new BatchProcessor();
    fakeTimer(moment('2016-11-26 11:00:00.000-08:00'));

    ConnectorManager.unregisterAll();
    ConnectorManager.registerMetaDataParser(FakeMetaDataParserWillFailOnSpecialFile);
    ConnectorManager.registerConnectors(CONNECTOR_FETCHER, [FakeFetcherConnector1]);
    ConnectorManager.registerConnectors(CONNECTOR_PREPARER, [FakePreparerConnector1]);
    ConnectorManager.registerConnectors(CONNECTOR_PUBLISHER, [FakePublisherConnector1]);
    ConnectorManager.registerConnectors(CONNECTOR_FINALIZER, [FakeFinalizerConnector2]);

    batch = new BatchProcessor();
    batch.startTimer();
    batch.addSlice(new ProcessorSlice(customer, {path: '/Users/noahlively/Desktop/testfile.mp4'}));
    batch.addSlice(new ProcessorSlice(customer, {path: '/Users/noahlively/Desktop/failure.mp4'}));
    batch.addSlice(new ProcessorSlice(customer, {path: '/Users/noahlively/Desktop/testfile2.mp4'}));

    let customer2 = new CustomerConfig('oddball', 'This customer should not be included');
    batch.addSlice(new ProcessorSlice(customer2, {path: '/Users/noahlively/Desktop/othercustomerfile.mp4'}));

    await batch.process();

    fakeTimer(moment('2016-11-26 11:43:00.000-08:00'));
    batch.endTimer();

    summary = new EmailSummary(customer, batch);

    message_content = summary.getMessageContent();
  });

  afterEach(() => {
    restoreTimer();
  });

  describe('constructor()', () => {

    describe('config.email_reports is not defined properly', () => {
      it('should throw an error if config.email_reports is not defined', () => {
        ConfigurationManager.setConfig({email_reports: null});
        expect(() => (new EmailSummary(customer, batch))).to.throw(Error, /email_reports must be defined in config.js and must include sender and transport properties/);
      });

      it('should throw an error if config.email_reports.sender is not defined', () => {
        ConfigurationManager.setConfig({email_reports: {sender: null, transport: 'smtp://test.transport'}});
        expect(() => (new EmailSummary(customer, batch))).to.throw(Error, /email_reports must be defined in config.js and must include sender and transport properties/);
      });

      it('should throw an error if config.email_reports.transport is not defined', () => {
        ConfigurationManager.setConfig({email_reports: {sender: 'noreply@koreinnovations.io', transport: null}});
        expect(() => (new EmailSummary(customer, batch))).to.throw(Error, /email_reports must be defined in config.js and must include sender and transport properties/);
      });
    });

    describe('config.email_reports is configured properly', () => {
      it('should thrown an error if CustomerConfig instance is not passed', () => {
        expect(() => (new EmailSummary({}, batch))).to.throw(Error, /First argument to constructor must be instance of CustomerConfig/);
      });

      it('should thrown an error if BatchProcessor instance is not passed', () => {
        expect(() => (new EmailSummary(customer, {}))).to.throw(Error, /Second argument to constructor must be instance of BatchProcessor/);
      });

      it('should not thrown an error if BatchProcessor instance is passed and config.email_reports is populated', () => {
        expect(() => (new EmailSummary(customer, batch))).not.to.throw(Error);
      });

    });

  });

  describe('getBatch()', () => {
    it('should return an instance of BatchProcessor', () => {
      let summary = new EmailSummary(customer, batch);

      expect(summary.getBatch()).to.be.instanceOf(BatchProcessor);
    });
  });

  describe('getMessageContent()', () => {


    describe('batch had mixed results with publisher connectors (some success, some failure)', () => {
      let customer;
      let summary;
      let message_content;
      beforeEach(async () => {

        ConnectorManager.unregisterAll();
        ConnectorManager.registerMetaDataParser(FakeMetaDataParserWillFailOnSpecialFile);
        ConnectorManager.registerConnectors(CONNECTOR_FETCHER, [FakeFetcherConnector1]);
        ConnectorManager.registerConnectors(CONNECTOR_PREPARER, [FakePreparerConnector1]);
        ConnectorManager.registerConnectors(CONNECTOR_PUBLISHER, [FakePublisherConnector1, FakePublisherConnectorError, FakePublisherConnector2]);
        ConnectorManager.registerConnectors(CONNECTOR_FINALIZER, [FakeFinalizerConnector2]);

        let batch = new BatchProcessor();
        batch.startTimer();
        let customer = new CustomerConfig('test', 'Test customer');
        batch.addSlice(new ProcessorSlice(customer, {path: '/Users/noahlively/Desktop/testfile.mp4'}));
        await batch.process();
        batch.endTimer();

        summary = new EmailSummary(customer, batch);
        message_content = summary.getMessageContent();
      });

      it('should contain bullet for successful publish connector 1', () => {
        expect(message_content).to.have.string('published using fake connector 1');
      });

      it('should contain bullet for successful publish connector 2', () => {
        expect(message_content).to.have.string('published using fake connector 2');
      });

      it('should contain bullet for unsuccessful publish connector', () => {
        console.log(message_content);
        expect(message_content).to.have.string('FakePublisherConnectorError: Fake connector failed on purpose');
      });
    });

    describe('batch did not process anything', () => {
      let customer;
      let summary;
      let message_content;

      beforeEach(async () => {
        let batch = new BatchProcessor();
        batch.startTimer();
        let customer = new CustomerConfig('test', 'Test customer');
        await batch.process();
        batch.endTimer();

        summary = new EmailSummary(customer, batch);
        message_content = summary.getMessageContent();
      });

      it('should not generate message content if there were no successes or failures', () => {
        expect(message_content).to.be.undefined;
      });

      it('summary.send() should resolve with sent==false', async() => {
        let result = await summary.send(['test@example.com']);
        expect(result.sent).to.be.false;
      });
    })

    it('should not include history belonging to other customers', () => {
      expect(message_content).not.to.have.string('othercustomerfile.mp4');
    });

    it('should show the name of the customer processed', () => {
      expect(message_content).to.have.string('Processed on behalf of: Test Customer');
    });
    it('should show the number of slices processed successfully', () => {
      expect(message_content).to.have.string('Files processed successfully: 2');
    });
    it('should show the number of slices that failed to process', () => {
      expect(message_content).to.have.string('Files processed unsuccessfully: 1');
    });

    it('should show start time', () => {
      expect(message_content).to.have.string('Started: 11:00am');
    });

    it('should show the batch time duration', () => {
      expect(message_content).to.have.string('Duration: 43 minutes');
    });

    it('should show bullets with all the filenames successfully processed', () => {
      expect(message_content).to.have.string("FILES PROCESSED:");
      expect(message_content).to.have.string("- testfile.mp4");
      expect(message_content).to.have.string("- testfile2.mp4");
    });
    it('should show bullets with all the filenames that failed to process', () => {
      expect(message_content).to.have.string("FILES WITH ISSUES:");
      expect(message_content).to.have.string("x failure.mp4");
    });

    it('should show reason why a file failed to process', () => {
      expect(message_content).to.have.string("x failure.mp4\n  - Issue: Mock metadata parser failing on special failure file");
    });

    it('next to each filename it should show nested bullets of all the connectors used', () => {
      expect(message_content).to.have.string("- testfile.mp4\n  - fetched using fake connector 1\n  - prepared using fake connector 1\n  - published using fake connector 1\n  - finalized using fake connector 2");
    });

  });

  describe('send()', () => {

    it('should reject the promise if recipients array is falsey', (done) => {
      summary.send(null).then(
        () => done('Should have rejected the promise but resolved it'),
        (err) => done()
      )
    });

    it('should reject the promise if recipients array is empty', (done) => {
      summary.send([]).then(
        () => done('Should have rejected the promise but resolved it'),
        (err) => done()
      )
    });

    it('should resolve the promise with sent==true if the email was sent', async() => {

      let promise = summary.send(['test@test.com']);
      let result = await promise;

      expect(promise).to.be.resolved;
      expect(result.sent).to.be.true;

    });
    it('should reject the promise if the email was not sent', async() => {
      let promise;
      try {
        promise = summary.send(['trigger_error']);
        await promise;
      } catch (ex) {

      }

      expect(promise).to.be.rejected;
    });
    it('should reject the promise if an error was thrown', async() => {
      let promise;
      try {
        promise = summary.send(['throw_error']);
        await promise;
      } catch (ex) {
      }

      expect(promise).to.be.rejected;
    });

  });

});
