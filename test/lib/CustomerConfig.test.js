import OAuth2Client from '../../build/lib/OAuth2Client';
import ConfigurationManager from '../../build/lib/ConfigurationManager';
import CustomerConfig from '../../build/lib/CustomerConfig';
import proxyquire from 'proxyquire';

describe('CustomerConfig', () => {


  describe('General', () => {

    const CustomerConfig = require('../../build/lib/CustomerConfig').default;
    let customer = new CustomerConfig('test_id', 'Test Name');

    describe('getID()', () => {
      it('Should return same value as passed in via constructor', () => {
        expect(customer.getID()).to.equal('test_id');
      });
    });

    describe('getName()', () => {
      it('Should return same value as passed in via constructor', () => {
        expect(customer.getName()).to.equal('Test Name');
      });
    });


    describe('When ftp credentials are set', () => {

      let customer = new CustomerConfig('test_id', 'Test Name');
      customer.setFTPCredentials('test.server', 'test.user', 'test.password', '/');

      describe('getFTPCredentials()', () => {
        it('Result should have a host property', () => {
          expect(customer.getFTPCredentials().host).equal('test.server');
        });
        it('Result should have a user property', () => {
          expect(customer.getFTPCredentials().user).to.equal('test.user');
        });
        it('Result should have a password property', () => {
          expect(customer.getFTPCredentials().password).to.equal('test.password');
        });

      });
      describe('getFTPPath()', () => {
        it('Result should have a path property', () => {
          expect(customer.getFTPPath()).to.equal('/');
        });
      });
    });
    describe('When ftp credentials are not set', () => {

      let customer = new CustomerConfig('test_id', 'Test Name');

      describe('getFTPCredentials()', () => {
        it('Result should have host undefined', () => {
          expect(customer.getFTPCredentials()).to.have.property('host');
        });
        it('Result should have user undefined', () => {
          expect(customer.getFTPCredentials()).to.have.property('user');
        });
        it('Result should have password undefined', () => {
          expect(customer.getFTPCredentials()).to.have.property('password');
        });
      });
      describe('getFTPPath()', () => {
        it('Result should have a path property', () => {
          expect(customer.getFTPPath()).to.be.undefined;
        });
      });
    });
  });

  describe('OAuth2', () => {

    describe('getYouTubeCredentials()', () => {

      it('When config.youtube is set, should return an instance of OAuth2Client', () => {
        ConfigurationManager.setConfig({
          oauth2: {
            youtube: {
              client_id: 'abcdefg',
              client_secret: 'hijklmnop',
              token_url: 'http://test'
            }
          }
        });
        let customer = new CustomerConfig('test_id', 'Test Name');
        expect(customer.getOAuth2Instance('youtube')).to.be.instanceOf(OAuth2Client);
      });

      it('When config.youtube is not set, should return undefined', () => {
        ConfigurationManager.setConfig({oauth2: {youtube: undefined}});
        let customer = new CustomerConfig('test_id', 'Test Name');
        expect(() => customer.getOAuth2Instance('youtube')).to.throw(Error);
      });

    });

    describe('getVimeoCredentials()', () => {

      it('When config.vimeo is set, should return an instance of OAuth2Client', () => {
        ConfigurationManager.setConfig({
          oauth2: {
            vimeo: {
              client_id: 'abcdefg',
              client_secret: 'hijklmnop',
              token_url: 'http://test'
            }
          }
        });
        let customer = new CustomerConfig('test_id', 'Test Name');
        expect(customer.getOAuth2Instance('vimeo')).to.be.instanceOf(OAuth2Client);
      });

      it('When config.vimeo is not set, should return undefined', () => {
        ConfigurationManager.setConfig({oauth2: {vimeo: undefined}});
        let customer = new CustomerConfig('test_id', 'Test Name');
        expect(() => customer.getOAuth2Instance('vimeo')).to.throw(Error);
      });

    });

    describe('getFacebookCredentials()', () => {

      it('When config.facebook is set, should return an instance of OAuth2Client', () => {
        ConfigurationManager.setConfig({
          oauth2: {
            facebook: {
              client_id: 'abcdefg',
              client_secret: 'hijklmnop',
              token_url: 'http://test'
            }
          }
        });
        let customer = new CustomerConfig('test_id', 'Test Name');
        expect(customer.getOAuth2Instance('facebook')).to.be.instanceOf(OAuth2Client);
      });

      it('When config.facebook is not set, should return undefined', () => {
        ConfigurationManager.setConfig({oauth2: {facebook: undefined}});
        let customer = new CustomerConfig('test_id', 'Test Name');
        expect(() => customer.getOAuth2Instance('facebook')).to.throw(Error);
      });

    });
  });

});