import sinon from 'sinon';
import moment from 'moment';

import BatchProcessor from '../../build/lib/BatchProcessor';
import ProcessorSlice from '../../build/lib/ProcessorSlice';
import CustomerConfig from '../../build/lib/CustomerConfig';
import ConnectorManager from '../../build/lib/ConnectorManager';
import ConfigurationManager from '../../build/lib/ConfigurationManager';
import MP4MetaDataParser from '../../build/lib/metadata_parsers/MP4MetaDataParser';

import {fakeTimer, restoreTimer} from '../mocks/timers';

import {
  CONNECTOR_FETCHER,
  CONNECTOR_PREPARER,
  CONNECTOR_PUBLISHER,
  CONNECTOR_FINALIZER
} from '../../build/lib/Constants';

import {
  FakeMetaDataParser,
  FakeMetaDataParserError,
  FakeMetaDataParserReturnsWrongType,
  FakeMetaDataParserWillFailOnSpecialFile,
  FakeFetcherConnector1,
  FakeFetcherConnector2,
  FakeFetcherConnectorError,
  FakePreparerConnector1,
  FakePreparerConnector2,
  FakePreparerConnectorError,
  FakePublisherThrowErrorConnector,
  FakePublisherConnector1,
  FakePublisherConnector2,
  FakePublisherConnectorError,
  FakeFinalizerConnector1,
  FakeFinalizerConnector2,
  FakeFinalizerConnectorError
} from '../mocks/fake_connectors.mock';

describe('BatchProcessor', () => {
  let batchProcessor;

  beforeEach(() => {
    ConnectorManager.registerMetaDataParser(FakeMetaDataParser);
    batchProcessor = new BatchProcessor();
  });


  describe('hasExceededTimeLimit', () => {

    beforeEach(() => {
      batchProcessor.startTimer();
    });

    describe('Max run time is not set or 0', () => {

      var clock;

      beforeEach(() => {
        fakeTimer(moment().add(3, 'hours'));
      });

      afterEach(() => {
        restoreTimer();
      });

      it('Should return false if time limit is 0', () => {
        ConfigurationManager.setConfig({max_run_time_seconds: 0});

        expect(batchProcessor.hasExceededTimeLimit()).to.be.false;
      });

      it('Should return false if time limit is undefined', () => {
        ConfigurationManager.setConfig({max_run_time_seconds: undefined});
        fakeTimer(moment().add(3, 'hours'));

        expect(batchProcessor.hasExceededTimeLimit()).to.be.false;
      });
    });

    describe('Max run time is 1 hour', () => {

      var clock;

      beforeEach(() => {
        ConfigurationManager.setConfig({max_run_time_seconds: 3600});
      });

      afterEach(() => {
        restoreTimer();
      });

      it('Should return false if time limit is 1 hour and time elapsed is 50 minutes', () => {
        fakeTimer(moment().add(50, 'minutes'));
        expect(batchProcessor.hasExceededTimeLimit()).to.be.false;

      });

      it('Should return true if time limit is 1 hour and time elapsed is 60 minutes', () => {
        fakeTimer(moment().add(60, 'minutes'));
        expect(batchProcessor.hasExceededTimeLimit()).to.be.true;
      });

      it('Should return true if time limit is 1 hour and time elapsed is 70 minutes', () => {
        fakeTimer(moment().add(70, 'minutes'));
        expect(batchProcessor.hasExceededTimeLimit()).to.be.true;
      });
    })

  });

  describe('addSlice()', () => {
    it('should throw error if first parameter is not a ProcessorSlice instance', () => {
      let customer = new CustomerConfig('test', 'Test Customer');
      let file = {path: '/Users/noahlively/Desktop/testfile.mp4'};
      expect(() => batchProcessor.addSlice(customer, file)).to.throw(Error);
    });
  });

  describe('getNextSlice()', () => {
    it('should return a ProcessorSlice instance', () => {
      let customer = new CustomerConfig('test', 'Test Customer');
      let file = {path: '/Users/noahlively/Desktop/testfile.mp4'};
      batchProcessor.addSlice(new ProcessorSlice(customer, file));

      let slice = batchProcessor.getNextSlice();
      expect(slice).to.be.instanceOf(ProcessorSlice);
    });
  });

  describe('process()', () => {

    describe('out of 3 processor slices, the middle one fails', () => {
      let customer;

      beforeEach(() => {
        ConnectorManager.unregisterAll();
        ConnectorManager.registerMetaDataParser(FakeMetaDataParserWillFailOnSpecialFile);
        ConnectorManager.registerConnectors(CONNECTOR_FETCHER, [FakeFetcherConnector1]);
        ConnectorManager.registerConnectors(CONNECTOR_PREPARER, [FakePreparerConnector1]);
        ConnectorManager.registerConnectors(CONNECTOR_PUBLISHER, [FakePublisherConnector1]);
        ConnectorManager.registerConnectors(CONNECTOR_FINALIZER, [FakeFinalizerConnector1]);

        customer = new CustomerConfig('test', 'Test Customer');

        batchProcessor.addSlice(new ProcessorSlice(customer, {path: '/Users/noahlively/Desktop/testfile.mp4'}));
        batchProcessor.addSlice(new ProcessorSlice(customer, {path: '/Users/noahlively/Desktop/failure.mp4'}));
        batchProcessor.addSlice(new ProcessorSlice(customer, {path: '/Users/noahlively/Desktop/testfile2.mp4'}));
      });
      it('should have processed third one after second one failed', async() => {
        await batchProcessor.process();
        let history = batchProcessor.getHistory();
        expect(history[0].success).to.be.true;
        expect(history[1].success).to.be.false;
        expect(history[2].success).to.be.true;
      });
      it('success cound should be 2', async() => {
        await batchProcessor.process();
        expect(batchProcessor.getSlicesProcessed()).to.equal(2);
      });
      it('failure count should be 1', async() => {
        await batchProcessor.process();
        expect(batchProcessor.getSlicesFailed()).to.equal(1);
      });

    });

    describe('There are 20 slices left to process', () => {

      beforeEach(() => {
        for (var i = 0; i < 20; i++) {
          let customer = new CustomerConfig('test', 'Test Customer');
          let file = {path: '/Users/noahlively/Desktop/testfile.mp4'};
          batchProcessor.addSlice(new ProcessorSlice(customer, file));
        }
      });

      describe('Time limit has passed', () => {

        let clock;

        beforeEach(() => {
          batchProcessor.startTimer();
          fakeTimer(moment().add(3, 'hours'));
        });

        afterEach(() => {
          restoreTimer();
        });

        it('hasExceededTimeLimit should be true', () => {
          expect(batchProcessor.hasExceededTimeLimit()).to.be.true;
        });

        it('Should end without processing more slices if time limit has been reached', (done) => {
          batchProcessor.process().then(() => {
            let count = batchProcessor.getRemainingSliceCount();
            if (count == 20) {
              done();
            } else {
              done('Invalid count: ' + count);
            }
          })
        });

      });

      it('Should continue processing slices if time limit has not been reached', (done) => {
        batchProcessor.process();

        setTimeout(() => {
          let count = batchProcessor.getRemainingSliceCount();
          if (count < 20) {
            done();
          }
          else {
            done('Expected remaining slice count to be less than 20.  Count is ' + count);
          }
        }, 1000);
      });

    });

    describe('There are no slices left to process', (done) => {
      it('Should end without having processed anything', () => {
        batchProcessor.process().then(() => {
          let count = batchProcessor.getSlicesProcessed();
          if (count == 0) {
            done();
          } else {
            done('Expected nothing to have been processed since nothing was in the queue.  Processed count is ' + count);
          }
        });
      });

    });

  });

});