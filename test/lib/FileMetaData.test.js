import FileMetaData from '../../build/lib/FileMetaData';
import path from 'path';
import moment from 'moment';

describe('FileMetaData', () => {

  let filepath = path.resolve(__dirname, '../../../my test video.mp4');

  describe('options passed to constructor', () => {

    it('getTitle() should match title', () => {
      let file = new FileMetaData(filepath, {title: 'noah'});
      expect(file.getTitle()).to.equal('noah');
    });

    it('getDescription() should match description', () => {
      let file = new FileMetaData(filepath, {description: 'abc'});
      expect(file.getDescription()).to.equal('abc');
    });

    it('getDateMoment() should return a moment', () => {
      let file = new FileMetaData(filepath, {date: '11/25/2016'});
      expect(moment.isMoment(file.getDateMoment())).to.be.true;
    });

    it('getDateMoment() should be same date passed in options', () => {
      let file = new FileMetaData(filepath, {date: '11/25/2016'});
      expect(file.getDateMoment().format('M-DD-YYYY')).to.equal('11-25-2016');
    });

  });

  describe('getSanitizedFilename()', () => {

    it('"my test video.mp4" should become "my_test_video.mp4', () => {

      let file = new FileMetaData(filepath);
      expect(file.getSanitizedFilename(true)).to.equal('my_test_video.mp4');
    });

    it('"my test - video1.mp4" should become "my_test_video1.mp4', () => {
      let file = new FileMetaData('my test - video1.mp4');
      expect(file.getSanitizedFilename(true)).to.equal('my_test_video1.mp4');
    });

  });

  describe('File is 720p (1280x720)', () => {

    let file = new FileMetaData(filepath);
    file.setDimensions(1280, 720);

    it('isWidescreen() should be true', () => {
      expect(file.isWidescreen()).to.be_truthy;
    });
    it('isStandard() should be false', () => {
      expect(file.isStandard()).to.be_falsey;
    });

  });

  describe('File is 1080p (1920x1080)', () => {

    let file = new FileMetaData(filepath);
    file.setDimensions(1920, 1080);

    it('isWidescreen() should be true', () => {
      expect(file.isWidescreen()).to.be_truthy;
    });
    it('isStandard() should be false', () => {
      expect(file.isStandard()).to.be_falsey;
    });

  });

  describe('File is 720x480', () => {

    let file = new FileMetaData(filepath);
    file.setDimensions(720, 480);

    it('isWidescreen() should be false', () => {
      expect(file.isWidescreen()).to.be_falsey;
    });
    it('isStandard() should be true', () => {
      expect(file.isStandard()).to.be_truthy;
    });

  });


});