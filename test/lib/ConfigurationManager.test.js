import ConfigurationManager from '../../build/lib/ConfigurationManager';

describe('ConfigurationManager', () => {

  describe('getConfig()', () => {

    it('Should throw an error if no configuration has been set', () => {
      ConfigurationManager.__reset();
      expect(() => ConfigurationManager.getConfig()).to.throw(Error);
    });

  });

  describe('setDefaults()', () => {

    describe('I call setDefaults() and then clearAll() and then getConfig()', () => {
      var config;
      beforeEach(() => {
        ConfigurationManager.setDefaults({first_name: 'Joe'});
        ConfigurationManager.setConfig({last_name: 'Blow'});
        ConfigurationManager.clearAll();
        config = ConfigurationManager.getConfig();
      });
      it('the defaults should persist', () => {
        expect(config.first_name).to.equal('Joe');
      });
      it('the custom stuff (non-defaults) should be wiped out', () => {
        expect(config.last_name).to.be.undefined;
      });
    });
  });

  describe('setConfig()', () => {

    it('The property I set on the configuration object should appear when I call getConfig()', ()=> {
      ConfigurationManager.setConfig({name: 'Noah'});
      let config = ConfigurationManager.getConfig();
      expect(config).to.contain.keys('name');
      expect(config.name).to.equal('Noah');
    });

    it('Should update/modify existing configuration object without destroying existing properties', () => {

    });

  });

  describe('clearAll()', () => {

    it('Should reset the configuration back to defaults', () => {
      ConfigurationManager.setConfig({name: 'Noah'});
      ConfigurationManager.clearAll();
      expect(ConfigurationManager.getConfig()).not.to.have.keys('Noah');
    });

  });

});