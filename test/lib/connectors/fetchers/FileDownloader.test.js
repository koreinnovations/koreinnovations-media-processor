import ConfigurationManager from '../../../../build/lib/ConfigurationManager';
import CustomerConfig from '../../../../build/lib/CustomerConfig';

import ftp_mock from '../../../mocks/ftp.mock';
import ftp_test_config from '../../../ftp_test_config';
import proxyquire from 'proxyquire';

const FileDownloader = proxyquire.noCallThru().load('../../../../build/lib/connectors/fetchers/FileDownloader', {
  'ftp': ftp_mock
}).default;

describe('FileDownloader', () => {

  let customer;
  let valid_filepath = {path: 'crawl/lttvsample1.mp4', name: 'lttvsample1.mp4'};
  let invalid_filepath = {path: 'file.not.found', name: 'file.not.found'};

  let invalid_credentials = {...ftp_test_config.invalid};
  let valid_credentials = {...ftp_test_config.valid};

  beforeEach(() => {
    ConfigurationManager.clearAll();
    customer = new CustomerConfig('test', 'Test Customer');
  });

  describe('constructor()', () => {
    it('Should throw an error if file_downloader is not defined in config', () => {
      ConfigurationManager.setConfig({file_downloader: undefined});
      expect(() => (new FileDownloader(customer, valid_filepath))).to.throw(Error, /file_downloader.temp_downloads_directory must be defined in config.js/);
    });
    it('Should throw an error if temp_downloads_directory is not defined in config', () => {
      ConfigurationManager.setConfig({file_downloader: {temp_downloads_directory: undefined}});
      expect(() => (new FileDownloader(customer, valid_filepath))).to.throw(Error, /file_downloader.temp_downloads_directory must be defined in config.js/);
    });
  });

  it('Should reject the promise if invalid FTP credentials are passed', (done) => {
    customer.setFTPCredentials(invalid_credentials.server, invalid_credentials.username, invalid_credentials.password, invalid_credentials.path);

    let downloader = new FileDownloader(customer, valid_filepath);

    downloader.process().then(
      () => done('Promise was resolved when should have been rejected'),
      (err) => done()
    );

  });

  it('Should reject the promise if file not found', async() => {
    customer.setFTPCredentials(valid_credentials.server, valid_credentials.username, valid_credentials.password, valid_credentials.path);

    let downloader = new FileDownloader(customer, invalid_filepath);
    let promise;

    try {
      promise = downloader.process();
      await promise;
    } catch (ex) {

    }

    expect(promise).to.be.resolved;
  });

  it('Should resolve the promise if file downloaded', async() => {
    customer.setFTPCredentials(valid_credentials.server, valid_credentials.username, valid_credentials.password, valid_credentials.path);

    let downloader = new FileDownloader(customer, valid_filepath);
    let promise;

    try {
      promise = downloader.process();
      await promise;
    } catch (ex) {
      console.warn(ex);
    }

    expect(promise).to.be.rejected;

  });

  it('Should update the raw file object if file downloaded');


});