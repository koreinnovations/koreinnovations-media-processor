import path from 'path';
import fs from 'fs';
import proxyquire from 'proxyquire';
import FileMetaData from '../../../../build/lib/FileMetaData';
import CustomerConfig from '../../../../build/lib/CustomerConfig';
import ConfigurationManager from '../../../../build/lib/ConfigurationManager';
import ffmpegMock from '../../../mocks/ffmpeg.mock';

const H264Encoder = proxyquire.noCallThru().load('../../../../build/lib/connectors/preparers/H264Encoder', {
  'fluent-ffmpeg': ffmpegMock
}).default;

ConfigurationManager.setConfig({
  paths: {
    temp: path.resolve(__dirname, '../../../temp/temp'),
    originals: path.resolve(__dirname, '../../../temp/originals')
  }
});


describe('H264Encoder', () => {

  describe('process()', () => {

    let customer = new CustomerConfig('test', 'Test Customer');

    it('should return a promise', () => {
      let file_raw = {path: 'error.mov'};
      let file_metadata = new FileMetaData('error.mov', {title: 'Test'});
      let connector = new H264Encoder(customer, file_raw, file_metadata);
      let result = connector.process();

      expect(result).to.be.instanceOf(Promise);
    });

    describe('When there is an encoding error', () => {
      it('Should reject the promise', (done) => {
        let file_raw = {path: 'error.mov'};
        let file_metadata = new FileMetaData('error.mov', {title: 'Test'});
        let connector = new H264Encoder(customer, file_raw, file_metadata);
        connector.process().then(
          () => done('It should have rejected the promise since there was an error'),
          (err) => done()
        );
      })
    });

    describe('When file has been encoded', () => {

      let filepath = path.resolve(__dirname, '../../../media/test.mov');
      let expected_new_filepath = path.resolve(__dirname, '../../../media/test.mp4');
      let file_raw = {path: filepath};
      let file_metadata = new FileMetaData(filepath, {title: 'Test'});
      let connector = new H264Encoder(customer, file_raw, file_metadata);
      let promise = connector.process();

      let expected_renamed_path = path.resolve(__dirname, '../../../temp/originals/test.mov');

      it('Raw file object should have a new property called "original_path" with original file', async() => {

        await promise;
        expect(file_raw.original_path).to.equal(filepath)

      });
      it('Raw file object\'s "path" should point to new h264 file, not original file', async() => {
        await promise;
        expect(file_raw.path).not.to.equal(filepath);
        expect(file_raw.path).to.equal(expected_new_filepath);
      });

      it('Meta data\'s file path should point to new h264 fil, not original file', async() => {
        await promise;
        expect(file_metadata.getFilename()).to.equal('test.mp4');
        expect(file_metadata.getFullPath()).to.equal(expected_new_filepath);
      });

      it('Original file should be moved to directory specified in config.path.originals', async() => {
        await promise;

        expect(fs.existsSync(filepath)).to.be.false;
        expect(fs.existsSync(expected_renamed_path)).to.be.true;

        // Cleanup
        fs.renameSync(expected_renamed_path, filepath);
      });

    });

    describe('When file does not need encoding', () => {

      let filepath = path.resolve(__dirname, '../../../media/test1.mp4');
      let file_raw = {path: filepath};
      let file_metadata = new FileMetaData(filepath, {title: 'Test'});
      let connector = new H264Encoder(customer, file_raw, file_metadata);
      let promise = connector.process();

      it('Raw file object should have a new property called "original_path" with original file', async() => {
        await promise;
        expect(file_raw.original_path).to.equal(filepath)
      });
      it('Raw file object\'s "path" should point to original file', async() => {
        await promise;
        expect(file_raw.original_path).to.equal(filepath)
      });
      it('Original file should not be moved to directory specified in config.path.originals', async() => {
        await promise;

        let renamed_path = path.resolve(__dirname, '../../../temp/originals/test1.mp4');

        expect(fs.existsSync(filepath)).to.be.true;
        expect(fs.existsSync(renamed_path)).to.be.false;
      });

    })

  });

});