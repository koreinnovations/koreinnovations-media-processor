import fs from 'fs';
import path from 'path';
import proxyquire from 'proxyquire';
import ConfigurationManager from '../../../../build/lib/ConfigurationManager';
import CustomerConfig from '../../../../build/lib/CustomerConfig';
import FileMetaData from '../../../../build/lib/FileMetaData';
import Deferred from '../../../../build/lib/Deferred';
import MP4MetaDataParser from '../../../../build/lib/metadata_parsers/MP4MetaDataParser';

const ThumbnailGenerator = proxyquire.noCallThru().load('../../../../build/lib/connectors/preparers/ThumbnailGenerator', {
  // 'fluent-ffmpeg': ffmpeg_mock
}).default;

let temp_path = path.resolve(__dirname, '../../../temp/temp');

describe('ThumbnailGenerator', () => {

  let customer = new CustomerConfig('test', 'Test Customer');

  var filepath, file_raw, file_metadata, connector;

  describe('process()', () => {
    it('should return a promise', () => {
      file_raw = {path: 'error.mov'};
      let file_metadata = new FileMetaData('error.mov', {title: 'Test'});
      let connector = new ThumbnailGenerator(customer, file_raw, file_metadata);
      let result = connector.process();

      expect(result).to.be.instanceOf(Promise);
    });
  });

  describe('Video file exists', () => {


    // it('Should generate thumbnails into the temp directory', async() => {
    //
    //   if (!fs.existsSync(temp_path))
    //     fs.mkdirSync(temp_path);
    //   ConfigurationManager.setConfig({
    //     paths: {
    //       temp: temp_path
    //     }
    //   });
    //   filepath = path.resolve(__dirname, '../../../media/test2.mp4');
    //   file_raw = {path: filepath};
    //
    //   var generateAsync = new Promise((resolve, reject) => {
    //     return MP4MetaDataParser.parse(filepath).then((file_metadata) => {
    //       connector = new ThumbnailGenerator(customer, file_raw, file_metadata);
    //       connector.process()
    //         .then(
    //           () => {
    //             resolve(file_metadata.getThumbnails())
    //           },
    //           (err) => {
    //             reject('Rejected promise when it should have been resolved')
    //           }
    //         );
    //     }, (err) => reject(err))
    //   });
    //
    //   var result = await generateAsync;
    //   let length = result.length;
    //
    //   // Cleanup
    //   result.forEach((item) => {
    //     let new_path = temp_path + '/' + item;
    //     if (fs.existsSync(new_path))
    //       fs.unlinkSync(new_path);
    //   });
    //   fs.rmdirSync(temp_path);
    //
    //   expect(length).to.be.greaterThan(0);
    // });
  });

  describe('Video file does not exist', () => {

    beforeEach(() => {
      filepath = path.resolve(__dirname, '../../../somerandomfilethatdoesntexist.mp4');
      file_raw = {path: filepath};

    });

    it('Should reject the promise', (done) => {
      let file_metadata = new FileMetaData(filepath);
      connector = new ThumbnailGenerator(customer, file_raw, file_metadata);
      connector.process()
        .then(
          () => {
            done('Resolved promise when it should have been rejected');
          },
          (err) => {
            done();
          }
        );
    });

  });

});