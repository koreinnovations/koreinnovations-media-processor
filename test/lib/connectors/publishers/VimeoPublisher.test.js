import moment from 'moment';
import path from 'path';
import ConfigurationManager from '../../../../build/lib/ConfigurationManager';
import CustomerConfig from '../../../../build/lib/CustomerConfig';
import FileMetaData from '../../../../build/lib/FileMetaData';
import mockery from 'mockery';
import proxyquire from 'proxyquire';

const VIMEO_TOKEN_URL = 'https://api.vimeo.com/oauth/access_token';

var vimeo_should_fail = false;

let vimeo_mock = {
  authenticate: () => {
    return {
      setCredentials: creds => {
      }
    }
  },
  videos: {
    insert: (video_metadata, callback) => {
      if (vimeo_should_fail) {
        callback('Failed to upload');
      } else {
        callback();
      }
    }
  }
};

import {getVimeoMetaData} from '../../../../build/lib/connectors/publishers/VimeoPublisher';
const VimeoPublisher = proxyquire.noCallThru().load('../../../../build/lib/connectors/publishers/VimeoPublisher', {
  'vimeo-api': vimeo_mock
}).default;


describe('VimeoPublisher', () => {

  describe('constructor()', () => {

    it('Should throw an error if vimeo OAuth2 credentials are missing from configuration', () => {

      ConfigurationManager.clearAll();
      let customer = new CustomerConfig('test', 'Test Customer');
      let filepath = path.resolve(__dirname, '../../../media/test1.mp4');
      let file_raw = {path: filepath};
      let file_metadata = new FileMetaData(filepath, {
        title: 'my test video',
        description: 'this is such an awesome vid'
      });
      file_metadata.setTags(['a', 'b', 'c']);

      expect(() => {
        new VimeoPublisher(customer, file_raw, file_metadata)
      }).to.throw(Error, /Vimeo OAuth2 credentials not provided/);
    });

    it('Should not throw an OAuth2 error if vimeo credentials exist in configuration', () => {
      ConfigurationManager.setConfig({
        oauth2: {
          vimeo: {
            client_id: 'abc',
            client_secret: '123',
            token_url: VIMEO_TOKEN_URL
          }
        }
      });
      let customer = new CustomerConfig('test', 'Test Customer');
      let filepath = path.resolve(__dirname, '../../../media/test1.mp4');
      let file_raw = {path: filepath};
      let file_metadata = new FileMetaData(filepath, {
        title: 'my test video',
        description: 'this is such an awesome vid'
      });
      file_metadata.setTags(['a', 'b', 'c']);

      expect(() => {
        new VimeoPublisher(customer, file_raw, file_metadata)
      }).not.to.throw(Error, /Vimeo OAuth2 credentials not provided/);
    });

  });

  describe('getVimeoMetaData()', () => {

    beforeEach(() => {
      ConfigurationManager.setConfig({
        vimeo: {
          client_id: 'abc',
          client_secret: 'def',
          token_url: VIMEO_TOKEN_URL
        }
      });
    });

    let filepath = path.resolve(__dirname, '../../../media/test1.mp4');
    let file_metadata = new FileMetaData(filepath, {
      title: 'my test video',
      description: 'this is such an awesome vid'
    });
    file_metadata.setTags(['a', 'b', 'c']);
    let vimeo_metadata = getVimeoMetaData(file_metadata);

    it('title should match title from file metadata', () => {
      expect(vimeo_metadata.resource.snippet.title).to.equal(file_metadata.getTitle());
    });
    it('description should match description from file metadata', () => {
      expect(vimeo_metadata.resource.snippet.description).to.equal(file_metadata.getDescription());
    });
    it('tags should match tags from file metadata', () => {
      expect(vimeo_metadata.resource.snippet.tags[0]).to.equal('a');
      expect(vimeo_metadata.resource.snippet.tags[1]).to.equal('b');
      expect(vimeo_metadata.resource.snippet.tags[2]).to.equal('c');
    });


  });

  // describe('process()', () => {
  //   let filepath = path.resolve(__dirname, '../../../media/test1.mp4');
  //   let file_raw = {path: filepath};
  //   let file_metadata = new FileMetaData(filepath, {
  //     title: 'my test video',
  //     description: 'this is such an awesome vid'
  //   });
  //   file_metadata.setTags(['a', 'b', 'c']);
  //
  //   it('Should reject the promise if it cannot get vimeo access', (done) => {
  //
  //     let customer = new CustomerConfig('test', 'Test Customer');
  //     customer.getOAuth2Instance('vimeo').setToken('failing', 'failing', moment().subtract(1, 'hour').unix());
  //
  //     let publisher = new VimeoPublisher(customer, file_raw, file_metadata);
  //     publisher.process().then(
  //       () => done('Should have rejected the promise but resolved it'),
  //       (err) => done()
  //     );
  //   });
  //
  //   it('Should reject the promise if it fails to upload to vimeo', (done) => {
  //
  //     let customer = new CustomerConfig('test', 'Test Customer');
  //     customer.getOAuth2Instance('vimeo').setToken('valid', 'valid', moment().subtract(1, 'hour').unix());
  //
  //     vimeo_should_fail = true;
  //
  //     let publisher = new VimeoPublisher(customer, file_raw, file_metadata);
  //     publisher.process().then(
  //       () => done('Should have rejected the promise but resolved it'),
  //       (err) => done()
  //     );
  //   });
  //
  //   it('Should resolve the promise if vimeo upload succeeds', (done) => {
  //
  //     let customer = new CustomerConfig('test', 'Test Customer');
  //     customer.getOAuth2Instance('vimeo').setToken('valid', 'valid', moment().subtract(1, 'hour').unix());
  //
  //     vimeo_should_fail = false;
  //
  //     try {
  //       let publisher = new VimeoPublisher(customer, file_raw, file_metadata);
  //       publisher.process().then(
  //         () => done(),
  //         (err) => done('Promise was rejected')
  //       );
  //     } catch (ex) {
  //       done(ex.message);
  //     }
  //   });
  //
  // });
});