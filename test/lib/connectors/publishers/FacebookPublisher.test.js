import moment from 'moment';
import path from 'path';
import ConfigurationManager from '../../../../build/lib/ConfigurationManager';
import CustomerConfig from '../../../../build/lib/CustomerConfig';
import FileMetaData from '../../../../build/lib/FileMetaData';
import mockery from 'mockery';
import proxyquire from 'proxyquire';

const FACEBOOK_TOKEN_URL = 'https://graph.facebook.com/oauth/access_token';

var facebook_should_fail = false;

let facebook_mock = {
  setAccessToken: () => {
    return {
      setCredentials: creds => {
      }
    }
  },
  api: (a, b, video_metadata, callback) => {
    if (facebook_should_fail) {
      callback({error: 'Failed to upload'});
    } else {
      callback({});
    }
  }
};

import {getFacebookMetaData} from '../../../../build/lib/connectors/publishers/FacebookPublisher';
const FacebookPublisher = proxyquire.noCallThru().load('../../../../build/lib/connectors/publishers/FacebookPublisher', {
  'fb': facebook_mock
}).default;


describe('FacebookPublisher', () => {

  describe('constructor()', () => {

    it('Should throw an error if facebook OAuth2 credentials are missing from configuration', () => {

      ConfigurationManager.clearAll();
      let customer = new CustomerConfig('test', 'Test Customer');
      let filepath = path.resolve(__dirname, '../../../media/test1.mp4');
      let file_raw = {path: filepath};
      let file_metadata = new FileMetaData(filepath, {
        title: 'my test video',
        description: 'this is such an awesome vid'
      });
      file_metadata.setTags(['a', 'b', 'c']);

      expect(() => {
        new FacebookPublisher(customer, file_raw, file_metadata)
      }).to.throw(Error, /Facebook OAuth2 credentials not provided/);
    });

    it('Should not throw an OAuth2 error if facebook credentials exist in configuration', () => {
      ConfigurationManager.setConfig({
        oauth2: {
          facebook: {
            client_id: 'abc',
            client_secret: '123',
            token_url: FACEBOOK_TOKEN_URL
          }
        }
      });
      let customer = new CustomerConfig('test', 'Test Customer');
      let filepath = path.resolve(__dirname, '../../../media/test1.mp4');
      let file_raw = {path: filepath};
      let file_metadata = new FileMetaData(filepath, {
        title: 'my test video',
        description: 'this is such an awesome vid'
      });
      file_metadata.setTags(['a', 'b', 'c']);

      expect(() => {
        new FacebookPublisher(customer, file_raw, file_metadata)
      }).not.to.throw(Error, /Facebook OAuth2 credentials not provided/);
    });

  });

  describe('getFacebookMetaData()', () => {

    beforeEach(() => {
      ConfigurationManager.setConfig({
        facebook: {
          client_id: 'abc',
          client_secret: 'def',
          token_url: FACEBOOK_TOKEN_URL
        }
      });
    });

    let filepath = path.resolve(__dirname, '../../../media/test1.mp4');
    let file_metadata = new FileMetaData(filepath, {
      title: 'my test video',
      description: 'this is such an awesome vid'
    });
    file_metadata.setTags(['a', 'b', 'c']);
    let facebook_metadata = getFacebookMetaData(file_metadata);

    it('title should match title from file metadata', () => {
      expect(facebook_metadata.title).to.equal(file_metadata.getTitle());
    });
    it('description should match description from file metadata', () => {
      expect(facebook_metadata.description).to.equal(file_metadata.getDescription());
    });
    it('content category should be FAMILY', () => {
      expect(facebook_metadata.content_category).to.equal('FAMILY');
    });


  });

  describe('process()', () => {
    let filepath = path.resolve(__dirname, '../../../media/test1.mp4');
    let file_raw = {path: filepath};
    let file_metadata = new FileMetaData(filepath, {
      title: 'my test video',
      description: 'this is such an awesome vid'
    });
    file_metadata.setTags(['a', 'b', 'c']);


    it('should return a promise', () => {
      let customer = new CustomerConfig('test', 'Test Customer');
      let publisher = new FacebookPublisher(customer, file_raw, file_metadata);
      let result = publisher.process();

      expect(result).to.be.instanceOf(Promise);
    });

    it('Should reject the promise if it fails to upload to facebook', (done) => {

      let customer = new CustomerConfig('test', 'Test Customer');
      customer.getOAuth2Instance('facebook').setToken('valid', 'valid', moment().subtract(1, 'hour').unix());

      facebook_should_fail = true;

      let publisher = new FacebookPublisher(customer, file_raw, file_metadata);
      publisher.process().then(
        () => done('Should have rejected the promise but resolved it'),
        (err) => done()
      );
    });

    it('Should resolve the promise if facebook upload succeeds', (done) => {

      let customer = new CustomerConfig('test', 'Test Customer');
      customer.getOAuth2Instance('facebook').setToken('valid', 'valid', moment().add(1, 'hour').unix());

      facebook_should_fail = false;

      try {
        let publisher = new FacebookPublisher(customer, file_raw, file_metadata);

        publisher.process().then(
          () => done(),
          (err) => done('Promise was rejected')
        );
      } catch (ex) {

        done(ex.message);
      }
    });

  });
});
