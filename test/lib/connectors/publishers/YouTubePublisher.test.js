import moment from 'moment';
import fs from 'fs';
import sinon from 'sinon';
import path from 'path';
import ConfigurationManager from '../../../../build/lib/ConfigurationManager';
import CustomerConfig from '../../../../build/lib/CustomerConfig';
import FileMetaData from '../../../../build/lib/FileMetaData';
import mockery from 'mockery';
import proxyquire from 'proxyquire';


let openURLStub = sinon.stub();

const YOUTUBE_TOKEN_URL = 'https://accounts.google.com/o/oauth2/token';

var youtube_should_fail = false;

let youtube_mock = {
  authenticate: () => {
    return {
      setCredentials: creds => {
      }
    }
  },
  videos: {
    insert: (video_metadata, callback) => {
      if (youtube_should_fail) {
        callback('Failed to upload');
      } else {
        callback();
      }
    }
  }
};

import {getYouTubeMetaData} from '../../../../build/lib/connectors/publishers/YouTubePublisher';
const YouTubePublisher = proxyquire.noCallThru().load('../../../../build/lib/connectors/publishers/YouTubePublisher', {
  'youtube-api': youtube_mock,
  '../../Util': {
    openURL: openURLStub,
    logMessage: (message) => {
    }
  }
  //'opn': mockOpn
}).default;


describe('YouTubePublisher', () => {

  describe('static getOAuth2Token()', () => {

    beforeEach(() => {

      ConfigurationManager.setConfig({
        oauth2: {
          youtube: {
            client_id: 'abc',
            client_secret: 'def',
            token_url: 'xyz'
          }
        }
      })

    });

    describe('A token file does not exist at the given path', () => {

      it('Should open a web browser prompting the user to authorize their YouTube account for the app', (done) => {

        let nonexistent_path = path.resolve(__dirname, '.youtube_nonexistent');
        expect(fs.existsSync(nonexistent_path)).to.be.false;

        YouTubePublisher.getOAuth2Token(nonexistent_path);

        setTimeout(() => {
          console.log(openURLStub);
          if (openURLStub.calledOnce) {
            done();
          } else {
            done('Expected browser to have been opened, but was not');
          }
        }, 1500);

      });

    });

    describe('A token was successfully fetched', () => {
      it('Should write the token to the file path provided');
      it('Should resolve the promise with the new token');
    });

    describe('Failed to fetch token', () => {
      it('Should reject the promise');
    })

  });

  describe('constructor()', () => {

    it('Should throw an error if youtube OAuth2 credentials are missing from configuration', () => {

      ConfigurationManager.clearAll();
      let customer = new CustomerConfig('test', 'Test Customer');
      let filepath = path.resolve(__dirname, '../../../media/test1.mp4');
      let file_raw = {path: filepath};
      let file_metadata = new FileMetaData(filepath, {
        title: 'my test video',
        description: 'this is such an awesome vid'
      });
      file_metadata.setTags(['a', 'b', 'c']);

      expect(() => {
        new YouTubePublisher(customer, file_raw, file_metadata)
      }).to.throw(Error, /YouTube OAuth2 credentials not provided/);
    });

    it('Should not throw an OAuth2 error if youtube credentials exist in configuration', () => {
      ConfigurationManager.setConfig({
        oauth2: {
          youtube: {
            client_id: 'abc',
            client_secret: '123',
            token_url: YOUTUBE_TOKEN_URL
          }
        }
      });
      let customer = new CustomerConfig('test', 'Test Customer');
      let filepath = path.resolve(__dirname, '../../../media/test1.mp4');
      let file_raw = {path: filepath};
      let file_metadata = new FileMetaData(filepath, {
        title: 'my test video',
        description: 'this is such an awesome vid'
      });
      file_metadata.setTags(['a', 'b', 'c']);

      expect(() => {
        new YouTubePublisher(customer, file_raw, file_metadata)
      }).not.to.throw(Error, /YouTube OAuth2 credentials not provided/);
    });

  });

  describe('getYouTubeMetaData()', () => {

    beforeEach(() => {
      ConfigurationManager.setConfig({
        youtube: {
          client_id: 'abc',
          client_secret: 'def',
          token_url: YOUTUBE_TOKEN_URL
        }
      });
    });

    let filepath = path.resolve(__dirname, '../../../media/test1.mp4');
    let file_metadata = new FileMetaData(filepath, {
      title: 'my test video',
      description: 'this is such an awesome vid'
    });
    file_metadata.setTags(['a', 'b', 'c']);
    let youtube_metadata = getYouTubeMetaData(file_metadata);

    it('title should match title from file metadata', () => {
      expect(youtube_metadata.resource.snippet.title).to.equal(file_metadata.getTitle());
    });
    it('description should match description from file metadata', () => {
      expect(youtube_metadata.resource.snippet.description).to.equal(file_metadata.getDescription());
    });
    it('tags should match tags from file metadata', () => {
      expect(youtube_metadata.resource.snippet.tags[0]).to.equal('a');
      expect(youtube_metadata.resource.snippet.tags[1]).to.equal('b');
      expect(youtube_metadata.resource.snippet.tags[2]).to.equal('c');
    });


  });

  describe('process()', () => {
    let filepath = path.resolve(__dirname, '../../../media/test1.mp4');
    let file_raw = {path: filepath};
    let file_metadata = new FileMetaData(filepath, {
      title: 'my test video',
      description: 'this is such an awesome vid'
    });
    file_metadata.setTags(['a', 'b', 'c']);


    it('should return a promise', () => {
      let customer = new CustomerConfig('test', 'Test Customer');
      let publisher = new YouTubePublisher(customer, file_raw, file_metadata);
      let result = publisher.process();

      expect(result).to.be.instanceOf(Promise);
    });

    it('Should reject the promise if it cannot get youtube access', (done) => {

      let customer = new CustomerConfig('test', 'Test Customer');
      customer.getOAuth2Instance('youtube').setToken('failing', 'failing', moment().subtract(1, 'hour').unix());

      let publisher = new YouTubePublisher(customer, file_raw, file_metadata);
      publisher.process().then(
        () => done('Should have rejected the promise but resolved it'),
        (err) => done()
      );
    });

    it('Should reject the promise if it fails to upload to youtube', (done) => {

      let customer = new CustomerConfig('test', 'Test Customer');
      customer.getOAuth2Instance('youtube').setToken('valid', 'valid', moment().subtract(1, 'hour').unix());

      youtube_should_fail = true;

      let publisher = new YouTubePublisher(customer, file_raw, file_metadata);
      publisher.process().then(
        () => done('Should have rejected the promise but resolved it'),
        (err) => done()
      );
    });

    it('Should resolve the promise if youtube upload succeeds', (done) => {

      let customer = new CustomerConfig('test', 'Test Customer');
      customer.getOAuth2Instance('youtube').setToken('valid', 'valid', moment().subtract(1, 'hour').unix());

      youtube_should_fail = false;

      try {
        let publisher = new YouTubePublisher(customer, file_raw, file_metadata);
        publisher.process().then(
          () => done(),
          (err) => done('Promise was rejected. ' + err)
        );
      } catch (ex) {
        done(ex.message);
      }
    });

  });
});