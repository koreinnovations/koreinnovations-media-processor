import moment from 'moment';

import OAuth2Client from '../../build/lib/OAuth2Client';

describe('OAuth2Client', () => {

  var client;
  beforeEach(() => {
    client = new OAuth2Client('abcdefg', 'hijklmnop', 'http://my.test.url/token');
  });

  describe('getAccessToken()', () => {

    it('Should return access token', () => {
      client.setToken('valid_access', 'valid_refresh', null);
      expect(client.getAccessToken()).to.equal('valid_access');
    });
  });

  describe('ensureAccess()', () => {

    it('Should reject the promise if access token is not present', (done) => {
      client.ensureAccess().then(
        () => done('Should have rejected promise but resolved instead'),
        (err) => done()
      )
    });

    it('Should resolve the promise with the current access token if token not expired', async () => {
      client.setToken('valid_access', 'valid_refresh', moment().add(4, 'days').unix());
      await client.ensureAccess();
      expect(client.getAccessToken()).to.equal('valid_access');
    });

    it('Should resolve the promise with a new access token if expired token was renewed', async () => {
      client.setToken('original_token', 'valid', moment().subtract(1, 'hour').unix());
      await client.ensureAccess();
      expect(client.getAccessToken()).not.to.equal('valid_access');
    });

  });

  describe('isExpired()', () => {
    it('Should be true if token expiration is not set', () => {
      client.setToken('valid', 'valid', null);
      expect(client.isExpired()).to.be_truthy;
    });

    it('Should be true if token expiration is less than current time', () => {
      client.setToken('valid', 'valid', moment().subtract(1, 'hour').unix());
      expect(client.isExpired()).to.be_truthy;
    });

    it('Should be true if token expiration is equal to current time', () => {
      client.setToken('valid', 'valid', moment().unix());
      expect(client.isExpired()).to.be_truthy;
    });

    it('Should be false if token expiration is ahead of current time', () => {
      client.setToken('valid', 'valid', moment().add(5, 'minutes').unix());
      expect(client.isExpired()).to.be_falsey;
    });
  });

  describe('refreshToken()', () => {

    it('Refreshed token should not be empty', (done) => {
      client.setToken('original_token', 'valid', moment().subtract(1, 'hour').unix());
      client.refreshToken().then(
        () => {
          let new_token = client.getAccessToken();
          if (new_token && new_token.length > 0) {
            done();
          } else {
            done('Refreshed access token is null or empty');
          }
        },
        (err) => done('Failed with error ' + err)
      );
    });

    it('Refreshed token should be different than previous access token', (done) => {
      client.setToken('original_token', 'valid', moment().subtract(1, 'hour').unix());
      client.refreshToken().then(
        () => {
          let new_token = client.getAccessToken();
          if (new_token != 'original_token') {
            done();
          } else {
            done('Refreshed access token is same as original access token');
          }
        },
        (err) => done('Failed with error ' + err)
      );
    });

  });

  describe('OAuth2 token is instantiated', () => {

    it('If I pass a null client id it should throw an error', ()=> {
      expect(() => new OAuth2Client(null, 'abcdefg', 'http://test')).to.throw(Error);
    });

    it('If I pass a null client secret it should throw an error', () => {
      expect(() => new OAuth2Client('abcdefg', null, 'http://test')).to.throw(Error);
    });

    it('If I pass a null token url it should throw an error', () => {
      expect(() => new OAuth2Client('abcdefg', 'fsfsdfsdf', null)).to.throw(Error);
    });

  });
});