import assert from 'assert';

import {
  CONNECTOR_FETCHER,
  CONNECTOR_PREPARER,
  CONNECTOR_PUBLISHER,
  CONNECTOR_FINALIZER
} from '../../build/lib/Constants';

import ConnectorManager from '../../build/lib/ConnectorManager';
import YouTubePublisher from '../../build/lib/connectors/publishers/YouTubePublisher';
import VimeoPublisher from '../../build/lib/connectors/publishers/VimeoPublisher';
import NetBroadcastingRokuPublisher from '../../build/lib/connectors/publishers/NetBroadcastingRokuPublisher';
import FacebookPublisher from '../../build/lib/connectors/publishers/FacebookPublisher';
import TemporaryFileRemoval from '../../build/lib/connectors/finalizers/TemporaryFileRemoval';
import FileDownloader from '../../build/lib/connectors/fetchers/FileDownloader';
import H264Encoder from '../../build/lib/connectors/preparers/H264Encoder';
import MP4MetaDataParser from '../../build/lib/metadata_parsers/MP4MetaDataParser';

import {
  FakeFetcherConnectorNoActionDescription,
  FakeFetcherConnectorEmptyActionDescription,

  FakeMetaDataParser,
  FakeMetaDataParserError,
  FakeMetaDataParserReturnsWrongType,
  FakeMetaDataParserWillFailOnSpecialFile,
  FakeFetcherConnector1,
  FakeFetcherConnector2,
  FakeFetcherConnectorError,
  FakePreparerConnector1,
  FakePreparerConnector2,
  FakePreparerConnectorError,
  FakePublisherThrowErrorConnector,
  FakePublisherConnector1,
  FakePublisherConnector2,
  FakePublisherConnectorError,
  FakeFinalizerConnector1,
  FakeFinalizerConnector2,
  FakeFinalizerConnectorError
} from '../mocks/fake_connectors.mock';

describe('validate()', () => {

  it('Should throw an error if no metadata parser has been set', () => {
    ConnectorManager.unregisterAll();
    expect(() => ConnectorManager.validate()).to.throw(Error, 'No metadata parser registered');
  });

});

describe('Registering connectors', () => {

  it('Should throw error if I register a connector that does not provide a static actionDescription() method that returns a string', () => {
    expect(() => ConnectorManager.registerConnectors(CONNECTOR_FETCHER, [FakeFetcherConnectorNoActionDescription])).to.throw(Error, 'Connector class must define actionDescription() static method');
  });
  it('Should throw error if I register a connector that provides a static actionDescription() method with a falsey return', () => {
    expect(() => ConnectorManager.registerConnectors(CONNECTOR_FETCHER, [FakeFetcherConnectorEmptyActionDescription])).to.throw(Error, 'Attempted to register connector that does not provide the required static actionDescription() method that returns a string');
  });

  it('Should throw error if I register a connector with a type other than "CONNECTOR_FETCHER", "CONNECTOR_PUBLISHER", "CONNECTOR_FINALIZER" or "CONNECTOR_PREPARER"', () => {
    expect(() => ConnectorManager.registerConnectors('abcefg', [FacebookPublisher])).to.throw(Error);
  });

  it('Should not throw error if I register a connector with a type of "CONNECTOR_FETCHER"', () => {
    expect(() => ConnectorManager.registerConnectors(CONNECTOR_FETCHER, [FileDownloader])).to.not.throw(Error);
  });

  it('Should not throw error if I register a connector with a type of "CONNECTOR_PUBLISHER"', () => {
    expect(() => ConnectorManager.registerConnectors(CONNECTOR_PUBLISHER, [FacebookPublisher])).to.not.throw(Error);
  });

  it('Should not throw error if I register a connector with a type of "CONNECTOR_PREPARER"', () => {
    expect(() => ConnectorManager.registerConnectors(CONNECTOR_PREPARER, [H264Encoder])).to.not.throw(Error);
  });

  it('Should not throw error if I register a connector with a type of "CONNECTOR_FINALIZER"', () => {
    expect(() => ConnectorManager.registerConnectors(CONNECTOR_FINALIZER, [TemporaryFileRemoval])).to.not.throw(Error);
  });

  describe(CONNECTOR_FETCHER, () => {

    it('Should throw error if I try register a preparer connector as a preparer', () => {
      expect(() => ConnectorManager.registerConnectors(CONNECTOR_FETCHER, [H264Encoder])).to.throw(Error);
    });

    it('Should throw error if I try register a publisher connector as a preparer', () => {
      expect(() => ConnectorManager.registerConnectors(CONNECTOR_FETCHER, [FacebookPublisher])).to.throw(Error);
    });

    it('Should throw error if I try register a finalization connector as a preparer', () => {
      expect(() => ConnectorManager.registerConnectors(CONNECTOR_FETCHER, [TemporaryFileRemoval])).to.throw(Error);
    });

  });


  describe(CONNECTOR_PREPARER, () => {

    it('Should throw error if I try register a fetcher connector as a preparer', () => {
      expect(() => ConnectorManager.registerConnectors(CONNECTOR_PREPARER, [FileDownloader])).to.throw(Error);
    });

    it('Should throw error if I try register a publisher connector as a preparer', () => {
      expect(() => ConnectorManager.registerConnectors(CONNECTOR_PREPARER, [FacebookPublisher])).to.throw(Error);
    });

    it('Should throw error if I try register a finalization connector as a preparer', () => {
      expect(() => ConnectorManager.registerConnectors(CONNECTOR_PREPARER, [TemporaryFileRemoval])).to.throw(Error);
    });

  });


  describe(CONNECTOR_PUBLISHER, () => {

    it('Should throw error if I try register a fetcher connector as a publisher', () => {
      expect(() => ConnectorManager.registerConnectors(CONNECTOR_PUBLISHER, [FileDownloader])).to.throw(Error);
    });

    it('Should throw error if I try register a preparation connector as a publisher', () => {
      expect(() => ConnectorManager.registerConnectors(CONNECTOR_PUBLISHER, [H264Encoder])).to.throw(Error);
    });

    it('Should throw error if I try register a finalization connector as a publisher', () => {
      expect(() => ConnectorManager.registerConnectors(CONNECTOR_PUBLISHER, [TemporaryFileRemoval])).to.throw(Error);
    });

  });

  describe(CONNECTOR_FINALIZER, () => {

    it('Should throw error if I try register a fetcher connector as a finalizer', () => {
      expect(() => ConnectorManager.registerConnectors(CONNECTOR_FINALIZER, [FileDownloader])).to.throw(Error);
    });

    it('Should throw error if I try register a publisher connector as a finalizer', () => {
      expect(() => ConnectorManager.registerConnectors(CONNECTOR_FINALIZER, [FacebookPublisher])).to.throw(Error);
    });

    it('Should throw error if I try register a preparer connector as a finalizer', () => {
      expect(() => ConnectorManager.registerConnectors(CONNECTOR_FINALIZER, [H264Encoder])).to.throw(Error);
    });

  });

  it('Should throw error if I register a connector that does not exist', () => {
    expect(() => ConnectorManager.registerConnectors(CONNECTOR_PUBLISHER, [undefined])).to.throw(Error);
  });

});

describe('Registering metadata parsers', () => {

  it('Should throw an error if I register a parser that is not an instance of MetaDataParserBase', () => {
    expect(() => ConnectorManager.registerMetaDataParser(FileDownloader)).to.throw(Error);
  });

  it('Should not throw an error if I register MP4MetaDataParser', () => {
    expect(() => ConnectorManager.registerMetaDataParser(MP4MetaDataParser)).not.to.throw(Error);
  })

});

describe('Getting connector sequence', () => {
  it('Should return connectors in the order they were registered', () => {
    ConnectorManager.unregisterAll();
    ConnectorManager.registerConnectors(CONNECTOR_PUBLISHER, [FacebookPublisher, YouTubePublisher, NetBroadcastingRokuPublisher, VimeoPublisher]);

    let connectors = ConnectorManager.getPublisherSequence();
    expect(connectors[0]).to.equal(FacebookPublisher);
    expect(connectors[1]).to.equal(YouTubePublisher);
    expect(connectors[2]).to.equal(NetBroadcastingRokuPublisher);
    expect(connectors[3]).to.equal(VimeoPublisher);

  });

  it('If I call registerConnectors more than once for the same type, it should include items from both calls', () => {
    ConnectorManager.unregisterAll();
    ConnectorManager.registerConnectors(CONNECTOR_PUBLISHER, [FacebookPublisher, YouTubePublisher]);
    ConnectorManager.registerConnectors(CONNECTOR_PUBLISHER, [NetBroadcastingRokuPublisher, VimeoPublisher]);

    let connectors = ConnectorManager.getPublisherSequence();
    expect(connectors[0]).to.equal(FacebookPublisher);
    expect(connectors[1]).to.equal(YouTubePublisher);
    expect(connectors[2]).to.equal(NetBroadcastingRokuPublisher);
    expect(connectors[3]).to.equal(VimeoPublisher);
  });

  it('If I register the same connector twice, it should exclude duplicates', () => {
    ConnectorManager.unregisterAll();
    ConnectorManager.registerConnectors(CONNECTOR_PUBLISHER, [FacebookPublisher, YouTubePublisher]);
    ConnectorManager.registerConnectors(CONNECTOR_PUBLISHER, [FacebookPublisher, NetBroadcastingRokuPublisher, YouTubePublisher, VimeoPublisher]);

    let connectors = ConnectorManager.getPublisherSequence();
    expect(connectors[0]).to.equal(FacebookPublisher);
    expect(connectors[1]).to.equal(YouTubePublisher);
    expect(connectors[2]).to.equal(NetBroadcastingRokuPublisher);
    expect(connectors[3]).to.equal(VimeoPublisher);
    expect(connectors[4]).to.be.undefined;
  });

});