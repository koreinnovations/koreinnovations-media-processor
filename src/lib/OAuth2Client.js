import Deferred from './Deferred';
import moment from 'moment';
import qs from 'qs';
// var fetch = require('node-fetch');
// import fetch from 'node-fetch';

export default class OAuth2Client {

  constructor(client_id: string, client_secret: string, token_url: string, refreshable: boolean = true) {

    if (!client_id) throw new Error('Client ID missing');
    if (!client_secret) throw new Error('Client secret missing');
    if (!token_url) throw new Error('Token URL missing');

    this._client_id = client_id;
    this._client_secret = client_secret;
    this._token_url = token_url;
    this._refreshable = refreshable;
  }

  getAppCredentials() {
    return {client_id: this._client_id, client_secret: this._client_secret};
  }

  setToken(access_token: string, refresh_token: string = null, token_expiration: integer = null) {
    this._access_token = access_token;
    this._refresh_token = refresh_token;
    this._token_expiration = token_expiration;
  }

  getAccessToken() {
    return this._access_token;
  }

  isExpired() {
    return (this._refreshable && moment().unix() >= this._token_expiration);
  }

  refreshToken() {
    let deferred = new Deferred();

    let request_params = {
      client_id: this._client_id,
      client_secret: this._client_secret,
      refresh_token: this._refresh_token,
      grant_type: 'refresh_token'
    };

    let headers = {'Content-Type': 'application/x-www-form-urlencoded'};

    fetch(this._token_url, {method: 'POST', body: qs.stringify(request_params), headers})
      .then((res) => {
        if (res.status == 200) {
          return res.json();
        }
        else {
          throw  'Response code ' + res.status;
        }
      }, (error) => {
        throw(error);
      })
      .then((responseJSON) => {
        if (responseJSON.access_token) {
          let new_expiration = moment().unix() + parseInt(responseJSON.expires_in);

          this.setToken(responseJSON.access_token, responseJSON.refresh_token, new_expiration);

          deferred.resolve(responseJSON.access_token);

        } else {
          deferred.reject('No access code returned');
        }
      })
      .catch((error) => {
        deferred.reject(error);
      });

    return deferred.promise;
  }

  ensureAccess() {
    let deferred = new Deferred();

    if (!this._access_token) {
      deferred.reject('No access token present');
    } else if (this.isExpired()) {
      return this.refreshToken();
    } else {
      deferred.resolve(this._access_token);
    }

    return deferred.promise;
  }

}
