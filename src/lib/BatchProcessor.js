import moment from 'moment';
import ProcessorSlice from './ProcessorSlice';
import ConnectorManager from './ConnectorManager';
import ConfigurationManager from './ConfigurationManager';
import Deferred from './Deferred';
import {logMessage} from './Util';

export default class BatchProcessor {

  constructor() {
    this._start_time = null;
    this._end_time = null;
    this._slices = [];
    this._slices_processed = 0;
    this._slices_failed = 0;
    this._history = [];
  }

  clearAll() {
    this._slices = [];
    this._slices_processed = 0;
    this._slices_failed = 0;
    this._history = [];
  }

  getSlicesProcessed() {
    return this._slices_processed;
  }

  getSlicesFailed() {
    return this._slices_failed;
  }

  getRemainingSliceCount() {
    return this._slices.length;
  }

  hasMoreSlices() {
    return (this.getRemainingSliceCount() > 0);
  }

  getNextSlice() {
    return this._slices.shift();
  }

  addSlice(slice: ProcessorSlice) {
    if (!(slice instanceof ProcessorSlice)) {
      throw new Error('Only an instance of ProcessorSlice may be passed to BatchProcessor.addSlice()');
    }
    this._slices.push(slice);
  }

  addHistory(slice, path, success, error) {
    this._history.push({slice, path, success, error});
  }

  getHistory() {
    return this._history;
  }

  startTimer() {
    if (!this._start_time) {
      this._start_time = moment();
    }
  }

  endTimer() {
    this._end_time = moment();
  }

  getStartTime() {
    return this._start_time;
  }

  getEndTime() {
    return this._end_time;
  }

  getDuration() {
    return moment.duration(this._end_time.diff(this._start_time));
  }

  hasExceededTimeLimit() {
    let config = ConfigurationManager.getConfig();
    if (!config.max_run_time_seconds || config.max_run_time_seconds === 0) {
      return false;
    }
    let time_elapsed = moment().unix() - this._start_time.unix();
    return (time_elapsed >= config.max_run_time_seconds);
  }

  process() {

    this.startTimer();

    ConnectorManager.validate();

    let deferred = new Deferred();

    let resolve = (reason) => {
      logMessage(reason);
      this.endTimer();
      deferred.resolve();
    };

    let processNextOne = () => {

      if (this.hasExceededTimeLimit()) {
        resolve('Time limit exceeded. Exiting...');
      }
      else if (!this.hasMoreSlices()) {
        resolve('All items in batch processed. Exiting...');
      } else {
        logMessage('Processing next slice in batch');
        let slice = this.getNextSlice();
        let file_path = slice.getFilePath();
        slice.process().then(
          () => {
            this.addHistory(slice, file_path, true);
            this._slices_processed++;
            processNextOne();
          },
          (err) => {
            this.addHistory(slice, file_path, false, err);
            this._slices_failed++;
            processNextOne();
          }
        );
      }

    };

    processNextOne();

    return deferred.promise;

  }


}