import fs from 'fs';
import moment from 'moment';
import _ from 'underscore';
import Opn from 'opn';

import ConfigurationManager from './ConfigurationManager';

export function openURL(url) {
  Opn(url);
}

export function logMessage(message) {
  if (process.env.NODE_ENV != 'test') {
    let date = moment().format('YYYY-MM-DD HH:mm:ss');
    console.log(date, '-', message);
  }
}

export function setupDirectories() {
  let config = ConfigurationManager.getConfig();
  logMessage('setupDirectories()');

  // Make sure each directory exists
  _.each(config.paths, (dir) => {
    if (!fs.existsSync(dir)) {
      fs.mkdirSync(dir);
    }
  });
}

// function to encode file data to base64 encoded string
export function base64EncodeFile(file) {
  // read binary data
  let bitmap = fs.readFileSync(file);
  // convert binary data to base64 encoded string
  return new Buffer(bitmap).toString('base64');
}