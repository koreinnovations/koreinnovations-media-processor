import _ from 'underscore';
import OAuth2Client from './OAuth2Client';
import ConfigurationManager from './ConfigurationManager';

export default class CustomerConfig {

  //TODO: handle updateYouTubeAccessCodeInMySQL(this.uid, this.youtube_token, this.youtube_refresh_token, this.youtube_token_expiration);

  constructor(id, name) {
    this._id = id;
    this._name = name;
    this._oauth2_credentials = {};

    let config = ConfigurationManager.getConfig();

    if (config.oauth2) {
      _.each(config.oauth2, (credentials, service) => {
        if (credentials) {
          this._oauth2_credentials[service] = new OAuth2Client(credentials.client_id, credentials.client_secret, credentials.token_url, (credentials.refreshable !== false));
        }
      });
    }

    this._ftp = {};
  }

  getID() {
    return this._id;
  }

  getName() {
    return this._name;
  }

  getFTPCredentials() {
    return {
      host: this._ftp.server,
      user: this._ftp.username,
      password: this._ftp.password
    };
  }

  getFTPPath() {
    return this._ftp.path;
  }

  setFTPCredentials(server, username, password, path) {
    this._ftp = {
      server,
      username,
      password,
      path
    };
  }

  getOAuth2Instance(service) {
    if (!this._oauth2_credentials[service]) {
      throw new Error(`Service "${service}" not registered as OAuth2 client`);
    }

    return this._oauth2_credentials[service];
  }

}