export default function Deferred() {
  var self = this;
  self.promise = new Promise((resolve, reject) => {
    self.resolve = resolve;
    self.reject = reject;
  });
}