import MetaDataParserBase from './MetaDataParserBase';
import MP4MetaDataParser from './MP4MetaDataParser';

export default {
  MetaDataParserBase,
  MP4MetaDataParser
};