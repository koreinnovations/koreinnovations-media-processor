import MetaDataParserBase from './MetaDataParserBase';
import FileMetaData from '../FileMetaData';
import ffmpeg from 'fluent-ffmpeg';
import _ from 'underscore';
import S from 'string';
import moment from 'moment';

import {logMessage} from '../Util';
import Deferred from '../Deferred';

export default class MP4MetaDataParser extends MetaDataParserBase {

  static parse(file_path) {
    let deferred = new Deferred();

    let callback = (err, metadata) => {

      if (err) {
        logMessage(err);
        deferred.reject(err.message);
      } else {

        let {tags} = metadata.format;

        if (!tags.title) {
          deferred.reject('Cannot determine title of video');
        }
        else {
          let options = {
            title: tags.title,
            description: tags.synopsis,
            speaker: tags.artist,
            category: tags.album,
            date: (tags.date) ? moment(tags.date).format('M/DD/YYYY') : null,
          };
          let file = new FileMetaData(file_path, options);

          if (tags.genre) {
            let split_tags = _.map(tags.genre.split(','), tag => S(tag).trim().s);
            file.setTags(split_tags);
          }

          file.setVideoMetaData(metadata);
          file.setDuration(metadata.format.duration * 1000);
          file.setFilesize(metadata.format.size);

          // logMessage(metadata);

          let stream = _.findWhere(metadata.streams, {codec_name: 'h264'});
          logMessage('stream', stream);
          if (!stream) {
            deferred.reject('Stream is in invalid format');
          } else {
            logMessage('stream is valid');
            file.setDimensions(stream.width, stream.height);
            file.setSampleAspectRatio(stream.sample_aspect_ratio);
            file.setDisplayAspectRatio(stream.display_aspect_ratio);
            deferred.resolve(file);
          }
        }
      }
    };

    ffmpeg.ffprobe(file_path, callback);

    return deferred.promise;
  }

}