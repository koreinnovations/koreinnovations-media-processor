let _defaults = {
  max_run_time_seconds: 3600,
};

let _config = {
  ..._defaults
};

export default class ConfigurationManager {

  static __reset(){
    _config = undefined;
  }

  static clearAll() {
    _config = {
      ..._defaults
    };
  }

  static setDefaults(defaults) {
    _defaults = {
      ..._defaults,
      ...defaults
    };
  }

  static setConfig(config) {
    _config = {
      ..._defaults,
      ..._config,
      ...config
    };
  }

  static getConfig() {
    if (!_config) {
      throw new Error('ConfigurationManager must be initialized for the app to work');
    }
    return {..._config};
  }

}