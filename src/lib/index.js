import BatchProcessor from './BatchProcessor';
import ConfigurationManager from './ConfigurationManager';
import ConnectorManager from './ConnectorManager';
import CustomerConfig from './CustomerConfig';
import Deferred from './Deferred';
import EmailSummary from './EmailSummary';
import FileMetaData from './FileMetaData';
import OAuth2Client from './OAuth2Client';
import ProcessorSlice from './ProcessorSlice';
import {setupDirectories, logMessage, base64EncodeFile} from './Util';
import * as Constants from './Constants';

import connectors from './connectors';
import metadata_parsers from './metadata_parsers';

export default {
  connectors,
  metadata_parsers,

  BatchProcessor,
  ConfigurationManager,
  ConnectorManager,
  CustomerConfig,
  Deferred,
  EmailSummary,
  FileMetaData,
  OAuth2Client,
  ProcessorSlice,

  Constants,
  Util: {
    setupDirectories,
    logMessage,
    base64EncodeFile
  }
};