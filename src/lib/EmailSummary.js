import nodemailer from 'nodemailer';
import path from 'path';
import moment from 'moment';
import sprintf from 'sprintf';
import {logMessage} from './Util';
import Deferred from './Deferred';
import CustomerConfig from './CustomerConfig';
import BatchProcessor from './BatchProcessor';
import ConfigurationManager from './ConfigurationManager';

export default class EmailSummary {
  constructor(customer: CustomerConfig, batch: BatchProcessor) {

    let {email_reports} = ConfigurationManager.getConfig();

    if (!email_reports || !email_reports.sender || !email_reports.transport) {
      throw new Error('email_reports must be defined in config.js and must include sender and transport properties');
    }

    this._sender = email_reports.sender;
    this._transportConfig = email_reports.transport;

    if (!(customer instanceof CustomerConfig)) {
      throw new Error('First argument to constructor must be instance of CustomerConfig');
    }
    if (!(batch instanceof BatchProcessor)) {
      throw new Error('Second argument to constructor must be instance of BatchProcessor');
    }

    this._customer = customer;
    this._batch = batch;
  }

  getBatch() {
    return this._batch;
  }

  getMessageContent() {
    var lines = [];

    let batch = this.getBatch();
    let history = batch.getHistory();
    var total_successes = 0;
    var total_failures = 0;
    let success_slices = [];
    let failure_slices = [];

    history.forEach(history_item => {
      if (history_item.slice.getCustomer() == this._customer) {
        if (history_item.success) {
          total_successes++;
          success_slices.push(history_item);
        } else {
          total_failures++;
          failure_slices.push(history_item);
        }
      }
    });

    if (total_successes == 0 && total_failures == 0) {
      // skip sending a report because nothing was done or even tried
      logMessage('There are no successes or failures to report.');
      return;
    }

    lines.push(sprintf('Processed on behalf of: %s', this._customer.getName()));
    lines.push(sprintf('Started: %s', batch.getStartTime().format('h:mma')));
    lines.push(sprintf('Duration: %s', batch.getDuration().humanize()));
    lines.push('');
    lines.push(sprintf('Files processed successfully: %d', total_successes));
    lines.push(sprintf('Files processed unsuccessfully: %d', total_failures));

    if (total_successes > 0) {
      lines.push('');
      lines.push('');
      lines.push('FILES PROCESSED:');

      success_slices.forEach(slice => {
        let parsed = path.parse(slice.path);
        lines.push('- ' + parsed.base);
        slice.slice.getChronologicalHistory().forEach(item => {
          if (item.success) {
            lines.push('  - ' + item.connector_class.actionDescription());
          } else {
            lines.push('  - ' + item.connector_name + ': ' + item.error);
          }
        });
        lines.push('');
      });
    }

    if (total_failures > 0) {
      lines.push('');
      lines.push('');
      lines.push('FILES WITH ISSUES:');

      failure_slices.forEach(slice => {
        let parsed = path.parse(slice.path);
        lines.push('x ' + parsed.base);
        lines.push('  - Issue: ' + slice.error);
        lines.push('');
      });
    }

    // eslint-disable-next-line
    return lines.join("\n");
  }

  send(recipients: Array) {
    let deferred = new Deferred();

    try {

      if (!recipients || recipients.length == 0) {
        throw new Error('Recipients must be an array of email addresses');
      }

      let message = this.getMessageContent();

      if (message && message.length > 0) {
        logMessage('Message contents', message);

        // create reusable transporter object using the default SMTP transport
        let transporter = nodemailer.createTransport(this._transportConfig);

        // setup e-mail data with unicode symbols
        let mailOptions = {
          from: this._sender,
          to: recipients.join(', '), // list of receivers
          subject: 'Media Processor - Batch Summary ' + moment().format('M/D h:mma'), // Subject line
          text: message, // plaintext body
          debug: true,
          logger: true
        };

        // send mail with defined transport object
        transporter.sendMail(mailOptions, (error, info) => {
          if (error) {
            logMessage('Encountered error sending mail', error);
            deferred.reject(error);
          } else {
            logMessage('Message recipients:', recipients.join(', '));
            logMessage('Message sent: ' + info.response);
            deferred.resolve({sent: true});
          }
        });
      } else {
        deferred.resolve({sent: false});
      }
    } catch (ex) {
      deferred.reject(ex.message);
    }

    return deferred.promise;
  }
}
