import path from 'path';
import CustomerConfig from './CustomerConfig';
import ConnectorManager from './ConnectorManager';
import FileMetaData from './FileMetaData';
import Deferred from './Deferred';
import {logMessage} from './Util';
import {CONNECTOR_FETCHER, CONNECTOR_PREPARER, CONNECTOR_PUBLISHER, CONNECTOR_FINALIZER} from './Constants';

export default class ProcessorSlice {

  constructor(customer: CustomerConfig, file_raw: Object) {

    if (!(customer instanceof CustomerConfig)) {
      throw new Error('First parameter must be an instance of the CustomerConfig class');
    }
    if (file_raw instanceof FileMetaData) {
      throw new Error('Second parameter cannot be an instance of FileMetaData. It must be a JS object with a path property');
    }
    if (!file_raw.path) {
      throw new Error('Second parameter must be a JS object with a path property');
    }

    this._chronological_history = [];
    this._history = {};

    this._customer = customer;
    this._file_raw = file_raw;

    let parsed = path.parse(file_raw.path);

    this._filename = parsed.base;

  }

  getCustomer() {
    return this._customer;
  }

  getFilePath() {
    return this._file_raw.path;
  }

  log(message) {
    logMessage(this.getCustomer().getName() + ' - ' + this._filename + ' - ' + message);
  }

  getChronologicalHistory() {
    return this._chronological_history;
  }

  getHistory(type) {
    return this._history[type];
  }

  addHistory(type, connector_name, connector_class, success, error) {
    if (!this._history[type]) {
      this._history[type] = [];
    }
    let record = {connector_name, connector_class, success, error};
    this._history[type].push(record);
    this._chronological_history.push(record);
  }

  parseMetaData() {
    let deferred = new Deferred();

    let parser = ConnectorManager.getMetaDataParser();
    parser.parse(this._file_raw.path).then(
      file_metadata => {
        this._file_metadata = file_metadata;
        deferred.resolve();
      },
      (err) => deferred.reject(err)
    );

    return deferred.promise;
  }

  _runSequence(type, sequence, constructor, quit_on_any_failure = true) {
    let deferred = new Deferred();

    var successes = 0;
    var failures = 0;

    try {
      let processNextConnector = () => {
        try {
          if (sequence.length > 0) {
            let module = sequence.shift();
            let connector_name = module.name;
            this.log(`${type}: ${connector_name}`);
            let connector = constructor(module);
            connector.process().then(
              () => {
                successes++;
                this.addHistory(type, connector_name, module, true);
                processNextConnector();
              },
              (err) => {
                failures++;
                this.addHistory(type, connector_name, module, false, err);
                if (quit_on_any_failure) {
                  this.log(`Failing connector ${connector_name} because of ${err}`);
                  deferred.reject(err);
                } else {
                  this.log(`Connector ${connector_name} failed because of ${err}, but continuing to process other connectors`);
                  processNextConnector();
                }
              }
            );
          } else {
            this.log(`Ran out of ${type} connectors.  Successes=${successes}, failures=${failures}`);
            if (successes > 0 || failures == 0) {
              this.log(`Resolving ${type} sequence`);
              deferred.resolve();
            } else {
              this.log(`Rejecting ${type} sequence`);
              deferred.reject(`None of the ${type} connectors succeeded. Rejecting promise`);
            }
          }
        } catch (ex) {
          failures++;
          if (quit_on_any_failure) {
            console.log('_runSequence inner exception encountered', ex);
            deferred.reject(ex.message);
          } else {
            processNextConnector();
          }
        }
      };

      processNextConnector();
    } catch (ex) {
      console.log('_runSequence outer exception encountered', ex);
      deferred.reject(ex.message);
    }

    return deferred.promise;
  }

  fetch() {
    let sequence = ConnectorManager.getFetcherSequence();
    let constructor = (module) => (new module(this._customer, this._file_raw));
    return this._runSequence(CONNECTOR_FETCHER, sequence, constructor);
  }

  prepare() {
    let sequence = ConnectorManager.getPreparerSequence();
    let constructor = (module) => (new module(this._customer, this._file_raw, this._file_metadata));
    return this._runSequence(CONNECTOR_PREPARER, sequence, constructor);
  }

  publish() {
    let sequence = ConnectorManager.getPublisherSequence();
    let constructor = (module) => (new module(this._customer, this._file_raw, this._file_metadata));
    return this._runSequence(CONNECTOR_PUBLISHER, sequence, constructor, false);
  }

  finalize() {
    let sequence = ConnectorManager.getFinalizerSequence();
    let constructor = (module) => (new module(this._customer, this._file_raw, this._file_metadata));
    return this._runSequence(CONNECTOR_FINALIZER, sequence, constructor);
  }

  process() {

    ConnectorManager.validate();

    let deferred = new Deferred();

    this.log('Running fetchers...');
    try {

      let handleError = (err) => {
        deferred.reject(err);
        throw(err);
      };

      this.fetch()
        .then(
          () => {
            this.log('Finished fetching. Parsing metadata...');
            return this.parseMetaData();
          },
          handleError)
        .then(() => {
          this.log('Finished parsing metadata. Preparing...');
          return this.prepare();
        }, handleError)
        .then(() => {
          this.log('Finished preparing. Publishing...');
          return this.publish();
        }, handleError)
        .then(() => {
          this.log('Finished publishing. Finalizing...');
          return this.finalize();
        }, handleError)
        .then(() => {
          this.log(('Finished finalizing. Slice complete'));
          deferred.resolve();
        }, handleError)
        .catch(err => {
          this.log('ProcessorSlice failing due to error (chained catch)');
          this.log(err);
          deferred.reject(err);
        });
    } catch (err) {
      this.log('ProcessorSlice failing due to error (wrapper)');
      this.log(err);
      deferred.reject(err);
    }

    return deferred.promise;
  }

}