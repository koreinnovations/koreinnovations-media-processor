export const CONNECTOR_FETCHER = 'CONNECTOR_FETCHER';
export const CONNECTOR_PREPARER = 'CONNECTOR_PREPARER';
export const CONNECTOR_PUBLISHER = 'CONNECTOR_PUBLISHER';
export const CONNECTOR_FINALIZER = 'CONNECTOR_FINALIZER';