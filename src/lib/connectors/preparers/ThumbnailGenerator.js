import PreparerConnectorBase from './PreparerConnectorBase';
import Deferred from '../../Deferred';
import ffmpeg from 'fluent-ffmpeg';
import ConfigurationManager from '../../ConfigurationManager';
import {setupDirectories} from '../../Util';

export default class ThumbnailGenerator extends PreparerConnectorBase {

  static actionDescription() {
    return 'generated still shot from video file';
  }

  process() {
    let deferred = new Deferred();
    let config = ConfigurationManager.getConfig();

    try {
      setupDirectories();

      this.log('generateThumbnails()');
      let file_metadata = this.getFileMetaData();

      let valid_times = file_metadata.getValidTimes();

      if (valid_times.length == 0) {
        deferred.reject('Video is too short to generate thumbnails');
      } else {

        // Get a version of the filename safer for the web
        // and use this version to generate the thumbnails
        let new_filename = file_metadata.getSanitizedFilename();
        let full_path = file_metadata.getFullPath();
        let dimensions = file_metadata.getThumbnailDimensionsString();

        this.log('encoding screenshots');
        ffmpeg(full_path)
          .on('filenames', (filenames) => {
            this.log('filenames generated');
            file_metadata.setThumbnails(filenames);
          })
          .on('error', (err) => {
            deferred.reject(err);
          })
          .on('end', () => {
            deferred.resolve(file_metadata);
          })
          .screenshots({
            folder: config.paths.temp,
            filename: new_filename + '_%wx%h_%0i.png',
            timestamps: valid_times,
            size: dimensions
          });

        this.log('extractImage: ' + file_metadata.getTitle());
      }
    } catch (ex) {
      console.log(ex);
      deferred.reject(ex);
      throw(ex);
    }

    return deferred.promise;

  }

}