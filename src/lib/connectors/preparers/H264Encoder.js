import path from 'path';
import fs from 'fs';
import ffmpeg from 'fluent-ffmpeg';
import Deferred from '../../Deferred';
import PreparerConnectorBase from './PreparerConnectorBase';
import ConfigurationManager from '../../ConfigurationManager';
import {setupDirectories} from '../../Util';

export default class H264Encoder extends PreparerConnectorBase {

  static actionDescription() {
    return 'encoded file in h264 (mp4) format';
  }

  process() {

    let config = ConfigurationManager.getConfig();

    setupDirectories();

    let file = this.getFile();
    let file_metadata = this.getFileMetaData();
    let file_path = file.path;
    let parsed = path.parse(file_path);

    file.original_path = file_path;



    let deferred = new Deferred();

    if (parsed.ext.toLowerCase() == '.mpg' || parsed.ext.toLowerCase() == '.mov') {
      this.log(file_path, 'is in mpg or mov format.  Encoding to mp4...');
      let new_path = parsed.dir + '/' + parsed.name + '.mp4';
      file.path = new_path;
      file_metadata.setFullPath(new_path);

      var command = ffmpeg(file_path)
        .outputOptions([
          '-vcodec h264',
          '-b 3500k',
          '-strict -2',
          '-vf yadif=0:-1:0'

        ])
        //.input(full_path)
        .audioCodec('aac');

      command.on('error', (err) => {
        deferred.reject(err);
      });

      command.on('end', () => {
        this.log('Finished with output file', parsed.name, '.mp4');
        let new_path = path.resolve(config.paths.originals + '/' + parsed.name + parsed.ext);
        fs.renameSync(file_path, new_path);
        deferred.resolve(new_path);
      });

      command.save(new_path);
    } else {


      deferred.resolve(file_path);
    }

    return deferred.promise;
  }

}