import ConnectorBase from './ConnectorBase';
import CustomerConfig from '../CustomerConfig';
import FileMetaData from '../FileMetaData';

export default class ConnectorBaseWithMetaData extends ConnectorBase {

  constructor(customer: CustomerConfig, file_raw: Object, file_metadata: FileMetaData) {
    super(customer);

    if (!(file_raw instanceof Object)) {
      throw new Error('ConnectorBaseWithMetaData: file_raw must be an object');
    }

    if (!(file_raw.path)) {
      throw new Error('ConnectorBaseWithMetaData: file_raw must have a "path" property');
    }

    if (!(file_metadata instanceof FileMetaData)) {
      throw new Error('ConnectorBaseWithMetaData: file_metadata must be instance of FileMetaData');
    }

    this._file_raw = file_raw;
    this._file_metadata = file_metadata;
  }

  getFile() {
    return this._file_raw;
  }

  getFileMetaData() {
    return this._file_metadata;
  }

}