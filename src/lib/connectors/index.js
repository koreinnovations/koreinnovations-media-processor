import fetchers from './fetchers';
import preparers from './preparers';
import publishers from './publishers';
import finalizers from './finalizers';

export default {
  fetchers,
  preparers,
  publishers,
  finalizers,
};