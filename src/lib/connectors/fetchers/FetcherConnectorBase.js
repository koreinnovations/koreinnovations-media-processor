import ConnectorBase from '../ConnectorBase';
import CustomerConfig from '../../CustomerConfig';

export default class FetcherConnectorBase extends ConnectorBase {
  constructor(customer: CustomerConfig, file_raw: Object) {
    super(customer);

    if (!(file_raw instanceof Object)) {
      throw new Error('FetcherConnectorBase: file_raw must be an object');
    }

    if (!(file_raw.path)) {
      throw new Error('FetcherConnectorBase: file_raw must have a "path" property');
    }

    this._file_raw = file_raw;
  }

  getFile() {
    return this._file_raw;
  }

  process() {
    throw new Error('This method has not yet been implemented for this class');
  }
}