import FTPClient from 'ftp';
import FetcherConnectorBase from './FetcherConnectorBase';
import Deferred from '../../Deferred';
import ConfigurationManager from '../../ConfigurationManager';
import CustomerConfig from '../../CustomerConfig';
import fs from 'fs';
import moment from 'moment';

export default class FileDownloader extends FetcherConnectorBase {

  static actionDescription() {
    return 'downloaded file from FTP server';
  }

  constructor(customer: CustomerConfig, file_raw: Object) {
    super(customer, file_raw);

    let config = ConfigurationManager.getConfig();

    if (!config.file_downloader || !config.file_downloader.temp_downloads_directory) {
      throw new Error('file_downloader.temp_downloads_directory must be defined in config.js');
    }
  }

  process() {
    let deferred = new Deferred();

    try {
      let config = ConfigurationManager.getConfig();
      let ftp_credentials = this.getCustomer().getFTPCredentials();
      let ftp_path = this.getCustomer().getFTPPath();
      let file = this.getFile();

      let filename = file.name;
      let file_path = file.path;

      let destination_path = config.file_downloader.temp_downloads_directory + '/' + filename;

      let ftp_connection = new FTPClient();

      ftp_connection.on('error', (err) => {
        this.log(`Encountered error connecting to FTP server ${ftp_credentials.host}. Error is ${err.message}`);

        deferred.reject(err);
      });

      // This is the callback that will be fired when the FTP connection
      // has been established
      ftp_connection.on('ready', () => {

        let base_path = ftp_path || '/';
        let target_path = base_path + '/' + file_path;

        this.log(`Downloading ${target_path}`);

        ftp_connection.size(target_path, (err, file_size_bytes) => {
          if (err) {
            this.log('FTP error: ' + err.message);
            deferred.reject(err);
            return;
          }

          ftp_connection.get(target_path, (err, stream) => {
            if (err) {
              this.log('FTP error: ' + err.message);
              deferred.reject(err);
              return;
            }

            try {
              var total_received = 0;
              var last_update = 0;
              stream.pipe(fs.createWriteStream(destination_path));
              stream.on('data', (chunk) => {
                total_received += chunk.length;
                let fraction = total_received / file_size_bytes;
                if (moment().unix() - last_update > 5) {
                  let percent = (fraction * 100).toFixed(2);
                  this.log(`Received ${percent}% for ${filename}.`);
                  last_update = moment().unix();
                }
              });
              stream.once('end', () => {
                this.log(`Finished downloading ${target_path}`);
                ftp_connection.end();
                file.ftp_path = file.path;
                file.path = destination_path;
                deferred.resolve(destination_path);
              });
            }
            catch (ex) {
              console('error', ex.message);
              deferred.reject(ex);
            }
          });
        });

      });

      // connect to FTP server
      ftp_connection.connect(ftp_credentials);
    } catch (ex) {
      console.log('Error', ex.message);
      deferred.reject(ex);
    }

    return deferred.promise;
  }

}