import PublisherConnectorBase from './PublisherConnectorBase';

export default class NetBroadcastingRokuPublisher extends PublisherConnectorBase {

  static actionDescription() {
    return 'published to NetBroadcasting Roku content manager';
  }

  process() {
    throw new Error('This connector has not yet been implemented.');
  }

}