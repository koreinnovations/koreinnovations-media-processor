import moment from 'moment';
import fs from 'fs';
import Lien  from 'lien';
import qs from 'qs';

import PublisherConnectorBase from './PublisherConnectorBase';
import FileMetaData from '../../FileMetaData';
import ConfigurationManager from '../../ConfigurationManager';
import {logMessage, openURL} from '../../Util';
import Deferred from '../../Deferred';
import OAuth2Client from '../../OAuth2Client';
import Youtube from 'youtube-api';
import prettyBytes from 'pretty-bytes';


export function setupYouTubeAuthentication(YOUTUBE_TOKEN_PATH) {

  let {youtube} = ConfigurationManager.getConfig().oauth2;

  let getDeviceCode = () => {
    let deferred = new Deferred();

    let request_params = {
      client_id: youtube.client_id,
      scope: 'https://gdata.youtube.com https://www.googleapis.com/auth/youtube'
    };
    let headers = {'Content-Type': 'application/x-www-form-urlencoded'};

    console.log('fetching token with device code');
    fetch('https://accounts.google.com/o/oauth2/device/code', {
      method: 'POST',
      body: qs.stringify(request_params),
      headers
    })
      .then((res) => {
        if (res.status == 200) {
          deferred.resolve(res.json());
        }
        else {
          throw  'Response code ' + res.status;
        }
      }, (error) => {
        console.warn(error.message);
        throw(error);
      })
      .catch((error) => {
        console.log(error);
        deferred.reject(error);
      });

    return deferred.promise;
  };

  let promptForAuthentication = () => {
    getDeviceCode().then(watchForAuthorization);
  };

  let checkGoogleServerForCompletedAuthorization = (device_code) => {
    let deferred = new Deferred();

    let request_params = {
      client_id: youtube.client_id,
      client_secret: youtube.client_secret,
      grant_type: 'http://oauth.net/grant_type/device/1.0',
      code: device_code
    };
    let headers = {'Content-Type': 'application/x-www-form-urlencoded'};

    fetch('https://accounts.google.com/o/oauth2/token', {method: 'POST', body: qs.stringify(request_params), headers})
      .then((res) => {
        if (res.status == 200) {
          deferred.resolve(res.json());
        }
        else {
          console.log('---------');
          throw  'Response code ' + res.status;
        }
      }, (error) => {
        console.warn(error.message);
        throw(error);
      });

    return deferred.promise;
  };

  let watchForAuthorization = (res) => {
    let code = res.device_code;
    let user_code = res.user_code;
    let verification_url = res.verification_url;

    if (code) {

      console.log('To complete the authorization procces please log in to your youtube account and go this url ' + verification_url
        + ' and use this code ' + user_code);

      let timer = setInterval(function () {

        checkGoogleServerForCompletedAuthorization(code)
          .then((res) => {
            if (res.access_token) {
              clearInterval(timer);
              console.log('YouTube access has been set up.  Finishing.');
              fs.writeFileSync(YOUTUBE_TOKEN_PATH, JSON.stringify(res));
            } else {
              console.log('Pending authorization...');
            }

          });
      }, 5000);
    } else {
      console.log('No device code was received from Google');
    }
  };

  if (fs.existsSync(YOUTUBE_TOKEN_PATH)) {

    console.log('YouTube access has already been set up.  Renewing token...');

    let existing_token = JSON.parse(fs.readFileSync(YOUTUBE_TOKEN_PATH, 'utf8'));
    let config = ConfigurationManager.getConfig();
    let client = new OAuth2Client(config.oauth2.youtube.client_id, config.oauth2.youtube.client_secret, config.oauth2.youtube.token_url);

    client.setToken(existing_token.access_token, existing_token.refresh_token, existing_token.token_expiration);

    client.ensureAccess().then(
      () => {
        let new_token = {
          access_token: client._access_token,
          refresh_token: client._refresh_token,
          token_expiration: client._token_expiration,
        };

        console.log(`Renewed token and wrote to ${YOUTUBE_TOKEN_PATH}`);
        fs.writeFileSync(YOUTUBE_TOKEN_PATH, JSON.stringify(new_token));
      },
      () => {
        console.log('Failed to renew token.  Let\'s get a brand-new token using your credentials.');
        fs.unlinkSync(YOUTUBE_TOKEN_PATH);
        promptForAuthentication();
      }
    );

  } else {
    console.log('No token exists for YouTube.  Login required.');
    promptForAuthentication();
  }
}

export function getYouTubeMetaData(file_metadata: FileMetaData) {
  let filepath = file_metadata.getFullPath();

  return {
    resource: {
      // Video title and description
      snippet: {
        title: file_metadata.getTitle(),
        description: file_metadata.getDescription(),
        tags: file_metadata.getTags()
      },
      status: {
        privacyStatus: 'public'
      }
    },
    // This is for the callback function
    part: 'snippet,status',

    // Create the readable stream to upload the video
    media: {
      body: fs.createReadStream(filepath)
    }
  };
}

export default class YouTubePublisher extends PublisherConnectorBase {

  static actionDescription() {
    return 'published to YouTube';
  }

  static getOAuth2Token(local_token_file_path) {
    let config = ConfigurationManager.getConfig();
    let deferred = new Deferred();
    let {client_id, client_secret} = config.oauth2.youtube;

    try {
      // This will throw an error if the file doesn't exist
      fs.accessSync(local_token_file_path);

      let contents = fs.readFileSync(local_token_file_path, 'utf8');
      logMessage('Read YouTube token from file');
      let token_from_file = JSON.parse(contents);
      if (token_from_file.access_token) {
        deferred.resolve(token_from_file);
      } else {
        throw new Error('Access token not found in YouTube token file');
      }
    } catch (ex) {

      // Init the lien server
      let server = new Lien({host: 'localhost', port: 5000});

      // Here we're waiting for the OAuth2 redirect containing the auth code
      server.addPage('/oauth2callback', (lien) => {

        let {code} = lien.query;

        logMessage('Trying to get the token using the following code: ' + code);

        // Get access token
        let token_url = 'https://accounts.google.com/o/oauth2/token';
        let request_params = {
          client_id,
          client_secret,
          code,
          redirect_uri: 'http://localhost:5000/oauth2callback',
          grant_type: 'authorization_code'
        };
        let headers = {'Content-Type': 'application/x-www-form-urlencoded'};

        fetch(token_url, {method: 'POST', body: qs.stringify(request_params), headers})
          .then((res) => {
            if (res.status == 200) {
              return res.json();
            }
            else {
              throw  'Response code ' + res.status;
            }
          }, (error) => {
            console.warn(error.message);
            throw(error);
          })
          .then((responseJSON) => {
            try {
              if (responseJSON.access_token) {
                let new_expiration = moment().unix() + parseInt(responseJSON.expires_in);

                let new_token = {
                  access_token: responseJSON.access_token,
                  refresh_token: responseJSON.refresh_token,
                  token_expiration: new_expiration
                };

                console.log('writing file to ', local_token_file_path);
                fs.writeFileSync(local_token_file_path, JSON.stringify(new_token));

                deferred.resolve(new_token);

              } else {
                deferred.reject('No access code returned');
              }
            } catch (ex) {
              console.log(ex);
              deferred.reject(ex);
            }
          })
          .catch((error) => {
            console.log(error);
            deferred.reject(error);
          });

      });


      let auth_url = 'https://accounts.google.com/o/oauth2/v2/auth';
      let auth_params = {
        client_id,
        redirect_uri: 'http://localhost:5000/oauth2callback',
        response_type: 'code',
        access_type: 'offline',
        prompt: 'consent',
        oauth: 1,
        scope: 'https://gdata.youtube.com https://www.googleapis.com/auth/youtube https://www.googleapis.com/auth/youtubepartner'
      };

      // Open the authentication url in the default browser
      openURL(auth_url + '?' + qs.stringify(auth_params));

    }

    return deferred.promise;
  }

  constructor(customer, file_raw, file_metadata) {
    super(customer, file_raw, file_metadata);

    try {
      customer.getOAuth2Instance('youtube');
    } catch (ex) {
      throw new Error('YouTube OAuth2 credentials not provided');
    }

  }

  uploadYoutubeVideo() {

    let deferred = new Deferred();

    try {
      let file_metadata = this.getFileMetaData();
      let filepath = this.getFileMetaData().getFullPath();
      let filename = filepath.replace(/.+\//, '');
      let customer = this.getCustomer();
      let youtube = customer.getOAuth2Instance('youtube');


      let youtube_token = youtube.getAccessToken();
      let {client_id, client_secret} = youtube.getAppCredentials();

      this.log(`YouTube token: ${youtube_token}`);
      let authentication = {
        type: 'oauth',
        client_id,
        client_secret,
        redirect_url: 'http://localhost:5000/oauth2callback',
        access_token: youtube_token
      };

      Youtube.authenticate(authentication)
        .setCredentials({access_token: youtube_token});

      let youtube_video_data = getYouTubeMetaData(file_metadata);

      let interval = setInterval(() => {
        this.log(`${prettyBytes(youtube_insert_api_request.req.connection._bytesDispatched)} bytes uploaded.`);
      }, 5000);

      let youtube_insert_api_request = Youtube.videos.insert(youtube_video_data, (err) => {
        clearInterval(interval);
        if (err) {
          deferred.reject(err);
        }
        else {
          deferred.resolve(filename);
        }
      });
    } catch (ex) {
      deferred.reject(ex.message);
    }

    return deferred.promise;
  }

  process() {

    this.log('uploading file to youtube');
    let deferred = new Deferred();

    try {
      let customer = this.getCustomer();
      let oauth2 = customer.getOAuth2Instance('youtube');
      oauth2.ensureAccess().then(() => {

        // Upload to youtube api
        this.uploadYoutubeVideo()
          .then(() => {
              deferred.resolve();
            },
            (err) => {
              this.log('Error uploading file to youtube');
              deferred.reject(err);
            }
          )
          .catch((error) => {
            this.log(`Exception caught: ${error.message}`);
            deferred.reject(error);
          });

      }, (err) => deferred.reject(err));
    } catch (ex) {
      deferred.reject(ex.message);
    }

    return deferred.promise;

  }

}