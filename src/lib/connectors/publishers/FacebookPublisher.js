import fs from 'fs';
import PublisherConnectorBase from './PublisherConnectorBase';
import FileMetaData from '../../FileMetaData';
import Deferred from '../../Deferred';
import ConfigurationManager from '../../ConfigurationManager';
import FB from 'fb';

import prompt from 'prompt';
import FormData from 'form-data';


export function setupFacebookAuthentication(FACEBOOK_TOKEN_PATH) {

  let config_values = ConfigurationManager.getConfig();
  let {client_id, client_secret, client_token, token_url, device_url} = config_values.oauth2.facebook;
  let pending_authorization_error_code = 1349174;

  let getDeviceCode = () => {
    let deferred = new Deferred();

    let form = new FormData();
    form.append('access_token', client_id + '|' + client_token);
    form.append('scope', 'manage_pages,publish_pages');

    fetch(device_url, {method: 'POST', body: form})
      .then((res) => {
        if (res.status == 200) {
          return res.json();
        }
        else {
          throw 'Response code ' + res.status;
        }
      }, (error) => {
        throw (error);
      })
      .then((responseJSON) => {
        if (responseJSON.code) {

          let code_params = {
            code: responseJSON.code,
            user_code: responseJSON.user_code,
            verification_url: responseJSON.verification_uri,
            expires_in: responseJSON.expires_in,
            interval: responseJSON.interval // Should end up as 5 seconds
          };

          deferred.resolve(code_params);

        }
        else {
          deferred.reject('No access code returned');
        }
      })
      .catch((error) => {
        deferred.reject(error);
      });

    return deferred.promise;
  };

  let checkForCompletedAuthorization = (device_code) => {
    let deferred = new Deferred();

    let form = new FormData();
    form.append('access_token', client_id + '|' + client_token);
    form.append('code', device_code);

    fetch(token_url, {method: 'POST', body: form})
      .then((res) => {
        // Facebook returns a 400 status if the user has not authorized the app yet
        if (res.status == 200 || res.status == 400) {
          return res.json();
        }
        else {
          throw 'Response code ' + res.status;
        }
      }, (error) => {
        throw (error);
      })
      .then((responseJSON) => {
        if (responseJSON) {
          deferred.resolve(responseJSON);
        } else {
          deferred.reject('No token was returned');
        }
      })
      .catch((error) => {
        deferred.reject(error);
      });

    return deferred.promise;
  };

  let waitForUserAuthenticationFromExternalSource = (code_params) => {
    let deferred = new Deferred();

    console.log('To complete the authorization process please log in to your Facebook account and go this url ' + code_params.verification_url
      + ' and use this code ' + code_params.user_code);


    let timer = setInterval(function () {
      checkForCompletedAuthorization(code_params.code)
        .then((res) => {
          if (res.hasOwnProperty('access_token')) {
            try {
              clearInterval(timer);
              // Setup FB with the user token
              FB.setAccessToken(res.access_token);
              // fs.writeFileSync(FACEBOOK_TOKEN_PATH, JSON.stringify(new_token));

              deferred.resolve();
            } catch (ex) {
              clearInterval(timer);
              deferred.reject(ex);
            }
          }
          else if (res.error.error_subcode == pending_authorization_error_code) {
            console.log('Pending authorization...');
          }
          else { // Something went wrong, abort
            throw (res.error.error_user_msg);
          }

        })
        .catch((error) => {
          clearInterval(timer);
          deferred.reject(error);
        });
    }, code_params.interval * 1000);

    return deferred.promise;

  };

  let getFacebookPagesBelongingToUser = () => {
    let deferred = new Deferred();
    FB.api('me/accounts', (res) => {
        if (!res || res.error) {
          deferred.reject(res.error);
          // throw (!res ? 'error occurred' : res.error);
        }
        console.log('possible tokens', res);
        deferred.resolve(res);
      }
    );

    return deferred.promise;
  };

  let determineWhichPageToGetTokenFor = () => {
    let deferred = new Deferred();

    console.log('determineWhichPageToGetTokenFor');

    // FB call to get user's list of accounts
    getFacebookPagesBelongingToUser()
      .then(
        (pagesResult) => promptToChooseFacebookPage(pagesResult),
        (ex) => console.warn(ex)
      )
      .then(() => {
        deferred.resolve();
      })
      .catch((error) => {
        console.log(error);
        deferred.reject(error);
      });

    return deferred.promise;
  };

  let promptToChooseFacebookPage = (pagesResult) => {

    let available_facebook_pages = pagesResult.data;

    console.log('Please select which facebook page you would like to connect.');

    let i = 0;
    let range = /^\d+$/;

    available_facebook_pages.forEach(function (page) {
      console.log('[' + (++i) + '] ' + page.name);
    });

    prompt.start();

    prompt.get({
        properties: {
          page: {
            pattern: range,
            message: 'Please choose a number between 1 and ' + i,
            required: true
          },
        }
      },
      () => {
        let page_index = i - 1;
        if (page_index < 1 || page_index > available_facebook_pages.length) {
          console.log('Number is out of range');
        } else {
          let chosen_page = available_facebook_pages[page_index];
          console.log(`You chose ${chosen_page.name}`);
          exchangeShortTermTokenForLongTermToken(chosen_page.access_token).then(
            (long_term_access_token) => {
              fs.writeFileSync(FACEBOOK_TOKEN_PATH, long_term_access_token);
              console.log(`Wrote long term Facebook access token to ${FACEBOOK_TOKEN_PATH}`);
            }
          );
        }
      }
    );
  };

  let exchangeShortTermTokenForLongTermToken = (access_token) => {
    let deferred = new Deferred();

    console.log('exchangeShortTermTokenForLongTermToken');

    FB.api('oauth/access_token', {
      client_id,
      client_secret,
      grant_type: 'fb_exchange_token',
      fb_exchange_token: access_token
    }, (res) => {
      if (!res || res.error) {
        console.log(!res ? 'error occurred' : res.error);
        deferred.reject(res.error);
      } else {
        let long_term_access_token = res.access_token;
        deferred.resolve(long_term_access_token);
      }
    });

    return deferred.promise;
  };

  let getFacebookDeviceAuthentication = () => {
    let deferred = new Deferred();

    getDeviceCode()
      .then(waitForUserAuthenticationFromExternalSource)
      .then(determineWhichPageToGetTokenFor)
      .then(() => {
        deferred.resolve();
      })
      .catch((error) => {
        deferred.reject(error);
      });


    return deferred.promise;
  };

  if (fs.existsSync(FACEBOOK_TOKEN_PATH)) {
    console.log('Facebook access has been set up. If the app yields a Facebook Access Denied error, remove the .facebook file and re-run this script.');
  } else {
    getFacebookDeviceAuthentication().then(
      () => console.log('Facebook access has been set up.  Finishing.'),
      (error) => console.warn('There was a problem obtaining Facebook access. ' + error)
    );
  }
}

export function getFacebookMetaData(file_metadata: FileMetaData) {
  let filepath = file_metadata.getFullPath();

  return {

    title: file_metadata.getTitle(),
    description: file_metadata.getDescription(),
    // Create the readable stream to upload the video
    source: fs.createReadStream(filepath),
    content_category: 'FAMILY' //file_metadata.getTags()

  };
}

export default class FacebookPublisher extends PublisherConnectorBase {

  static actionDescription() {
    return 'published to Facebook';
  }

  constructor(customer, file_raw, file_metadata) {
    super(customer, file_raw, file_metadata);

    try {
      customer.getOAuth2Instance('facebook');
    } catch (ex) {
      throw new Error('Facebook OAuth2 credentials not provided');
    }

  }

  uploadFacebookVideo() {

    let deferred = new Deferred();

    try {
      let file_metadata = this.getFileMetaData();
      let customer = this.getCustomer();
      let facebook = customer.getOAuth2Instance('facebook');
      let facebook_token = facebook.getAccessToken();

      FB.setAccessToken(facebook_token);

      let facebook_video_data = getFacebookMetaData(file_metadata);

      FB.api('me/videos', 'post', facebook_video_data, (res) => {
        if (!res || res.error) {
          deferred.reject(res.error);
        }
        else {
          deferred.resolve();
        }
      });

    } catch (ex) {
      deferred.reject(ex.message);
    }

    return deferred.promise;
  }

  process() {

    this.log('uploading file to facebook');
    let deferred = new Deferred();

    try {
      let customer = this.getCustomer();
      let oauth2 = customer.getOAuth2Instance('facebook');
      oauth2.ensureAccess().then(() => {

        // Upload to facebook api
        this.uploadFacebookVideo()
          .then(() => {
              deferred.resolve();
            },
            (err) => {
              this.log('Error uploading file to facebook');
              deferred.reject(err);
            }
          )
          .catch((error) => {
            this.log(`Exception caught: ${error.message}`);
            deferred.reject(error);
          });

      }, (err) => deferred.reject(err));
    } catch (ex) {
      deferred.reject(ex.message);
    }

    return deferred.promise;

  }

}
