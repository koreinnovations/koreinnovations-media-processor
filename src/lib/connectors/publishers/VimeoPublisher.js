import PublisherConnectorBase from './PublisherConnectorBase';
import fs from 'fs';
import FileMetaData from '../../FileMetaData';
import Deferred from '../../Deferred';
// eslint-disable-next-line
import vimeo from 'vimeo';

export function getVimeoMetaData(file_metadata: FileMetaData) {
  let filepath = file_metadata.getFullPath();

  return {
    resource: {
      // Video title and description
      snippet: {
        title: file_metadata.getTitle(),
        description: file_metadata.getDescription(),
        tags: file_metadata.getTags()
      },
      status: {
        privacyStatus: 'public'
      }
    },
    // This is for the callback function
    part: 'snippet,status',

    // Create the readable stream to upload the video
    media: {
      body: fs.createReadStream(filepath)
    }
  };
}

export default class VimeoPublisher extends PublisherConnectorBase {

  static actionDescription() {
    return 'published to Vimeo';
  }

  constructor(customer, file_raw, file_metadata) {
    super(customer, file_raw, file_metadata);

    try {
      customer.getOAuth2Instance('vimeo');
    } catch (ex) {
      throw new Error('Vimeo OAuth2 credentials not provided');
    }

  }

  uploadVimeoVideo() {

    let deferred = new Deferred();

    try {
      let file_metadata = this.getFileMetaData();
      let filepath = this.getFileMetaData().getFullPath();
      let filename = filepath.replace(/.+\//, '');
      let customer = this.getCustomer();
      let vimeo = customer.getOAuth2Instance('vimeo');

      let vimeo_token = vimeo.getAccessToken();
      let {client_id, client_secret} = vimeo.getAppCredentials();

      this.log(`Vimeo token: ${vimeo_token}`);
      let authentication = {
        client_id,
        client_secret,
        vimeo_token
      };
      lib = new Vimeo(authentication);


      // var url = lib.buildAuthorizationEndpoint(redirect_uri, scopes, state);


      lib.streamingUpload(filepath, function (error, body, status_code, headers) {
        if (error) {
          throw error;
        }

        lib.request(headers.location, function (error, body) {
          console.log(body);
        });
      }, function (upload_size, file_size) {
        console.log('You have uploaded ' + Math.round((uploaded_size / file_size) * 100) + '% of the video');
      });

      let vimeo_video_data = getVimeoMetaData(file_metadata);

      //let interval = setInterval(function () {
      //  this.log(`${prettyBytes(vimeo_insert_api_request.req.connection._bytesDispatched)} bytes uploaded.`);
      //}, 5000);

      Vimeo.videos.insert(vimeo_video_data, (err) => {
        clearInterval(interval);
        if (err) {
          deferred.reject(err);
        }
        else {
          deferred.resolve(filename);
        }
      });
    } catch (ex) {
      deferred.reject(ex.message);
    }

    return deferred.promise;
  }

  process() {

    this.log('uploading file to vimeo');
    let deferred = new Deferred();

    try {
      let customer = this.getCustomer();
      let oauth2 = customer.getOAuth2Instance('vimeo');
      oauth2.ensureAccess().then(() => {

        // Upload to vimeo api
        this.uploadVimeoVideo()
          .then(() => {
              deferred.resolve();
            },
            (err) => {
              this.log('Error uploading file to vimeo');
              deferred.reject(err);
            }
          )
          .catch((error) => {
            this.log(`Exception caught: ${error.message}`);
            deferred.reject(error);
          });

      }, (err) => deferred.reject(err));
    } catch (ex) {
      deferred.reject(ex.message);
    }

    return deferred.promise;

  }

}