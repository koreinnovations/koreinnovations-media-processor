import ConnectorBaseWithMetaData from '../ConnectorBaseWithMetaData';

export default class PublisherConnectorBase extends ConnectorBaseWithMetaData {

  process() {
    throw new Error('This method has not yet been implemented for this class');
  }
}