import PublisherConnectorBase from './PublisherConnectorBase';
import FacebookPublisher, {setupFacebookAuthentication} from './FacebookPublisher';
import NetBroadcastingRokuPublisher from './NetBroadcastingRokuPublisher';
import VimeoPublisher from './VimeoPublisher';
import YouTubePublisher, {setupYouTubeAuthentication} from './YouTubePublisher';

export default {
  PublisherConnectorBase,
  FacebookPublisher,
  NetBroadcastingRokuPublisher,
  VimeoPublisher,
  YouTubePublisher,
  setupFacebookAuthentication,
  setupYouTubeAuthentication
};