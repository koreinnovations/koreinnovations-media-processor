import FinalizerConnectorBase from './FinalizerConnectorBase';
import TemporaryFileRemoval from './TemporaryFileRemoval';

export default {
  FinalizerConnectorBase,
  TemporaryFileRemoval
};