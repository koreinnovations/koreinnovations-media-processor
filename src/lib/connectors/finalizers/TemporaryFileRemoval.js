import FinalizerConnectorBase from './FinalizerConnectorBase';

export default class TemporaryFileRemovalConnector extends FinalizerConnectorBase {

  static actionDescription() {
    return 'removed temporary files';
  }

  process() {
    throw new Error('This connector has not yet been implemented.');
  }
  
}