import CustomerConfig from '../CustomerConfig';
import {logMessage} from '../Util';

export default class ConnectorBase {

  static actionDescription() {
    throw new Error('Connector class must define actionDescription() static method');
  }

  constructor(customer: CustomerConfig) {

    if (!(customer instanceof CustomerConfig)) {
      throw new Error('ConnectorBase: customer must be instance of CustomerConfig');
    }

    this._customer = customer;
  }

  getCustomer() {
    return this._customer;
  }

  log(message) {
    logMessage(this.getCustomer().getName() + ' - ' + message);
  }

}