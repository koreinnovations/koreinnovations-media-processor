import _ from 'underscore';

import {CONNECTOR_FETCHER, CONNECTOR_PREPARER, CONNECTOR_PUBLISHER, CONNECTOR_FINALIZER} from './Constants';

import FetcherConnectorBase from './connectors/fetchers/FetcherConnectorBase';
import PreparerConnectorBase from './connectors/preparers/PreparerConnectorBase';
import PublisherConnectorBase from './connectors/publishers/PublisherConnectorBase';
import FinalizerConnectorBase from './connectors/finalizers/FinalizerConnectorBase';
import MetaDataParserBase from './metadata_parsers/MetaDataParserBase';

let fetcher_connectors = [];
let preparation_connectors = [];
let publisher_connectors = [];
let finalizer_connectors = [];

let metadata_parser;

export default class ConnectorManager {

  static validate() {
    if (!metadata_parser) {
      throw new Error('No metadata parser registered');
    }
  }

  static unregisterAll() {
    metadata_parser = null;
    fetcher_connectors = [];
    preparation_connectors = [];
    publisher_connectors = [];
    finalizer_connectors = [];
  }

  static registerConnectors(type, connectors) {

    connectors.forEach(connector => {
      let action_description = connector.actionDescription();
      if (!action_description) {
        throw new Error('Attempted to register connector that does not provide the required static actionDescription() method that returns a string');
      }
    });

    switch (type) {
      case CONNECTOR_FETCHER:

        _.each(connectors, connector => {
          if (!(connector.prototype instanceof FetcherConnectorBase)) {
            throw new Error(`Attempted to register invalid connector ${connector.name} as a ${CONNECTOR_FETCHER}`);
          }
        });

        connectors.forEach(connector => {
          if (!fetcher_connectors.includes(connector)) fetcher_connectors.push(connector);
        });
        break;
      case CONNECTOR_PREPARER:

        _.each(connectors, connector => {
          if (!(connector.prototype instanceof PreparerConnectorBase)) {
            throw new Error(`Attempted to register invalid connector ${connector.name} as a ${CONNECTOR_PREPARER}`);
          }
        });

        connectors.forEach(connector => {
          if (!preparation_connectors.includes(connector)) preparation_connectors.push(connector);
        });
        break;
      case CONNECTOR_PUBLISHER:

        _.each(connectors, connector => {
          if (!(connector.prototype instanceof PublisherConnectorBase)) {
            throw new Error(`Attempted to register invalid connector ${connector.name} as a ${CONNECTOR_PUBLISHER}`);
          }
        });

        connectors.forEach(connector => {
          if (!publisher_connectors.includes(connector)) publisher_connectors.push(connector);
        });
        break;
      case CONNECTOR_FINALIZER:

        _.each(connectors, connector => {
          if (!(connector.prototype instanceof FinalizerConnectorBase)) {
            throw new Error(`Attempted to register invalid connector ${connector.name} as a ${CONNECTOR_FINALIZER}`);
          }
        });

        connectors.forEach(connector => {
          if (!finalizer_connectors.includes(connector)) finalizer_connectors.push(connector);
        });
        break;
      default:
        throw new Error('Unrecognized connector type');
    }
  }

  static registerMetaDataParser(parser) {

    if (!(parser.prototype instanceof MetaDataParserBase)) {
      throw new Error('Metadata parser must be an instance of MetaDataParserBase');
    }

    metadata_parser = parser;
  }

  static getFetcherSequence() {
    return [...fetcher_connectors];
  }

  static getPreparerSequence() {
    return [...preparation_connectors];
  }

  static getPublisherSequence() {
    return [...publisher_connectors];
  }

  static getFinalizerSequence() {
    return [...finalizer_connectors];
  }

  static getMetaDataParser() {
    return metadata_parser;
  }

}