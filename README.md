# Kore Innovations Media Processor

@TODO: update this

YouTube uploader is a NodeJS application that works in conjunction with the `netbroadcasting_content_index`
application to monitor an FTP server for new videos and automatically upload them to YouTube.

The basic concept of the app is this:

- `netbroadcasting_content_index` regularly scans clients' FTP accounts for new video content and indexes this data into a MongoDB database
- Files in a certain directory on the FTP server are eligible to be automatically uploaded to YouTube
- The YouTube Uploader accesses this MongoDB database and keeps its own copy of the file data to track what has been uploaded and what still needs to be uploaded
- The YouTube Uploader runs on a schedule with a cutoff limit and uploads as many files as possible on behalf of connected clients
- The YouTUbe uploader downloads files from FTP temporarily, then uploads to YouTube

### Setup

This application should run on one of the NetBroadcasting player servers.  It can also be run locally during development, but requires setup of MySQL and MongoDB.

#### Requirements

1. NodeJS (6.x)
1. MongoDB
1. MySQL
1. Google API credentials for YouTube uploader (https://console.developers.google.com/)

#### Configuration
Copy the `config.example.js` file in the `src/` directory to `config.js` and fill in all the fields.

- `max_run_time_seconds` - The maximum time the uploader should run
- `temp_downloads_directory` - Where files are temporarily stored while downloading/uploading
- `file_path_regex` - Only files that match this regex in the path will be eligible for upload
- `youtube`
  - `client_id` - client id of youtube application
  - `client_secret` client secret of youtube application
- `mysql`
  - All the appropriate MySQL connection information
- `mongo`
  - `uri` - this should match the uri used by `netbroadcasting_content_index`

#### Installation

For local development, take a snapshot of one of the NetBroadcasting player databases and load it into your local MySQL instance.

In the project directory, run `npm install` and `npm run build` after configuring `config.js`.

To run the application, run `npm start`

To schedule the application for repeated execution, use `crontab -e` and set up the cron process to run the `build/index.js` file.

#### Additional notes

##### crawler.js
There is a `crawler.js` file.  This can be run via `npm run crawl`.  This file processes the `users` mongodb collection created by `netbroadcasting_content_index` and uses that data to populate a `files` collection used by the YouTube Uploader.

This script's functionality is automatically encompassed into the main `index.js` script of the YouTube uploader, but is also available to be run independently via `crawler.js`.

##### Drupal notes
Clients can connect their accounts to YouTube via the NetBroadcasting user dashboard.

Access http://player05.netbroadcasting.tv/user/integrations to see this.

### Future development

The following features would be nice to have at some point.

**Upload to Vimeo**

**Upload to Facebook**

**Parse metadata from video and pass through to YouTube**