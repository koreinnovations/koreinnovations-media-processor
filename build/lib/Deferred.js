"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = Deferred;
function Deferred() {
  var self = this;
  self.promise = new Promise(function (resolve, reject) {
    self.resolve = resolve;
    self.reject = reject;
  });
}