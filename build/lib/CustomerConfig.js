'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _underscore = require('underscore');

var _underscore2 = _interopRequireDefault(_underscore);

var _OAuth2Client = require('./OAuth2Client');

var _OAuth2Client2 = _interopRequireDefault(_OAuth2Client);

var _ConfigurationManager = require('./ConfigurationManager');

var _ConfigurationManager2 = _interopRequireDefault(_ConfigurationManager);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var CustomerConfig = function () {

  //TODO: handle updateYouTubeAccessCodeInMySQL(this.uid, this.youtube_token, this.youtube_refresh_token, this.youtube_token_expiration);

  function CustomerConfig(id, name) {
    var _this = this;

    _classCallCheck(this, CustomerConfig);

    this._id = id;
    this._name = name;
    this._oauth2_credentials = {};

    var config = _ConfigurationManager2.default.getConfig();

    if (config.oauth2) {
      _underscore2.default.each(config.oauth2, function (credentials, service) {
        if (credentials) {
          _this._oauth2_credentials[service] = new _OAuth2Client2.default(credentials.client_id, credentials.client_secret, credentials.token_url, credentials.refreshable !== false);
        }
      });
    }

    this._ftp = {};
  }

  _createClass(CustomerConfig, [{
    key: 'getID',
    value: function getID() {
      return this._id;
    }
  }, {
    key: 'getName',
    value: function getName() {
      return this._name;
    }
  }, {
    key: 'getFTPCredentials',
    value: function getFTPCredentials() {
      return {
        host: this._ftp.server,
        user: this._ftp.username,
        password: this._ftp.password
      };
    }
  }, {
    key: 'getFTPPath',
    value: function getFTPPath() {
      return this._ftp.path;
    }
  }, {
    key: 'setFTPCredentials',
    value: function setFTPCredentials(server, username, password, path) {
      this._ftp = {
        server: server,
        username: username,
        password: password,
        path: path
      };
    }
  }, {
    key: 'getOAuth2Instance',
    value: function getOAuth2Instance(service) {
      if (!this._oauth2_credentials[service]) {
        throw new Error('Service "' + service + '" not registered as OAuth2 client');
      }

      return this._oauth2_credentials[service];
    }
  }]);

  return CustomerConfig;
}();

exports.default = CustomerConfig;