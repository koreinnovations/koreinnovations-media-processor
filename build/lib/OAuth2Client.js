'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _Deferred = require('./Deferred');

var _Deferred2 = _interopRequireDefault(_Deferred);

var _moment = require('moment');

var _moment2 = _interopRequireDefault(_moment);

var _qs = require('qs');

var _qs2 = _interopRequireDefault(_qs);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

// var fetch = require('node-fetch');
// import fetch from 'node-fetch';

var OAuth2Client = function () {
  function OAuth2Client(client_id, client_secret, token_url) {
    var refreshable = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : true;

    _classCallCheck(this, OAuth2Client);

    if (!client_id) throw new Error('Client ID missing');
    if (!client_secret) throw new Error('Client secret missing');
    if (!token_url) throw new Error('Token URL missing');

    this._client_id = client_id;
    this._client_secret = client_secret;
    this._token_url = token_url;
    this._refreshable = refreshable;
  }

  _createClass(OAuth2Client, [{
    key: 'getAppCredentials',
    value: function getAppCredentials() {
      return { client_id: this._client_id, client_secret: this._client_secret };
    }
  }, {
    key: 'setToken',
    value: function setToken(access_token) {
      var refresh_token = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
      var token_expiration = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;

      this._access_token = access_token;
      this._refresh_token = refresh_token;
      this._token_expiration = token_expiration;
    }
  }, {
    key: 'getAccessToken',
    value: function getAccessToken() {
      return this._access_token;
    }
  }, {
    key: 'isExpired',
    value: function isExpired() {
      return this._refreshable && (0, _moment2.default)().unix() >= this._token_expiration;
    }
  }, {
    key: 'refreshToken',
    value: function refreshToken() {
      var _this = this;

      var deferred = new _Deferred2.default();

      var request_params = {
        client_id: this._client_id,
        client_secret: this._client_secret,
        refresh_token: this._refresh_token,
        grant_type: 'refresh_token'
      };

      var headers = { 'Content-Type': 'application/x-www-form-urlencoded' };

      fetch(this._token_url, { method: 'POST', body: _qs2.default.stringify(request_params), headers: headers }).then(function (res) {
        if (res.status == 200) {
          return res.json();
        } else {
          throw 'Response code ' + res.status;
        }
      }, function (error) {
        throw error;
      }).then(function (responseJSON) {
        if (responseJSON.access_token) {
          var new_expiration = (0, _moment2.default)().unix() + parseInt(responseJSON.expires_in);

          _this.setToken(responseJSON.access_token, responseJSON.refresh_token, new_expiration);

          deferred.resolve(responseJSON.access_token);
        } else {
          deferred.reject('No access code returned');
        }
      }).catch(function (error) {
        deferred.reject(error);
      });

      return deferred.promise;
    }
  }, {
    key: 'ensureAccess',
    value: function ensureAccess() {
      var deferred = new _Deferred2.default();

      if (!this._access_token) {
        deferred.reject('No access token present');
      } else if (this.isExpired()) {
        return this.refreshToken();
      } else {
        deferred.resolve(this._access_token);
      }

      return deferred.promise;
    }
  }]);

  return OAuth2Client;
}();

exports.default = OAuth2Client;