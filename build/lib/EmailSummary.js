'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _nodemailer = require('nodemailer');

var _nodemailer2 = _interopRequireDefault(_nodemailer);

var _path = require('path');

var _path2 = _interopRequireDefault(_path);

var _moment = require('moment');

var _moment2 = _interopRequireDefault(_moment);

var _sprintf = require('sprintf');

var _sprintf2 = _interopRequireDefault(_sprintf);

var _Util = require('./Util');

var _Deferred = require('./Deferred');

var _Deferred2 = _interopRequireDefault(_Deferred);

var _CustomerConfig = require('./CustomerConfig');

var _CustomerConfig2 = _interopRequireDefault(_CustomerConfig);

var _BatchProcessor = require('./BatchProcessor');

var _BatchProcessor2 = _interopRequireDefault(_BatchProcessor);

var _ConfigurationManager2 = require('./ConfigurationManager');

var _ConfigurationManager3 = _interopRequireDefault(_ConfigurationManager2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var EmailSummary = function () {
  function EmailSummary(customer, batch) {
    _classCallCheck(this, EmailSummary);

    var _ConfigurationManager = _ConfigurationManager3.default.getConfig(),
        email_reports = _ConfigurationManager.email_reports;

    if (!email_reports || !email_reports.sender || !email_reports.transport) {
      throw new Error('email_reports must be defined in config.js and must include sender and transport properties');
    }

    this._sender = email_reports.sender;
    this._transportConfig = email_reports.transport;

    if (!(customer instanceof _CustomerConfig2.default)) {
      throw new Error('First argument to constructor must be instance of CustomerConfig');
    }
    if (!(batch instanceof _BatchProcessor2.default)) {
      throw new Error('Second argument to constructor must be instance of BatchProcessor');
    }

    this._customer = customer;
    this._batch = batch;
  }

  _createClass(EmailSummary, [{
    key: 'getBatch',
    value: function getBatch() {
      return this._batch;
    }
  }, {
    key: 'getMessageContent',
    value: function getMessageContent() {
      var _this = this;

      var lines = [];

      var batch = this.getBatch();
      var history = batch.getHistory();
      var total_successes = 0;
      var total_failures = 0;
      var success_slices = [];
      var failure_slices = [];

      history.forEach(function (history_item) {
        if (history_item.slice.getCustomer() == _this._customer) {
          if (history_item.success) {
            total_successes++;
            success_slices.push(history_item);
          } else {
            total_failures++;
            failure_slices.push(history_item);
          }
        }
      });

      if (total_successes == 0 && total_failures == 0) {
        // skip sending a report because nothing was done or even tried
        (0, _Util.logMessage)('There are no successes or failures to report.');
        return;
      }

      lines.push((0, _sprintf2.default)('Processed on behalf of: %s', this._customer.getName()));
      lines.push((0, _sprintf2.default)('Started: %s', batch.getStartTime().format('h:mma')));
      lines.push((0, _sprintf2.default)('Duration: %s', batch.getDuration().humanize()));
      lines.push('');
      lines.push((0, _sprintf2.default)('Files processed successfully: %d', total_successes));
      lines.push((0, _sprintf2.default)('Files processed unsuccessfully: %d', total_failures));

      if (total_successes > 0) {
        lines.push('');
        lines.push('');
        lines.push('FILES PROCESSED:');

        success_slices.forEach(function (slice) {
          var parsed = _path2.default.parse(slice.path);
          lines.push('- ' + parsed.base);
          slice.slice.getChronologicalHistory().forEach(function (item) {
            if (item.success) {
              lines.push('  - ' + item.connector_class.actionDescription());
            } else {
              lines.push('  - ' + item.connector_name + ': ' + item.error);
            }
          });
          lines.push('');
        });
      }

      if (total_failures > 0) {
        lines.push('');
        lines.push('');
        lines.push('FILES WITH ISSUES:');

        failure_slices.forEach(function (slice) {
          var parsed = _path2.default.parse(slice.path);
          lines.push('x ' + parsed.base);
          lines.push('  - Issue: ' + slice.error);
          lines.push('');
        });
      }

      // eslint-disable-next-line
      return lines.join("\n");
    }
  }, {
    key: 'send',
    value: function send(recipients) {
      var deferred = new _Deferred2.default();

      try {

        if (!recipients || recipients.length == 0) {
          throw new Error('Recipients must be an array of email addresses');
        }

        var message = this.getMessageContent();

        if (message && message.length > 0) {
          (0, _Util.logMessage)('Message contents', message);

          // create reusable transporter object using the default SMTP transport
          var transporter = _nodemailer2.default.createTransport(this._transportConfig);

          // setup e-mail data with unicode symbols
          var mailOptions = {
            from: this._sender,
            to: recipients.join(', '), // list of receivers
            subject: 'Media Processor - Batch Summary ' + (0, _moment2.default)().format('M/D h:mma'), // Subject line
            text: message, // plaintext body
            debug: true,
            logger: true
          };

          // send mail with defined transport object
          transporter.sendMail(mailOptions, function (error, info) {
            if (error) {
              (0, _Util.logMessage)('Encountered error sending mail', error);
              deferred.reject(error);
            } else {
              (0, _Util.logMessage)('Message recipients:', recipients.join(', '));
              (0, _Util.logMessage)('Message sent: ' + info.response);
              deferred.resolve({ sent: true });
            }
          });
        } else {
          deferred.resolve({ sent: false });
        }
      } catch (ex) {
        deferred.reject(ex.message);
      }

      return deferred.promise;
    }
  }]);

  return EmailSummary;
}();

exports.default = EmailSummary;