'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _underscore = require('underscore');

var _underscore2 = _interopRequireDefault(_underscore);

var _Constants = require('./Constants');

var _FetcherConnectorBase = require('./connectors/fetchers/FetcherConnectorBase');

var _FetcherConnectorBase2 = _interopRequireDefault(_FetcherConnectorBase);

var _PreparerConnectorBase = require('./connectors/preparers/PreparerConnectorBase');

var _PreparerConnectorBase2 = _interopRequireDefault(_PreparerConnectorBase);

var _PublisherConnectorBase = require('./connectors/publishers/PublisherConnectorBase');

var _PublisherConnectorBase2 = _interopRequireDefault(_PublisherConnectorBase);

var _FinalizerConnectorBase = require('./connectors/finalizers/FinalizerConnectorBase');

var _FinalizerConnectorBase2 = _interopRequireDefault(_FinalizerConnectorBase);

var _MetaDataParserBase = require('./metadata_parsers/MetaDataParserBase');

var _MetaDataParserBase2 = _interopRequireDefault(_MetaDataParserBase);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var fetcher_connectors = [];
var preparation_connectors = [];
var publisher_connectors = [];
var finalizer_connectors = [];

var metadata_parser = void 0;

var ConnectorManager = function () {
  function ConnectorManager() {
    _classCallCheck(this, ConnectorManager);
  }

  _createClass(ConnectorManager, null, [{
    key: 'validate',
    value: function validate() {
      if (!metadata_parser) {
        throw new Error('No metadata parser registered');
      }
    }
  }, {
    key: 'unregisterAll',
    value: function unregisterAll() {
      metadata_parser = null;
      fetcher_connectors = [];
      preparation_connectors = [];
      publisher_connectors = [];
      finalizer_connectors = [];
    }
  }, {
    key: 'registerConnectors',
    value: function registerConnectors(type, connectors) {

      connectors.forEach(function (connector) {
        var action_description = connector.actionDescription();
        if (!action_description) {
          throw new Error('Attempted to register connector that does not provide the required static actionDescription() method that returns a string');
        }
      });

      switch (type) {
        case _Constants.CONNECTOR_FETCHER:

          _underscore2.default.each(connectors, function (connector) {
            if (!(connector.prototype instanceof _FetcherConnectorBase2.default)) {
              throw new Error('Attempted to register invalid connector ' + connector.name + ' as a ' + _Constants.CONNECTOR_FETCHER);
            }
          });

          connectors.forEach(function (connector) {
            if (!fetcher_connectors.includes(connector)) fetcher_connectors.push(connector);
          });
          break;
        case _Constants.CONNECTOR_PREPARER:

          _underscore2.default.each(connectors, function (connector) {
            if (!(connector.prototype instanceof _PreparerConnectorBase2.default)) {
              throw new Error('Attempted to register invalid connector ' + connector.name + ' as a ' + _Constants.CONNECTOR_PREPARER);
            }
          });

          connectors.forEach(function (connector) {
            if (!preparation_connectors.includes(connector)) preparation_connectors.push(connector);
          });
          break;
        case _Constants.CONNECTOR_PUBLISHER:

          _underscore2.default.each(connectors, function (connector) {
            if (!(connector.prototype instanceof _PublisherConnectorBase2.default)) {
              throw new Error('Attempted to register invalid connector ' + connector.name + ' as a ' + _Constants.CONNECTOR_PUBLISHER);
            }
          });

          connectors.forEach(function (connector) {
            if (!publisher_connectors.includes(connector)) publisher_connectors.push(connector);
          });
          break;
        case _Constants.CONNECTOR_FINALIZER:

          _underscore2.default.each(connectors, function (connector) {
            if (!(connector.prototype instanceof _FinalizerConnectorBase2.default)) {
              throw new Error('Attempted to register invalid connector ' + connector.name + ' as a ' + _Constants.CONNECTOR_FINALIZER);
            }
          });

          connectors.forEach(function (connector) {
            if (!finalizer_connectors.includes(connector)) finalizer_connectors.push(connector);
          });
          break;
        default:
          throw new Error('Unrecognized connector type');
      }
    }
  }, {
    key: 'registerMetaDataParser',
    value: function registerMetaDataParser(parser) {

      if (!(parser.prototype instanceof _MetaDataParserBase2.default)) {
        throw new Error('Metadata parser must be an instance of MetaDataParserBase');
      }

      metadata_parser = parser;
    }
  }, {
    key: 'getFetcherSequence',
    value: function getFetcherSequence() {
      return [].concat(_toConsumableArray(fetcher_connectors));
    }
  }, {
    key: 'getPreparerSequence',
    value: function getPreparerSequence() {
      return [].concat(_toConsumableArray(preparation_connectors));
    }
  }, {
    key: 'getPublisherSequence',
    value: function getPublisherSequence() {
      return [].concat(_toConsumableArray(publisher_connectors));
    }
  }, {
    key: 'getFinalizerSequence',
    value: function getFinalizerSequence() {
      return [].concat(_toConsumableArray(finalizer_connectors));
    }
  }, {
    key: 'getMetaDataParser',
    value: function getMetaDataParser() {
      return metadata_parser;
    }
  }]);

  return ConnectorManager;
}();

exports.default = ConnectorManager;