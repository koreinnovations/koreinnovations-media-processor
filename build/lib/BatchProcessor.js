'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _moment = require('moment');

var _moment2 = _interopRequireDefault(_moment);

var _ProcessorSlice = require('./ProcessorSlice');

var _ProcessorSlice2 = _interopRequireDefault(_ProcessorSlice);

var _ConnectorManager = require('./ConnectorManager');

var _ConnectorManager2 = _interopRequireDefault(_ConnectorManager);

var _ConfigurationManager = require('./ConfigurationManager');

var _ConfigurationManager2 = _interopRequireDefault(_ConfigurationManager);

var _Deferred = require('./Deferred');

var _Deferred2 = _interopRequireDefault(_Deferred);

var _Util = require('./Util');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var BatchProcessor = function () {
  function BatchProcessor() {
    _classCallCheck(this, BatchProcessor);

    this._start_time = null;
    this._end_time = null;
    this._slices = [];
    this._slices_processed = 0;
    this._slices_failed = 0;
    this._history = [];
  }

  _createClass(BatchProcessor, [{
    key: 'clearAll',
    value: function clearAll() {
      this._slices = [];
      this._slices_processed = 0;
      this._slices_failed = 0;
      this._history = [];
    }
  }, {
    key: 'getSlicesProcessed',
    value: function getSlicesProcessed() {
      return this._slices_processed;
    }
  }, {
    key: 'getSlicesFailed',
    value: function getSlicesFailed() {
      return this._slices_failed;
    }
  }, {
    key: 'getRemainingSliceCount',
    value: function getRemainingSliceCount() {
      return this._slices.length;
    }
  }, {
    key: 'hasMoreSlices',
    value: function hasMoreSlices() {
      return this.getRemainingSliceCount() > 0;
    }
  }, {
    key: 'getNextSlice',
    value: function getNextSlice() {
      return this._slices.shift();
    }
  }, {
    key: 'addSlice',
    value: function addSlice(slice) {
      if (!(slice instanceof _ProcessorSlice2.default)) {
        throw new Error('Only an instance of ProcessorSlice may be passed to BatchProcessor.addSlice()');
      }
      this._slices.push(slice);
    }
  }, {
    key: 'addHistory',
    value: function addHistory(slice, path, success, error) {
      this._history.push({ slice: slice, path: path, success: success, error: error });
    }
  }, {
    key: 'getHistory',
    value: function getHistory() {
      return this._history;
    }
  }, {
    key: 'startTimer',
    value: function startTimer() {
      if (!this._start_time) {
        this._start_time = (0, _moment2.default)();
      }
    }
  }, {
    key: 'endTimer',
    value: function endTimer() {
      this._end_time = (0, _moment2.default)();
    }
  }, {
    key: 'getStartTime',
    value: function getStartTime() {
      return this._start_time;
    }
  }, {
    key: 'getEndTime',
    value: function getEndTime() {
      return this._end_time;
    }
  }, {
    key: 'getDuration',
    value: function getDuration() {
      return _moment2.default.duration(this._end_time.diff(this._start_time));
    }
  }, {
    key: 'hasExceededTimeLimit',
    value: function hasExceededTimeLimit() {
      var config = _ConfigurationManager2.default.getConfig();
      if (!config.max_run_time_seconds || config.max_run_time_seconds === 0) {
        return false;
      }
      var time_elapsed = (0, _moment2.default)().unix() - this._start_time.unix();
      return time_elapsed >= config.max_run_time_seconds;
    }
  }, {
    key: 'process',
    value: function process() {
      var _this = this;

      this.startTimer();

      _ConnectorManager2.default.validate();

      var deferred = new _Deferred2.default();

      var resolve = function resolve(reason) {
        (0, _Util.logMessage)(reason);
        _this.endTimer();
        deferred.resolve();
      };

      var processNextOne = function processNextOne() {

        if (_this.hasExceededTimeLimit()) {
          resolve('Time limit exceeded. Exiting...');
        } else if (!_this.hasMoreSlices()) {
          resolve('All items in batch processed. Exiting...');
        } else {
          (function () {
            (0, _Util.logMessage)('Processing next slice in batch');
            var slice = _this.getNextSlice();
            var file_path = slice.getFilePath();
            slice.process().then(function () {
              _this.addHistory(slice, file_path, true);
              _this._slices_processed++;
              processNextOne();
            }, function (err) {
              _this.addHistory(slice, file_path, false, err);
              _this._slices_failed++;
              processNextOne();
            });
          })();
        }
      };

      processNextOne();

      return deferred.promise;
    }
  }]);

  return BatchProcessor;
}();

exports.default = BatchProcessor;