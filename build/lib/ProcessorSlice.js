'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _path = require('path');

var _path2 = _interopRequireDefault(_path);

var _CustomerConfig = require('./CustomerConfig');

var _CustomerConfig2 = _interopRequireDefault(_CustomerConfig);

var _ConnectorManager = require('./ConnectorManager');

var _ConnectorManager2 = _interopRequireDefault(_ConnectorManager);

var _FileMetaData = require('./FileMetaData');

var _FileMetaData2 = _interopRequireDefault(_FileMetaData);

var _Deferred = require('./Deferred');

var _Deferred2 = _interopRequireDefault(_Deferred);

var _Util = require('./Util');

var _Constants = require('./Constants');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var ProcessorSlice = function () {
  function ProcessorSlice(customer, file_raw) {
    _classCallCheck(this, ProcessorSlice);

    if (!(customer instanceof _CustomerConfig2.default)) {
      throw new Error('First parameter must be an instance of the CustomerConfig class');
    }
    if (file_raw instanceof _FileMetaData2.default) {
      throw new Error('Second parameter cannot be an instance of FileMetaData. It must be a JS object with a path property');
    }
    if (!file_raw.path) {
      throw new Error('Second parameter must be a JS object with a path property');
    }

    this._chronological_history = [];
    this._history = {};

    this._customer = customer;
    this._file_raw = file_raw;

    var parsed = _path2.default.parse(file_raw.path);

    this._filename = parsed.base;
  }

  _createClass(ProcessorSlice, [{
    key: 'getCustomer',
    value: function getCustomer() {
      return this._customer;
    }
  }, {
    key: 'getFilePath',
    value: function getFilePath() {
      return this._file_raw.path;
    }
  }, {
    key: 'log',
    value: function log(message) {
      (0, _Util.logMessage)(this.getCustomer().getName() + ' - ' + this._filename + ' - ' + message);
    }
  }, {
    key: 'getChronologicalHistory',
    value: function getChronologicalHistory() {
      return this._chronological_history;
    }
  }, {
    key: 'getHistory',
    value: function getHistory(type) {
      return this._history[type];
    }
  }, {
    key: 'addHistory',
    value: function addHistory(type, connector_name, connector_class, success, error) {
      if (!this._history[type]) {
        this._history[type] = [];
      }
      var record = { connector_name: connector_name, connector_class: connector_class, success: success, error: error };
      this._history[type].push(record);
      this._chronological_history.push(record);
    }
  }, {
    key: 'parseMetaData',
    value: function parseMetaData() {
      var _this = this;

      var deferred = new _Deferred2.default();

      var parser = _ConnectorManager2.default.getMetaDataParser();
      parser.parse(this._file_raw.path).then(function (file_metadata) {
        _this._file_metadata = file_metadata;
        deferred.resolve();
      }, function (err) {
        return deferred.reject(err);
      });

      return deferred.promise;
    }
  }, {
    key: '_runSequence',
    value: function _runSequence(type, sequence, constructor) {
      var _this2 = this;

      var quit_on_any_failure = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : true;

      var deferred = new _Deferred2.default();

      var successes = 0;
      var failures = 0;

      try {
        (function () {
          var processNextConnector = function processNextConnector() {
            try {
              if (sequence.length > 0) {
                (function () {
                  var module = sequence.shift();
                  var connector_name = module.name;
                  _this2.log(type + ': ' + connector_name);
                  var connector = constructor(module);
                  connector.process().then(function () {
                    successes++;
                    _this2.addHistory(type, connector_name, module, true);
                    processNextConnector();
                  }, function (err) {
                    failures++;
                    _this2.addHistory(type, connector_name, module, false, err);
                    if (quit_on_any_failure) {
                      _this2.log('Failing connector ' + connector_name + ' because of ' + err);
                      deferred.reject(err);
                    } else {
                      _this2.log('Connector ' + connector_name + ' failed because of ' + err + ', but continuing to process other connectors');
                      processNextConnector();
                    }
                  });
                })();
              } else {
                _this2.log('Ran out of ' + type + ' connectors.  Successes=' + successes + ', failures=' + failures);
                if (successes > 0 || failures == 0) {
                  _this2.log('Resolving ' + type + ' sequence');
                  deferred.resolve();
                } else {
                  _this2.log('Rejecting ' + type + ' sequence');
                  deferred.reject('None of the ' + type + ' connectors succeeded. Rejecting promise');
                }
              }
            } catch (ex) {
              failures++;
              if (quit_on_any_failure) {
                console.log('_runSequence inner exception encountered', ex);
                deferred.reject(ex.message);
              } else {
                processNextConnector();
              }
            }
          };

          processNextConnector();
        })();
      } catch (ex) {
        console.log('_runSequence outer exception encountered', ex);
        deferred.reject(ex.message);
      }

      return deferred.promise;
    }
  }, {
    key: 'fetch',
    value: function fetch() {
      var _this3 = this;

      var sequence = _ConnectorManager2.default.getFetcherSequence();
      var constructor = function constructor(module) {
        return new module(_this3._customer, _this3._file_raw);
      };
      return this._runSequence(_Constants.CONNECTOR_FETCHER, sequence, constructor);
    }
  }, {
    key: 'prepare',
    value: function prepare() {
      var _this4 = this;

      var sequence = _ConnectorManager2.default.getPreparerSequence();
      var constructor = function constructor(module) {
        return new module(_this4._customer, _this4._file_raw, _this4._file_metadata);
      };
      return this._runSequence(_Constants.CONNECTOR_PREPARER, sequence, constructor);
    }
  }, {
    key: 'publish',
    value: function publish() {
      var _this5 = this;

      var sequence = _ConnectorManager2.default.getPublisherSequence();
      var constructor = function constructor(module) {
        return new module(_this5._customer, _this5._file_raw, _this5._file_metadata);
      };
      return this._runSequence(_Constants.CONNECTOR_PUBLISHER, sequence, constructor, false);
    }
  }, {
    key: 'finalize',
    value: function finalize() {
      var _this6 = this;

      var sequence = _ConnectorManager2.default.getFinalizerSequence();
      var constructor = function constructor(module) {
        return new module(_this6._customer, _this6._file_raw, _this6._file_metadata);
      };
      return this._runSequence(_Constants.CONNECTOR_FINALIZER, sequence, constructor);
    }
  }, {
    key: 'process',
    value: function process() {
      var _this7 = this;

      _ConnectorManager2.default.validate();

      var deferred = new _Deferred2.default();

      this.log('Running fetchers...');
      try {

        var handleError = function handleError(err) {
          deferred.reject(err);
          throw err;
        };

        this.fetch().then(function () {
          _this7.log('Finished fetching. Parsing metadata...');
          return _this7.parseMetaData();
        }, handleError).then(function () {
          _this7.log('Finished parsing metadata. Preparing...');
          return _this7.prepare();
        }, handleError).then(function () {
          _this7.log('Finished preparing. Publishing...');
          return _this7.publish();
        }, handleError).then(function () {
          _this7.log('Finished publishing. Finalizing...');
          return _this7.finalize();
        }, handleError).then(function () {
          _this7.log('Finished finalizing. Slice complete');
          deferred.resolve();
        }, handleError).catch(function (err) {
          _this7.log('ProcessorSlice failing due to error (chained catch)');
          _this7.log(err);
          deferred.reject(err);
        });
      } catch (err) {
        this.log('ProcessorSlice failing due to error (wrapper)');
        this.log(err);
        deferred.reject(err);
      }

      return deferred.promise;
    }
  }]);

  return ProcessorSlice;
}();

exports.default = ProcessorSlice;