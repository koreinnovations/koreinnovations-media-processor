'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _MetaDataParserBase2 = require('./MetaDataParserBase');

var _MetaDataParserBase3 = _interopRequireDefault(_MetaDataParserBase2);

var _FileMetaData = require('../FileMetaData');

var _FileMetaData2 = _interopRequireDefault(_FileMetaData);

var _fluentFfmpeg = require('fluent-ffmpeg');

var _fluentFfmpeg2 = _interopRequireDefault(_fluentFfmpeg);

var _underscore = require('underscore');

var _underscore2 = _interopRequireDefault(_underscore);

var _string = require('string');

var _string2 = _interopRequireDefault(_string);

var _moment = require('moment');

var _moment2 = _interopRequireDefault(_moment);

var _Util = require('../Util');

var _Deferred = require('../Deferred');

var _Deferred2 = _interopRequireDefault(_Deferred);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var MP4MetaDataParser = function (_MetaDataParserBase) {
  _inherits(MP4MetaDataParser, _MetaDataParserBase);

  function MP4MetaDataParser() {
    _classCallCheck(this, MP4MetaDataParser);

    return _possibleConstructorReturn(this, (MP4MetaDataParser.__proto__ || Object.getPrototypeOf(MP4MetaDataParser)).apply(this, arguments));
  }

  _createClass(MP4MetaDataParser, null, [{
    key: 'parse',
    value: function parse(file_path) {
      var deferred = new _Deferred2.default();

      var callback = function callback(err, metadata) {

        if (err) {
          (0, _Util.logMessage)(err);
          deferred.reject(err.message);
        } else {
          var tags = metadata.format.tags;


          if (!tags.title) {
            deferred.reject('Cannot determine title of video');
          } else {
            var options = {
              title: tags.title,
              description: tags.synopsis,
              speaker: tags.artist,
              category: tags.album,
              date: tags.date ? (0, _moment2.default)(tags.date).format('M/DD/YYYY') : null
            };
            var file = new _FileMetaData2.default(file_path, options);

            if (tags.genre) {
              var split_tags = _underscore2.default.map(tags.genre.split(','), function (tag) {
                return (0, _string2.default)(tag).trim().s;
              });
              file.setTags(split_tags);
            }

            file.setVideoMetaData(metadata);
            file.setDuration(metadata.format.duration * 1000);
            file.setFilesize(metadata.format.size);

            // logMessage(metadata);

            var stream = _underscore2.default.findWhere(metadata.streams, { codec_name: 'h264' });
            (0, _Util.logMessage)('stream', stream);
            if (!stream) {
              deferred.reject('Stream is in invalid format');
            } else {
              (0, _Util.logMessage)('stream is valid');
              file.setDimensions(stream.width, stream.height);
              file.setSampleAspectRatio(stream.sample_aspect_ratio);
              file.setDisplayAspectRatio(stream.display_aspect_ratio);
              deferred.resolve(file);
            }
          }
        }
      };

      _fluentFfmpeg2.default.ffprobe(file_path, callback);

      return deferred.promise;
    }
  }]);

  return MP4MetaDataParser;
}(_MetaDataParserBase3.default);

exports.default = MP4MetaDataParser;