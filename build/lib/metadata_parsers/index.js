'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _MetaDataParserBase = require('./MetaDataParserBase');

var _MetaDataParserBase2 = _interopRequireDefault(_MetaDataParserBase);

var _MP4MetaDataParser = require('./MP4MetaDataParser');

var _MP4MetaDataParser2 = _interopRequireDefault(_MP4MetaDataParser);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
  MetaDataParserBase: _MetaDataParserBase2.default,
  MP4MetaDataParser: _MP4MetaDataParser2.default
};