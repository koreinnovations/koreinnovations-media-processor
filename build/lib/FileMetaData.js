'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _moment = require('moment');

var _moment2 = _interopRequireDefault(_moment);

var _path = require('path');

var _path2 = _interopRequireDefault(_path);

var _underscore = require('underscore');

var _underscore2 = _interopRequireDefault(_underscore);

var _sprintf = require('sprintf');

var _sprintf2 = _interopRequireDefault(_sprintf);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var FileMetaData = function () {
  function FileMetaData(file_path) {
    var options = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

    _classCallCheck(this, FileMetaData);

    var parsed = _path2.default.parse(file_path);

    this._directory = parsed.dir;
    this._originalFilename = parsed.base;
    this._filename = parsed.base;
    this._name_only = parsed.name;
    this._extension = parsed.ext;

    this._description = options.description;
    this._date = options.date;
    this._title = options.title;
    this._speaker = options.speaker;
    this._category = options.category;

    this._tags = [];
  }

  _createClass(FileMetaData, [{
    key: 'getTags',
    value: function getTags() {
      return this._tags;
    }
  }, {
    key: 'setTags',
    value: function setTags(tags) {
      this._tags = tags;
    }
  }, {
    key: 'getTitle',
    value: function getTitle() {
      return this._title;
    }
  }, {
    key: 'getDescription',
    value: function getDescription() {
      return this._description;
    }
  }, {
    key: 'getCategory',
    value: function getCategory() {
      return this._category;
    }
  }, {
    key: 'getSpeaker',
    value: function getSpeaker() {
      return this._speaker;
    }
  }, {
    key: 'getDate',
    value: function getDate() {
      return this._date;
    }
  }, {
    key: 'getDateMoment',
    value: function getDateMoment() {
      return (0, _moment2.default)(this._date);
    }
  }, {
    key: 'getOriginalFilename',
    value: function getOriginalFilename() {
      return this._originalFilename;
    }
  }, {
    key: 'getFilename',
    value: function getFilename() {
      return this._filename;
    }
  }, {
    key: 'getOriginalFullPath',
    value: function getOriginalFullPath() {
      return this._directory + '/' + this._originalFilename;
    }
  }, {
    key: 'getFullPath',
    value: function getFullPath() {
      return this._directory + '/' + this._filename;
    }
  }, {
    key: 'setFullPath',
    value: function setFullPath(new_path) {
      var parsed = _path2.default.parse(new_path);
      this._directory = parsed.dir;
      this._originalFilename = parsed.base;
      this._filename = parsed.base;
      this._name_only = parsed.name;
      this._extension = parsed.ext;
    }

    // This mutates the _filename property

  }, {
    key: 'getSanitizedFilename',
    value: function getSanitizedFilename(include_extension) {
      var sanitized = this._name_only.replace(/[^0-9A-Za-z\.]/g, '_').replace(/_{2,}/g, '_').toLowerCase();
      if (include_extension) {
        return sanitized + this._extension;
      } else {
        return sanitized;
      }
    }
  }, {
    key: 'getValidTimes',
    value: function getValidTimes() {
      var self = this;
      return _underscore2.default.filter([5, 10, 45, 100, 200, 500], function (time) {
        return self.getDuration() >= time;
      });
    }
  }, {
    key: 'getDuration',
    value: function getDuration() {
      return this._duration_milliseconds;
    }
  }, {
    key: 'setDuration',
    value: function setDuration(milliseconds) {
      this._duration_milliseconds = milliseconds;
    }
  }, {
    key: 'setVideoMetaData',
    value: function setVideoMetaData(video_metadata) {
      this._videoMetadata = video_metadata;
    }
  }, {
    key: 'setFilesize',
    value: function setFilesize(bytes) {
      this._filesize = bytes;
    }
  }, {
    key: 'getFilesize',
    value: function getFilesize() {
      return this._filesize;
    }
  }, {
    key: 'getSampleAspectRatio',
    value: function getSampleAspectRatio() {
      return this._sample_aspect_ratio;
    }
  }, {
    key: 'setSampleAspectRatio',
    value: function setSampleAspectRatio(sample_aspect_ratio) {
      this._sample_aspect_ratio = sample_aspect_ratio;
    }
  }, {
    key: 'getDisplayAspectRatio',
    value: function getDisplayAspectRatio() {
      return this._display_aspect_ratio;
    }
  }, {
    key: 'setDisplayAspectRatio',
    value: function setDisplayAspectRatio(display_aspect_ratio) {
      this._display_aspect_ratio = display_aspect_ratio;
    }
  }, {
    key: 'setDimensions',
    value: function setDimensions(width, height) {
      this._width = width;
      this._height = height;
    }
  }, {
    key: 'setThumbnails',
    value: function setThumbnails(thumbnails) {
      this._thumbnails = thumbnails;
    }
  }, {
    key: 'getThumbnails',
    value: function getThumbnails() {
      return this._thumbnails;
    }
  }, {
    key: 'getThumbnailDimensionsString',
    value: function getThumbnailDimensionsString() {
      var ratio_split = this._display_aspect_ratio.split(':');
      var width_multiplier = ratio_split[0] / ratio_split[1];
      var width = Math.floor(this._height * width_multiplier);
      return (0, _sprintf2.default)('%dx%d', width, this._height);
    }
  }, {
    key: 'getDimensionsString',
    value: function getDimensionsString() {
      return (0, _sprintf2.default)('%dx%d', this._width, this._height);
    }
  }, {
    key: 'isWidescreen',
    value: function isWidescreen() {
      return this._height / this._width == 9 / 16;
    }
  }, {
    key: 'isStandard',
    value: function isStandard() {
      return this._height / this._width == 3 / 4;
    }
  }, {
    key: 'getPreferredThumbnail',
    value: function getPreferredThumbnail() {
      // Pick the second thumbnail in if it exists.
      // Otherwise pick the first
      if (this._thumbnails.length > 1) {
        return this._thumbnails[1];
      } else {
        return this._thumbnails[0];
      }
    }
  }, {
    key: 'getPreferredThumbnailPath',
    value: function getPreferredThumbnailPath() {
      return this._directory + '/' + this.getPreferredThumbnail();
    }
  }]);

  return FileMetaData;
}();

exports.default = FileMetaData;