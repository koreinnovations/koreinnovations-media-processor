'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var _defaults = {
  max_run_time_seconds: 3600
};

var _config = _extends({}, _defaults);

var ConfigurationManager = function () {
  function ConfigurationManager() {
    _classCallCheck(this, ConfigurationManager);
  }

  _createClass(ConfigurationManager, null, [{
    key: '__reset',
    value: function __reset() {
      _config = undefined;
    }
  }, {
    key: 'clearAll',
    value: function clearAll() {
      _config = _extends({}, _defaults);
    }
  }, {
    key: 'setDefaults',
    value: function setDefaults(defaults) {
      _defaults = _extends({}, _defaults, defaults);
    }
  }, {
    key: 'setConfig',
    value: function setConfig(config) {
      _config = _extends({}, _defaults, _config, config);
    }
  }, {
    key: 'getConfig',
    value: function getConfig() {
      if (!_config) {
        throw new Error('ConfigurationManager must be initialized for the app to work');
      }
      return _extends({}, _config);
    }
  }]);

  return ConfigurationManager;
}();

exports.default = ConfigurationManager;