'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.openURL = openURL;
exports.logMessage = logMessage;
exports.setupDirectories = setupDirectories;
exports.base64EncodeFile = base64EncodeFile;

var _fs = require('fs');

var _fs2 = _interopRequireDefault(_fs);

var _moment = require('moment');

var _moment2 = _interopRequireDefault(_moment);

var _underscore = require('underscore');

var _underscore2 = _interopRequireDefault(_underscore);

var _opn = require('opn');

var _opn2 = _interopRequireDefault(_opn);

var _ConfigurationManager = require('./ConfigurationManager');

var _ConfigurationManager2 = _interopRequireDefault(_ConfigurationManager);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function openURL(url) {
  (0, _opn2.default)(url);
}

function logMessage(message) {
  if (process.env.NODE_ENV != 'test') {
    var date = (0, _moment2.default)().format('YYYY-MM-DD HH:mm:ss');
    console.log(date, '-', message);
  }
}

function setupDirectories() {
  var config = _ConfigurationManager2.default.getConfig();
  logMessage('setupDirectories()');

  // Make sure each directory exists
  _underscore2.default.each(config.paths, function (dir) {
    if (!_fs2.default.existsSync(dir)) {
      _fs2.default.mkdirSync(dir);
    }
  });
}

// function to encode file data to base64 encoded string
function base64EncodeFile(file) {
  // read binary data
  var bitmap = _fs2.default.readFileSync(file);
  // convert binary data to base64 encoded string
  return new Buffer(bitmap).toString('base64');
}