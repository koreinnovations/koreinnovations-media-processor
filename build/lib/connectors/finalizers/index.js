'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _FinalizerConnectorBase = require('./FinalizerConnectorBase');

var _FinalizerConnectorBase2 = _interopRequireDefault(_FinalizerConnectorBase);

var _TemporaryFileRemoval = require('./TemporaryFileRemoval');

var _TemporaryFileRemoval2 = _interopRequireDefault(_TemporaryFileRemoval);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
  FinalizerConnectorBase: _FinalizerConnectorBase2.default,
  TemporaryFileRemoval: _TemporaryFileRemoval2.default
};