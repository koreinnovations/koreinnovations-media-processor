'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _ConnectorBaseWithMetaData = require('../ConnectorBaseWithMetaData');

var _ConnectorBaseWithMetaData2 = _interopRequireDefault(_ConnectorBaseWithMetaData);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var FinalizerConnectorBase = function (_ConnectorBaseWithMet) {
  _inherits(FinalizerConnectorBase, _ConnectorBaseWithMet);

  function FinalizerConnectorBase() {
    _classCallCheck(this, FinalizerConnectorBase);

    return _possibleConstructorReturn(this, (FinalizerConnectorBase.__proto__ || Object.getPrototypeOf(FinalizerConnectorBase)).apply(this, arguments));
  }

  _createClass(FinalizerConnectorBase, [{
    key: 'process',
    value: function process() {
      throw new Error('This method has not yet been implemented for this class');
    }
  }]);

  return FinalizerConnectorBase;
}(_ConnectorBaseWithMetaData2.default);

exports.default = FinalizerConnectorBase;