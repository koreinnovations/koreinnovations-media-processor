'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _CustomerConfig = require('../CustomerConfig');

var _CustomerConfig2 = _interopRequireDefault(_CustomerConfig);

var _Util = require('../Util');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var ConnectorBase = function () {
  _createClass(ConnectorBase, null, [{
    key: 'actionDescription',
    value: function actionDescription() {
      throw new Error('Connector class must define actionDescription() static method');
    }
  }]);

  function ConnectorBase(customer) {
    _classCallCheck(this, ConnectorBase);

    if (!(customer instanceof _CustomerConfig2.default)) {
      throw new Error('ConnectorBase: customer must be instance of CustomerConfig');
    }

    this._customer = customer;
  }

  _createClass(ConnectorBase, [{
    key: 'getCustomer',
    value: function getCustomer() {
      return this._customer;
    }
  }, {
    key: 'log',
    value: function log(message) {
      (0, _Util.logMessage)(this.getCustomer().getName() + ' - ' + message);
    }
  }]);

  return ConnectorBase;
}();

exports.default = ConnectorBase;