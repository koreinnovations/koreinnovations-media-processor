'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _path = require('path');

var _path2 = _interopRequireDefault(_path);

var _fs = require('fs');

var _fs2 = _interopRequireDefault(_fs);

var _fluentFfmpeg = require('fluent-ffmpeg');

var _fluentFfmpeg2 = _interopRequireDefault(_fluentFfmpeg);

var _Deferred = require('../../Deferred');

var _Deferred2 = _interopRequireDefault(_Deferred);

var _PreparerConnectorBase = require('./PreparerConnectorBase');

var _PreparerConnectorBase2 = _interopRequireDefault(_PreparerConnectorBase);

var _ConfigurationManager = require('../../ConfigurationManager');

var _ConfigurationManager2 = _interopRequireDefault(_ConfigurationManager);

var _Util = require('../../Util');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var H264Encoder = function (_PreparerConnectorBas) {
  _inherits(H264Encoder, _PreparerConnectorBas);

  function H264Encoder() {
    _classCallCheck(this, H264Encoder);

    return _possibleConstructorReturn(this, (H264Encoder.__proto__ || Object.getPrototypeOf(H264Encoder)).apply(this, arguments));
  }

  _createClass(H264Encoder, [{
    key: 'process',
    value: function process() {
      var _this2 = this;

      var config = _ConfigurationManager2.default.getConfig();

      (0, _Util.setupDirectories)();

      var file = this.getFile();
      var file_metadata = this.getFileMetaData();
      var file_path = file.path;
      var parsed = _path2.default.parse(file_path);

      file.original_path = file_path;

      var deferred = new _Deferred2.default();

      if (parsed.ext.toLowerCase() == '.mpg' || parsed.ext.toLowerCase() == '.mov') {
        this.log(file_path, 'is in mpg or mov format.  Encoding to mp4...');
        var new_path = parsed.dir + '/' + parsed.name + '.mp4';
        file.path = new_path;
        file_metadata.setFullPath(new_path);

        var command = (0, _fluentFfmpeg2.default)(file_path).outputOptions(['-vcodec h264', '-b 3500k', '-strict -2', '-vf yadif=0:-1:0'])
        //.input(full_path)
        .audioCodec('aac');

        command.on('error', function (err) {
          deferred.reject(err);
        });

        command.on('end', function () {
          _this2.log('Finished with output file', parsed.name, '.mp4');
          var new_path = _path2.default.resolve(config.paths.originals + '/' + parsed.name + parsed.ext);
          _fs2.default.renameSync(file_path, new_path);
          deferred.resolve(new_path);
        });

        command.save(new_path);
      } else {

        deferred.resolve(file_path);
      }

      return deferred.promise;
    }
  }], [{
    key: 'actionDescription',
    value: function actionDescription() {
      return 'encoded file in h264 (mp4) format';
    }
  }]);

  return H264Encoder;
}(_PreparerConnectorBase2.default);

exports.default = H264Encoder;