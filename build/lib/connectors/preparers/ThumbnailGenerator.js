'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _PreparerConnectorBase = require('./PreparerConnectorBase');

var _PreparerConnectorBase2 = _interopRequireDefault(_PreparerConnectorBase);

var _Deferred = require('../../Deferred');

var _Deferred2 = _interopRequireDefault(_Deferred);

var _fluentFfmpeg = require('fluent-ffmpeg');

var _fluentFfmpeg2 = _interopRequireDefault(_fluentFfmpeg);

var _ConfigurationManager = require('../../ConfigurationManager');

var _ConfigurationManager2 = _interopRequireDefault(_ConfigurationManager);

var _Util = require('../../Util');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var ThumbnailGenerator = function (_PreparerConnectorBas) {
  _inherits(ThumbnailGenerator, _PreparerConnectorBas);

  function ThumbnailGenerator() {
    _classCallCheck(this, ThumbnailGenerator);

    return _possibleConstructorReturn(this, (ThumbnailGenerator.__proto__ || Object.getPrototypeOf(ThumbnailGenerator)).apply(this, arguments));
  }

  _createClass(ThumbnailGenerator, [{
    key: 'process',
    value: function process() {
      var _this2 = this;

      var deferred = new _Deferred2.default();
      var config = _ConfigurationManager2.default.getConfig();

      try {
        (function () {
          (0, _Util.setupDirectories)();

          _this2.log('generateThumbnails()');
          var file_metadata = _this2.getFileMetaData();

          var valid_times = file_metadata.getValidTimes();

          if (valid_times.length == 0) {
            deferred.reject('Video is too short to generate thumbnails');
          } else {

            // Get a version of the filename safer for the web
            // and use this version to generate the thumbnails
            var new_filename = file_metadata.getSanitizedFilename();
            var full_path = file_metadata.getFullPath();
            var dimensions = file_metadata.getThumbnailDimensionsString();

            _this2.log('encoding screenshots');
            (0, _fluentFfmpeg2.default)(full_path).on('filenames', function (filenames) {
              _this2.log('filenames generated');
              file_metadata.setThumbnails(filenames);
            }).on('error', function (err) {
              deferred.reject(err);
            }).on('end', function () {
              deferred.resolve(file_metadata);
            }).screenshots({
              folder: config.paths.temp,
              filename: new_filename + '_%wx%h_%0i.png',
              timestamps: valid_times,
              size: dimensions
            });

            _this2.log('extractImage: ' + file_metadata.getTitle());
          }
        })();
      } catch (ex) {
        console.log(ex);
        deferred.reject(ex);
        throw ex;
      }

      return deferred.promise;
    }
  }], [{
    key: 'actionDescription',
    value: function actionDescription() {
      return 'generated still shot from video file';
    }
  }]);

  return ThumbnailGenerator;
}(_PreparerConnectorBase2.default);

exports.default = ThumbnailGenerator;