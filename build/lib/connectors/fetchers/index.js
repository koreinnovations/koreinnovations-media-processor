'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _FileDownloader = require('./FileDownloader');

var _FileDownloader2 = _interopRequireDefault(_FileDownloader);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
  FileDownloader: _FileDownloader2.default
};