'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _ConnectorBase2 = require('../ConnectorBase');

var _ConnectorBase3 = _interopRequireDefault(_ConnectorBase2);

var _CustomerConfig = require('../../CustomerConfig');

var _CustomerConfig2 = _interopRequireDefault(_CustomerConfig);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var FetcherConnectorBase = function (_ConnectorBase) {
  _inherits(FetcherConnectorBase, _ConnectorBase);

  function FetcherConnectorBase(customer, file_raw) {
    _classCallCheck(this, FetcherConnectorBase);

    var _this = _possibleConstructorReturn(this, (FetcherConnectorBase.__proto__ || Object.getPrototypeOf(FetcherConnectorBase)).call(this, customer));

    if (!(file_raw instanceof Object)) {
      throw new Error('FetcherConnectorBase: file_raw must be an object');
    }

    if (!file_raw.path) {
      throw new Error('FetcherConnectorBase: file_raw must have a "path" property');
    }

    _this._file_raw = file_raw;
    return _this;
  }

  _createClass(FetcherConnectorBase, [{
    key: 'getFile',
    value: function getFile() {
      return this._file_raw;
    }
  }, {
    key: 'process',
    value: function process() {
      throw new Error('This method has not yet been implemented for this class');
    }
  }]);

  return FetcherConnectorBase;
}(_ConnectorBase3.default);

exports.default = FetcherConnectorBase;