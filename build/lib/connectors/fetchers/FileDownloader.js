'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _ftp = require('ftp');

var _ftp2 = _interopRequireDefault(_ftp);

var _FetcherConnectorBase2 = require('./FetcherConnectorBase');

var _FetcherConnectorBase3 = _interopRequireDefault(_FetcherConnectorBase2);

var _Deferred = require('../../Deferred');

var _Deferred2 = _interopRequireDefault(_Deferred);

var _ConfigurationManager = require('../../ConfigurationManager');

var _ConfigurationManager2 = _interopRequireDefault(_ConfigurationManager);

var _CustomerConfig = require('../../CustomerConfig');

var _CustomerConfig2 = _interopRequireDefault(_CustomerConfig);

var _fs = require('fs');

var _fs2 = _interopRequireDefault(_fs);

var _moment = require('moment');

var _moment2 = _interopRequireDefault(_moment);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var FileDownloader = function (_FetcherConnectorBase) {
  _inherits(FileDownloader, _FetcherConnectorBase);

  _createClass(FileDownloader, null, [{
    key: 'actionDescription',
    value: function actionDescription() {
      return 'downloaded file from FTP server';
    }
  }]);

  function FileDownloader(customer, file_raw) {
    _classCallCheck(this, FileDownloader);

    var _this = _possibleConstructorReturn(this, (FileDownloader.__proto__ || Object.getPrototypeOf(FileDownloader)).call(this, customer, file_raw));

    var config = _ConfigurationManager2.default.getConfig();

    if (!config.file_downloader || !config.file_downloader.temp_downloads_directory) {
      throw new Error('file_downloader.temp_downloads_directory must be defined in config.js');
    }
    return _this;
  }

  _createClass(FileDownloader, [{
    key: 'process',
    value: function process() {
      var _this2 = this;

      var deferred = new _Deferred2.default();

      try {
        (function () {
          var config = _ConfigurationManager2.default.getConfig();
          var ftp_credentials = _this2.getCustomer().getFTPCredentials();
          var ftp_path = _this2.getCustomer().getFTPPath();
          var file = _this2.getFile();

          var filename = file.name;
          var file_path = file.path;

          var destination_path = config.file_downloader.temp_downloads_directory + '/' + filename;

          var ftp_connection = new _ftp2.default();

          ftp_connection.on('error', function (err) {
            _this2.log('Encountered error connecting to FTP server ' + ftp_credentials.host + '. Error is ' + err.message);

            deferred.reject(err);
          });

          // This is the callback that will be fired when the FTP connection
          // has been established
          ftp_connection.on('ready', function () {

            var base_path = ftp_path || '/';
            var target_path = base_path + '/' + file_path;

            _this2.log('Downloading ' + target_path);

            ftp_connection.size(target_path, function (err, file_size_bytes) {
              if (err) {
                _this2.log('FTP error: ' + err.message);
                deferred.reject(err);
                return;
              }

              ftp_connection.get(target_path, function (err, stream) {
                if (err) {
                  _this2.log('FTP error: ' + err.message);
                  deferred.reject(err);
                  return;
                }

                try {
                  var total_received = 0;
                  var last_update = 0;
                  stream.pipe(_fs2.default.createWriteStream(destination_path));
                  stream.on('data', function (chunk) {
                    total_received += chunk.length;
                    var fraction = total_received / file_size_bytes;
                    if ((0, _moment2.default)().unix() - last_update > 5) {
                      var percent = (fraction * 100).toFixed(2);
                      _this2.log('Received ' + percent + '% for ' + filename + '.');
                      last_update = (0, _moment2.default)().unix();
                    }
                  });
                  stream.once('end', function () {
                    _this2.log('Finished downloading ' + target_path);
                    ftp_connection.end();
                    file.ftp_path = file.path;
                    file.path = destination_path;
                    deferred.resolve(destination_path);
                  });
                } catch (ex) {
                  console('error', ex.message);
                  deferred.reject(ex);
                }
              });
            });
          });

          // connect to FTP server
          ftp_connection.connect(ftp_credentials);
        })();
      } catch (ex) {
        console.log('Error', ex.message);
        deferred.reject(ex);
      }

      return deferred.promise;
    }
  }]);

  return FileDownloader;
}(_FetcherConnectorBase3.default);

exports.default = FileDownloader;