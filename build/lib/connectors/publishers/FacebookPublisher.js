'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

exports.setupFacebookAuthentication = setupFacebookAuthentication;
exports.getFacebookMetaData = getFacebookMetaData;

var _fs = require('fs');

var _fs2 = _interopRequireDefault(_fs);

var _PublisherConnectorBase = require('./PublisherConnectorBase');

var _PublisherConnectorBase2 = _interopRequireDefault(_PublisherConnectorBase);

var _FileMetaData = require('../../FileMetaData');

var _FileMetaData2 = _interopRequireDefault(_FileMetaData);

var _Deferred = require('../../Deferred');

var _Deferred2 = _interopRequireDefault(_Deferred);

var _ConfigurationManager = require('../../ConfigurationManager');

var _ConfigurationManager2 = _interopRequireDefault(_ConfigurationManager);

var _fb = require('fb');

var _fb2 = _interopRequireDefault(_fb);

var _prompt = require('prompt');

var _prompt2 = _interopRequireDefault(_prompt);

var _formData = require('form-data');

var _formData2 = _interopRequireDefault(_formData);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

function setupFacebookAuthentication(FACEBOOK_TOKEN_PATH) {

  var config_values = _ConfigurationManager2.default.getConfig();
  var _config_values$oauth = config_values.oauth2.facebook,
      client_id = _config_values$oauth.client_id,
      client_secret = _config_values$oauth.client_secret,
      client_token = _config_values$oauth.client_token,
      token_url = _config_values$oauth.token_url,
      device_url = _config_values$oauth.device_url;

  var pending_authorization_error_code = 1349174;

  var getDeviceCode = function getDeviceCode() {
    var deferred = new _Deferred2.default();

    var form = new _formData2.default();
    form.append('access_token', client_id + '|' + client_token);
    form.append('scope', 'manage_pages,publish_pages');

    fetch(device_url, { method: 'POST', body: form }).then(function (res) {
      if (res.status == 200) {
        return res.json();
      } else {
        throw 'Response code ' + res.status;
      }
    }, function (error) {
      throw error;
    }).then(function (responseJSON) {
      if (responseJSON.code) {

        var code_params = {
          code: responseJSON.code,
          user_code: responseJSON.user_code,
          verification_url: responseJSON.verification_uri,
          expires_in: responseJSON.expires_in,
          interval: responseJSON.interval // Should end up as 5 seconds
        };

        deferred.resolve(code_params);
      } else {
        deferred.reject('No access code returned');
      }
    }).catch(function (error) {
      deferred.reject(error);
    });

    return deferred.promise;
  };

  var checkForCompletedAuthorization = function checkForCompletedAuthorization(device_code) {
    var deferred = new _Deferred2.default();

    var form = new _formData2.default();
    form.append('access_token', client_id + '|' + client_token);
    form.append('code', device_code);

    fetch(token_url, { method: 'POST', body: form }).then(function (res) {
      // Facebook returns a 400 status if the user has not authorized the app yet
      if (res.status == 200 || res.status == 400) {
        return res.json();
      } else {
        throw 'Response code ' + res.status;
      }
    }, function (error) {
      throw error;
    }).then(function (responseJSON) {
      if (responseJSON) {
        deferred.resolve(responseJSON);
      } else {
        deferred.reject('No token was returned');
      }
    }).catch(function (error) {
      deferred.reject(error);
    });

    return deferred.promise;
  };

  var waitForUserAuthenticationFromExternalSource = function waitForUserAuthenticationFromExternalSource(code_params) {
    var deferred = new _Deferred2.default();

    console.log('To complete the authorization process please log in to your Facebook account and go this url ' + code_params.verification_url + ' and use this code ' + code_params.user_code);

    var timer = setInterval(function () {
      checkForCompletedAuthorization(code_params.code).then(function (res) {
        if (res.hasOwnProperty('access_token')) {
          try {
            clearInterval(timer);
            // Setup FB with the user token
            _fb2.default.setAccessToken(res.access_token);
            // fs.writeFileSync(FACEBOOK_TOKEN_PATH, JSON.stringify(new_token));

            deferred.resolve();
          } catch (ex) {
            clearInterval(timer);
            deferred.reject(ex);
          }
        } else if (res.error.error_subcode == pending_authorization_error_code) {
          console.log('Pending authorization...');
        } else {
          // Something went wrong, abort
          throw res.error.error_user_msg;
        }
      }).catch(function (error) {
        clearInterval(timer);
        deferred.reject(error);
      });
    }, code_params.interval * 1000);

    return deferred.promise;
  };

  var getFacebookPagesBelongingToUser = function getFacebookPagesBelongingToUser() {
    var deferred = new _Deferred2.default();
    _fb2.default.api('me/accounts', function (res) {
      if (!res || res.error) {
        deferred.reject(res.error);
        // throw (!res ? 'error occurred' : res.error);
      }
      console.log('possible tokens', res);
      deferred.resolve(res);
    });

    return deferred.promise;
  };

  var determineWhichPageToGetTokenFor = function determineWhichPageToGetTokenFor() {
    var deferred = new _Deferred2.default();

    console.log('determineWhichPageToGetTokenFor');

    // FB call to get user's list of accounts
    getFacebookPagesBelongingToUser().then(function (pagesResult) {
      return promptToChooseFacebookPage(pagesResult);
    }, function (ex) {
      return console.warn(ex);
    }).then(function () {
      deferred.resolve();
    }).catch(function (error) {
      console.log(error);
      deferred.reject(error);
    });

    return deferred.promise;
  };

  var promptToChooseFacebookPage = function promptToChooseFacebookPage(pagesResult) {

    var available_facebook_pages = pagesResult.data;

    console.log('Please select which facebook page you would like to connect.');

    var i = 0;
    var range = /^\d+$/;

    available_facebook_pages.forEach(function (page) {
      console.log('[' + ++i + '] ' + page.name);
    });

    _prompt2.default.start();

    _prompt2.default.get({
      properties: {
        page: {
          pattern: range,
          message: 'Please choose a number between 1 and ' + i,
          required: true
        }
      }
    }, function () {
      var page_index = i - 1;
      if (page_index < 1 || page_index > available_facebook_pages.length) {
        console.log('Number is out of range');
      } else {
        var chosen_page = available_facebook_pages[page_index];
        console.log('You chose ' + chosen_page.name);
        exchangeShortTermTokenForLongTermToken(chosen_page.access_token).then(function (long_term_access_token) {
          _fs2.default.writeFileSync(FACEBOOK_TOKEN_PATH, long_term_access_token);
          console.log('Wrote long term Facebook access token to ' + FACEBOOK_TOKEN_PATH);
        });
      }
    });
  };

  var exchangeShortTermTokenForLongTermToken = function exchangeShortTermTokenForLongTermToken(access_token) {
    var deferred = new _Deferred2.default();

    console.log('exchangeShortTermTokenForLongTermToken');

    _fb2.default.api('oauth/access_token', {
      client_id: client_id,
      client_secret: client_secret,
      grant_type: 'fb_exchange_token',
      fb_exchange_token: access_token
    }, function (res) {
      if (!res || res.error) {
        console.log(!res ? 'error occurred' : res.error);
        deferred.reject(res.error);
      } else {
        var long_term_access_token = res.access_token;
        deferred.resolve(long_term_access_token);
      }
    });

    return deferred.promise;
  };

  var getFacebookDeviceAuthentication = function getFacebookDeviceAuthentication() {
    var deferred = new _Deferred2.default();

    getDeviceCode().then(waitForUserAuthenticationFromExternalSource).then(determineWhichPageToGetTokenFor).then(function () {
      deferred.resolve();
    }).catch(function (error) {
      deferred.reject(error);
    });

    return deferred.promise;
  };

  if (_fs2.default.existsSync(FACEBOOK_TOKEN_PATH)) {
    console.log('Facebook access has been set up. If the app yields a Facebook Access Denied error, remove the .facebook file and re-run this script.');
  } else {
    getFacebookDeviceAuthentication().then(function () {
      return console.log('Facebook access has been set up.  Finishing.');
    }, function (error) {
      return console.warn('There was a problem obtaining Facebook access. ' + error);
    });
  }
}

function getFacebookMetaData(file_metadata) {
  var filepath = file_metadata.getFullPath();

  return {

    title: file_metadata.getTitle(),
    description: file_metadata.getDescription(),
    // Create the readable stream to upload the video
    source: _fs2.default.createReadStream(filepath),
    content_category: 'FAMILY' //file_metadata.getTags()

  };
}

var FacebookPublisher = function (_PublisherConnectorBa) {
  _inherits(FacebookPublisher, _PublisherConnectorBa);

  _createClass(FacebookPublisher, null, [{
    key: 'actionDescription',
    value: function actionDescription() {
      return 'published to Facebook';
    }
  }]);

  function FacebookPublisher(customer, file_raw, file_metadata) {
    _classCallCheck(this, FacebookPublisher);

    var _this = _possibleConstructorReturn(this, (FacebookPublisher.__proto__ || Object.getPrototypeOf(FacebookPublisher)).call(this, customer, file_raw, file_metadata));

    try {
      customer.getOAuth2Instance('facebook');
    } catch (ex) {
      throw new Error('Facebook OAuth2 credentials not provided');
    }

    return _this;
  }

  _createClass(FacebookPublisher, [{
    key: 'uploadFacebookVideo',
    value: function uploadFacebookVideo() {

      var deferred = new _Deferred2.default();

      try {
        var file_metadata = this.getFileMetaData();
        var customer = this.getCustomer();
        var facebook = customer.getOAuth2Instance('facebook');
        var facebook_token = facebook.getAccessToken();

        _fb2.default.setAccessToken(facebook_token);

        var facebook_video_data = getFacebookMetaData(file_metadata);

        _fb2.default.api('me/videos', 'post', facebook_video_data, function (res) {
          if (!res || res.error) {
            deferred.reject(res.error);
          } else {
            deferred.resolve();
          }
        });
      } catch (ex) {
        deferred.reject(ex.message);
      }

      return deferred.promise;
    }
  }, {
    key: 'process',
    value: function process() {
      var _this2 = this;

      this.log('uploading file to facebook');
      var deferred = new _Deferred2.default();

      try {
        var customer = this.getCustomer();
        var oauth2 = customer.getOAuth2Instance('facebook');
        oauth2.ensureAccess().then(function () {

          // Upload to facebook api
          _this2.uploadFacebookVideo().then(function () {
            deferred.resolve();
          }, function (err) {
            _this2.log('Error uploading file to facebook');
            deferred.reject(err);
          }).catch(function (error) {
            _this2.log('Exception caught: ' + error.message);
            deferred.reject(error);
          });
        }, function (err) {
          return deferred.reject(err);
        });
      } catch (ex) {
        deferred.reject(ex.message);
      }

      return deferred.promise;
    }
  }]);

  return FacebookPublisher;
}(_PublisherConnectorBase2.default);

exports.default = FacebookPublisher;