'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

exports.getYouTubeMetaData = getYouTubeMetaData;

var _fs = require('fs');

var _fs2 = _interopRequireDefault(_fs);

var _PublisherConnectorBase = require('./PublisherConnectorBase');

var _PublisherConnectorBase2 = _interopRequireDefault(_PublisherConnectorBase);

var _FileMetaData = require('../../FileMetaData');

var _FileMetaData2 = _interopRequireDefault(_FileMetaData);

var _Deferred = require('../../Deferred');

var _Deferred2 = _interopRequireDefault(_Deferred);

var _youtubeApi = require('youtube-api');

var _youtubeApi2 = _interopRequireDefault(_youtubeApi);

var _prettyBytes = require('pretty-bytes');

var _prettyBytes2 = _interopRequireDefault(_prettyBytes);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

function getYouTubeMetaData(file_metadata) {
  var filepath = file_metadata.getFullPath();

  return {
    resource: {
      // Video title and description
      snippet: {
        title: file_metadata.getTitle(),
        description: file_metadata.getDescription(),
        tags: file_metadata.getTags()
      },
      status: {
        privacyStatus: "public"
      }
    },
    // This is for the callback function
    part: "snippet,status",

    // Create the readable stream to upload the video
    media: {
      body: _fs2.default.createReadStream(filepath)
    }
  };
}

var YouTubePublisher = function (_PublisherConnectorBa) {
  _inherits(YouTubePublisher, _PublisherConnectorBa);

  function YouTubePublisher(customer, file_raw, file_metadata) {
    _classCallCheck(this, YouTubePublisher);

    var _this = _possibleConstructorReturn(this, (YouTubePublisher.__proto__ || Object.getPrototypeOf(YouTubePublisher)).call(this, customer, file_raw, file_metadata));

    try {
      customer.getOAuth2Instance('youtube');
    } catch (ex) {
      throw new Error('YouTube OAuth2 credentials not provided');
    }

    return _this;
  }

  _createClass(YouTubePublisher, [{
    key: 'uploadYoutubeVideo',
    value: function uploadYoutubeVideo() {
      var _this2 = this;

      var deferred = new _Deferred2.default();

      try {
        (function () {
          var file_metadata = _this2.getFileMetaData();
          var filepath = _this2.getFileMetaData().getFullPath();
          var filename = filepath.replace(/.+\//, '');
          var customer = _this2.getCustomer();
          var youtube = customer.getOAuth2Instance('youtube');

          var youtube_token = youtube.getAccessToken();

          var _youtube$getAppCreden = youtube.getAppCredentials(),
              client_id = _youtube$getAppCreden.client_id,
              client_secret = _youtube$getAppCreden.client_secret;

          _this2.log('YouTube token: ' + youtube_token);
          var authentication = {
            type: "oauth",
            client_id: client_id,
            client_secret: client_secret,
            redirect_url: 'http://localhost:5000/oauth2callback',
            access_token: youtube_token
          };

          _youtubeApi2.default.authenticate(authentication).setCredentials({ access_token: youtube_token });

          var youtube_video_data = getYouTubeMetaData(file_metadata);

          var interval = setInterval(function () {
            _this2.log((0, _prettyBytes2.default)(youtube_insert_api_request.req.connection._bytesDispatched) + ' bytes uploaded.');
          }, 5000);

          var youtube_insert_api_request = _youtubeApi2.default.videos.insert(youtube_video_data, function (err) {
            clearInterval(interval);
            if (err) {
              deferred.reject(err);
            } else {
              deferred.resolve(filename);
            }
          });
        })();
      } catch (ex) {
        deferred.reject(ex.message);
      }

      return deferred.promise;
    }
  }, {
    key: 'process',
    value: function process() {
      var _this3 = this;

      this.log('uploading file to youtube');
      var deferred = new _Deferred2.default();

      try {
        var customer = this.getCustomer();
        var oauth2 = customer.getOAuth2Instance('youtube');
        oauth2.ensureAccess().then(function () {

          // Upload to youtube api
          _this3.uploadYoutubeVideo().then(function () {
            deferred.resolve();
          }, function (err) {
            _this3.log('Error uploading file to youtube');
            deferred.reject(err);
          }).catch(function (error) {
            _this3.log('Exception caught: ' + error.message);
            deferred.reject(error);
          });
        }, function (err) {
          return deferred.reject(err);
        });
      } catch (ex) {
        deferred.reject(ex.message);
      }

      return deferred.promise;
    }
  }]);

  return YouTubePublisher;
}(_PublisherConnectorBase2.default);

exports.default = YouTubePublisher;