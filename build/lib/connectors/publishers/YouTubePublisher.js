'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

exports.setupYouTubeAuthentication = setupYouTubeAuthentication;
exports.getYouTubeMetaData = getYouTubeMetaData;

var _moment = require('moment');

var _moment2 = _interopRequireDefault(_moment);

var _fs = require('fs');

var _fs2 = _interopRequireDefault(_fs);

var _lien = require('lien');

var _lien2 = _interopRequireDefault(_lien);

var _qs = require('qs');

var _qs2 = _interopRequireDefault(_qs);

var _PublisherConnectorBase = require('./PublisherConnectorBase');

var _PublisherConnectorBase2 = _interopRequireDefault(_PublisherConnectorBase);

var _FileMetaData = require('../../FileMetaData');

var _FileMetaData2 = _interopRequireDefault(_FileMetaData);

var _ConfigurationManager = require('../../ConfigurationManager');

var _ConfigurationManager2 = _interopRequireDefault(_ConfigurationManager);

var _Util = require('../../Util');

var _Deferred = require('../../Deferred');

var _Deferred2 = _interopRequireDefault(_Deferred);

var _OAuth2Client = require('../../OAuth2Client');

var _OAuth2Client2 = _interopRequireDefault(_OAuth2Client);

var _youtubeApi = require('youtube-api');

var _youtubeApi2 = _interopRequireDefault(_youtubeApi);

var _prettyBytes = require('pretty-bytes');

var _prettyBytes2 = _interopRequireDefault(_prettyBytes);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

function setupYouTubeAuthentication(YOUTUBE_TOKEN_PATH) {
  var youtube = _ConfigurationManager2.default.getConfig().oauth2.youtube;

  var getDeviceCode = function getDeviceCode() {
    var deferred = new _Deferred2.default();

    var request_params = {
      client_id: youtube.client_id,
      scope: 'https://gdata.youtube.com https://www.googleapis.com/auth/youtube'
    };
    var headers = { 'Content-Type': 'application/x-www-form-urlencoded' };

    console.log('fetching token with device code');
    fetch('https://accounts.google.com/o/oauth2/device/code', {
      method: 'POST',
      body: _qs2.default.stringify(request_params),
      headers: headers
    }).then(function (res) {
      if (res.status == 200) {
        deferred.resolve(res.json());
      } else {
        throw 'Response code ' + res.status;
      }
    }, function (error) {
      console.warn(error.message);
      throw error;
    }).catch(function (error) {
      console.log(error);
      deferred.reject(error);
    });

    return deferred.promise;
  };

  var promptForAuthentication = function promptForAuthentication() {
    getDeviceCode().then(watchForAuthorization);
  };

  var checkGoogleServerForCompletedAuthorization = function checkGoogleServerForCompletedAuthorization(device_code) {
    var deferred = new _Deferred2.default();

    var request_params = {
      client_id: youtube.client_id,
      client_secret: youtube.client_secret,
      grant_type: 'http://oauth.net/grant_type/device/1.0',
      code: device_code
    };
    var headers = { 'Content-Type': 'application/x-www-form-urlencoded' };

    fetch('https://accounts.google.com/o/oauth2/token', { method: 'POST', body: _qs2.default.stringify(request_params), headers: headers }).then(function (res) {
      if (res.status == 200) {
        deferred.resolve(res.json());
      } else {
        console.log('---------');
        throw 'Response code ' + res.status;
      }
    }, function (error) {
      console.warn(error.message);
      throw error;
    });

    return deferred.promise;
  };

  var watchForAuthorization = function watchForAuthorization(res) {
    var code = res.device_code;
    var user_code = res.user_code;
    var verification_url = res.verification_url;

    if (code) {
      (function () {

        console.log('To complete the authorization procces please log in to your youtube account and go this url ' + verification_url + ' and use this code ' + user_code);

        var timer = setInterval(function () {

          checkGoogleServerForCompletedAuthorization(code).then(function (res) {
            if (res.access_token) {
              clearInterval(timer);
              console.log('YouTube access has been set up.  Finishing.');
              _fs2.default.writeFileSync(YOUTUBE_TOKEN_PATH, JSON.stringify(res));
            } else {
              console.log('Pending authorization...');
            }
          });
        }, 5000);
      })();
    } else {
      console.log('No device code was received from Google');
    }
  };

  if (_fs2.default.existsSync(YOUTUBE_TOKEN_PATH)) {
    (function () {

      console.log('YouTube access has already been set up.  Renewing token...');

      var existing_token = JSON.parse(_fs2.default.readFileSync(YOUTUBE_TOKEN_PATH, 'utf8'));
      var config = _ConfigurationManager2.default.getConfig();
      var client = new _OAuth2Client2.default(config.oauth2.youtube.client_id, config.oauth2.youtube.client_secret, config.oauth2.youtube.token_url);

      client.setToken(existing_token.access_token, existing_token.refresh_token, existing_token.token_expiration);

      client.ensureAccess().then(function () {
        var new_token = {
          access_token: client._access_token,
          refresh_token: client._refresh_token,
          token_expiration: client._token_expiration
        };

        console.log('Renewed token and wrote to ' + YOUTUBE_TOKEN_PATH);
        _fs2.default.writeFileSync(YOUTUBE_TOKEN_PATH, JSON.stringify(new_token));
      }, function () {
        console.log('Failed to renew token.  Let\'s get a brand-new token using your credentials.');
        _fs2.default.unlinkSync(YOUTUBE_TOKEN_PATH);
        promptForAuthentication();
      });
    })();
  } else {
    console.log('No token exists for YouTube.  Login required.');
    promptForAuthentication();
  }
}

function getYouTubeMetaData(file_metadata) {
  var filepath = file_metadata.getFullPath();

  return {
    resource: {
      // Video title and description
      snippet: {
        title: file_metadata.getTitle(),
        description: file_metadata.getDescription(),
        tags: file_metadata.getTags()
      },
      status: {
        privacyStatus: 'public'
      }
    },
    // This is for the callback function
    part: 'snippet,status',

    // Create the readable stream to upload the video
    media: {
      body: _fs2.default.createReadStream(filepath)
    }
  };
}

var YouTubePublisher = function (_PublisherConnectorBa) {
  _inherits(YouTubePublisher, _PublisherConnectorBa);

  _createClass(YouTubePublisher, null, [{
    key: 'actionDescription',
    value: function actionDescription() {
      return 'published to YouTube';
    }
  }, {
    key: 'getOAuth2Token',
    value: function getOAuth2Token(local_token_file_path) {
      var config = _ConfigurationManager2.default.getConfig();
      var deferred = new _Deferred2.default();
      var _config$oauth2$youtub = config.oauth2.youtube,
          client_id = _config$oauth2$youtub.client_id,
          client_secret = _config$oauth2$youtub.client_secret;


      try {
        // This will throw an error if the file doesn't exist
        _fs2.default.accessSync(local_token_file_path);

        var contents = _fs2.default.readFileSync(local_token_file_path, 'utf8');
        (0, _Util.logMessage)('Read YouTube token from file');
        var token_from_file = JSON.parse(contents);
        if (token_from_file.access_token) {
          deferred.resolve(token_from_file);
        } else {
          throw new Error('Access token not found in YouTube token file');
        }
      } catch (ex) {

        // Init the lien server
        var server = new _lien2.default({ host: 'localhost', port: 5000 });

        // Here we're waiting for the OAuth2 redirect containing the auth code
        server.addPage('/oauth2callback', function (lien) {
          var code = lien.query.code;


          (0, _Util.logMessage)('Trying to get the token using the following code: ' + code);

          // Get access token
          var token_url = 'https://accounts.google.com/o/oauth2/token';
          var request_params = {
            client_id: client_id,
            client_secret: client_secret,
            code: code,
            redirect_uri: 'http://localhost:5000/oauth2callback',
            grant_type: 'authorization_code'
          };
          var headers = { 'Content-Type': 'application/x-www-form-urlencoded' };

          fetch(token_url, { method: 'POST', body: _qs2.default.stringify(request_params), headers: headers }).then(function (res) {
            if (res.status == 200) {
              return res.json();
            } else {
              throw 'Response code ' + res.status;
            }
          }, function (error) {
            console.warn(error.message);
            throw error;
          }).then(function (responseJSON) {
            try {
              if (responseJSON.access_token) {
                var new_expiration = (0, _moment2.default)().unix() + parseInt(responseJSON.expires_in);

                var new_token = {
                  access_token: responseJSON.access_token,
                  refresh_token: responseJSON.refresh_token,
                  token_expiration: new_expiration
                };

                console.log('writing file to ', local_token_file_path);
                _fs2.default.writeFileSync(local_token_file_path, JSON.stringify(new_token));

                deferred.resolve(new_token);
              } else {
                deferred.reject('No access code returned');
              }
            } catch (ex) {
              console.log(ex);
              deferred.reject(ex);
            }
          }).catch(function (error) {
            console.log(error);
            deferred.reject(error);
          });
        });

        var auth_url = 'https://accounts.google.com/o/oauth2/v2/auth';
        var auth_params = {
          client_id: client_id,
          redirect_uri: 'http://localhost:5000/oauth2callback',
          response_type: 'code',
          access_type: 'offline',
          prompt: 'consent',
          oauth: 1,
          scope: 'https://gdata.youtube.com https://www.googleapis.com/auth/youtube https://www.googleapis.com/auth/youtubepartner'
        };

        // Open the authentication url in the default browser
        (0, _Util.openURL)(auth_url + '?' + _qs2.default.stringify(auth_params));
      }

      return deferred.promise;
    }
  }]);

  function YouTubePublisher(customer, file_raw, file_metadata) {
    _classCallCheck(this, YouTubePublisher);

    var _this = _possibleConstructorReturn(this, (YouTubePublisher.__proto__ || Object.getPrototypeOf(YouTubePublisher)).call(this, customer, file_raw, file_metadata));

    try {
      customer.getOAuth2Instance('youtube');
    } catch (ex) {
      throw new Error('YouTube OAuth2 credentials not provided');
    }

    return _this;
  }

  _createClass(YouTubePublisher, [{
    key: 'uploadYoutubeVideo',
    value: function uploadYoutubeVideo() {
      var _this2 = this;

      var deferred = new _Deferred2.default();

      try {
        (function () {
          var file_metadata = _this2.getFileMetaData();
          var filepath = _this2.getFileMetaData().getFullPath();
          var filename = filepath.replace(/.+\//, '');
          var customer = _this2.getCustomer();
          var youtube = customer.getOAuth2Instance('youtube');

          var youtube_token = youtube.getAccessToken();

          var _youtube$getAppCreden = youtube.getAppCredentials(),
              client_id = _youtube$getAppCreden.client_id,
              client_secret = _youtube$getAppCreden.client_secret;

          _this2.log('YouTube token: ' + youtube_token);
          var authentication = {
            type: 'oauth',
            client_id: client_id,
            client_secret: client_secret,
            redirect_url: 'http://localhost:5000/oauth2callback',
            access_token: youtube_token
          };

          _youtubeApi2.default.authenticate(authentication).setCredentials({ access_token: youtube_token });

          var youtube_video_data = getYouTubeMetaData(file_metadata);

          var interval = setInterval(function () {
            _this2.log((0, _prettyBytes2.default)(youtube_insert_api_request.req.connection._bytesDispatched) + ' bytes uploaded.');
          }, 5000);

          var youtube_insert_api_request = _youtubeApi2.default.videos.insert(youtube_video_data, function (err) {
            clearInterval(interval);
            if (err) {
              deferred.reject(err);
            } else {
              deferred.resolve(filename);
            }
          });
        })();
      } catch (ex) {
        deferred.reject(ex.message);
      }

      return deferred.promise;
    }
  }, {
    key: 'process',
    value: function process() {
      var _this3 = this;

      this.log('uploading file to youtube');
      var deferred = new _Deferred2.default();

      try {
        var customer = this.getCustomer();
        var oauth2 = customer.getOAuth2Instance('youtube');
        oauth2.ensureAccess().then(function () {

          // Upload to youtube api
          _this3.uploadYoutubeVideo().then(function () {
            deferred.resolve();
          }, function (err) {
            _this3.log('Error uploading file to youtube');
            deferred.reject(err);
          }).catch(function (error) {
            _this3.log('Exception caught: ' + error.message);
            deferred.reject(error);
          });
        }, function (err) {
          return deferred.reject(err);
        });
      } catch (ex) {
        deferred.reject(ex.message);
      }

      return deferred.promise;
    }
  }]);

  return YouTubePublisher;
}(_PublisherConnectorBase2.default);

exports.default = YouTubePublisher;