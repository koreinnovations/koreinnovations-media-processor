'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

exports.getVimeoMetaData = getVimeoMetaData;

var _PublisherConnectorBase = require('./PublisherConnectorBase');

var _PublisherConnectorBase2 = _interopRequireDefault(_PublisherConnectorBase);

var _fs = require('fs');

var _fs2 = _interopRequireDefault(_fs);

var _FileMetaData = require('../../FileMetaData');

var _FileMetaData2 = _interopRequireDefault(_FileMetaData);

var _Deferred = require('../../Deferred');

var _Deferred2 = _interopRequireDefault(_Deferred);

var _vimeo = require('vimeo');

var _vimeo2 = _interopRequireDefault(_vimeo);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }
// eslint-disable-next-line


function getVimeoMetaData(file_metadata) {
  var filepath = file_metadata.getFullPath();

  return {
    resource: {
      // Video title and description
      snippet: {
        title: file_metadata.getTitle(),
        description: file_metadata.getDescription(),
        tags: file_metadata.getTags()
      },
      status: {
        privacyStatus: 'public'
      }
    },
    // This is for the callback function
    part: 'snippet,status',

    // Create the readable stream to upload the video
    media: {
      body: _fs2.default.createReadStream(filepath)
    }
  };
}

var VimeoPublisher = function (_PublisherConnectorBa) {
  _inherits(VimeoPublisher, _PublisherConnectorBa);

  _createClass(VimeoPublisher, null, [{
    key: 'actionDescription',
    value: function actionDescription() {
      return 'published to Vimeo';
    }
  }]);

  function VimeoPublisher(customer, file_raw, file_metadata) {
    _classCallCheck(this, VimeoPublisher);

    var _this = _possibleConstructorReturn(this, (VimeoPublisher.__proto__ || Object.getPrototypeOf(VimeoPublisher)).call(this, customer, file_raw, file_metadata));

    try {
      customer.getOAuth2Instance('vimeo');
    } catch (ex) {
      throw new Error('Vimeo OAuth2 credentials not provided');
    }

    return _this;
  }

  _createClass(VimeoPublisher, [{
    key: 'uploadVimeoVideo',
    value: function uploadVimeoVideo() {
      var _this2 = this;

      var deferred = new _Deferred2.default();

      try {
        (function () {
          var file_metadata = _this2.getFileMetaData();
          var filepath = _this2.getFileMetaData().getFullPath();
          var filename = filepath.replace(/.+\//, '');
          var customer = _this2.getCustomer();
          var vimeo = customer.getOAuth2Instance('vimeo');

          var vimeo_token = vimeo.getAccessToken();

          var _vimeo$getAppCredenti = vimeo.getAppCredentials(),
              client_id = _vimeo$getAppCredenti.client_id,
              client_secret = _vimeo$getAppCredenti.client_secret;

          _this2.log('Vimeo token: ' + vimeo_token);
          var authentication = {
            client_id: client_id,
            client_secret: client_secret,
            vimeo_token: vimeo_token
          };
          lib = new Vimeo(authentication);

          // var url = lib.buildAuthorizationEndpoint(redirect_uri, scopes, state);


          lib.streamingUpload(filepath, function (error, body, status_code, headers) {
            if (error) {
              throw error;
            }

            lib.request(headers.location, function (error, body) {
              console.log(body);
            });
          }, function (upload_size, file_size) {
            console.log('You have uploaded ' + Math.round(uploaded_size / file_size * 100) + '% of the video');
          });

          var vimeo_video_data = getVimeoMetaData(file_metadata);

          //let interval = setInterval(function () {
          //  this.log(`${prettyBytes(vimeo_insert_api_request.req.connection._bytesDispatched)} bytes uploaded.`);
          //}, 5000);

          Vimeo.videos.insert(vimeo_video_data, function (err) {
            clearInterval(interval);
            if (err) {
              deferred.reject(err);
            } else {
              deferred.resolve(filename);
            }
          });
        })();
      } catch (ex) {
        deferred.reject(ex.message);
      }

      return deferred.promise;
    }
  }, {
    key: 'process',
    value: function process() {
      var _this3 = this;

      this.log('uploading file to vimeo');
      var deferred = new _Deferred2.default();

      try {
        var customer = this.getCustomer();
        var oauth2 = customer.getOAuth2Instance('vimeo');
        oauth2.ensureAccess().then(function () {

          // Upload to vimeo api
          _this3.uploadVimeoVideo().then(function () {
            deferred.resolve();
          }, function (err) {
            _this3.log('Error uploading file to vimeo');
            deferred.reject(err);
          }).catch(function (error) {
            _this3.log('Exception caught: ' + error.message);
            deferred.reject(error);
          });
        }, function (err) {
          return deferred.reject(err);
        });
      } catch (ex) {
        deferred.reject(ex.message);
      }

      return deferred.promise;
    }
  }]);

  return VimeoPublisher;
}(_PublisherConnectorBase2.default);

exports.default = VimeoPublisher;